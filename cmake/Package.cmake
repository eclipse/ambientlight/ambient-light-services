#  ========================================================================
#  Ambient Light Services
# 
#  Copyright (C) 2019 - 2022 T-Systems International GmbH
# 
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#  
#       http://www.apache.org/licenses/LICENSE-2.0
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# 
#  SPDX-License-Identifier: Apache-2.0
# 
#  ========================================================================

# Before including the CPack module in your CMakeLists.txt file, there are a
# variety of variables that can be set to customize the resulting installers.
# The CPack module then generates binary (CPackConfig.cmake) and
# source installers (CPackSourceConfig.cmake).

# Config CPack version
set(CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${VERSION_PATCH}")

# Config package name
set(PACKAGE_NAME_COMPANY "TS")
set(PACKAGE_NAME_MODULE "AmbLS")

string(JOIN "-" PACKAGE_NAME_VERSION
    R${CPACK_PACKAGE_VERSION_MAJOR}
    ${CPACK_PACKAGE_VERSION_MINOR}
    ${CPACK_PACKAGE_VERSION_PATCH}
)

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    string(APPEND PACKAGE_NAME_VERSION "-${CMAKE_BUILD_TYPE}")
endif()

set(PACKAGE_NAME_STATUS "Vorab")

string(JOIN "_" CPACK_PACKAGE_NAME
    ${PACKAGE_NAME_COMPANY}
    ${PACKAGE_NAME_MODULE}
    ${PACKAGE_NAME_VERSION}
)

string(JOIN "_" CPACK_PACKAGE_FILE_NAME
    ${CPACK_PACKAGE_NAME} "SWC" ${PACKAGE_NAME_STATUS}
)
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}_Source")

set(CPACK_GENERATOR "ZIP")
set(CPACK_SOURCE_GENERATOR "ZIP")

set(CPACK_SOURCE_IGNORE_FILES
    "/build/"
    "/\\\\.settings/"
    "/\\\\.project"
    "/\\\\.cproject"
    "/\\\\.gitignore"
    "/\\\\.clang-format"
    "/\\\\.git/"
    "\\\\.#;/#"
)