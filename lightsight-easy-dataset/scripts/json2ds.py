#!C:/Python37/python.exe
# ========================================================================
# json2ds.py
#
# Copyright (c) 2020, Dmitrii Fedotov, T-Systems RUS
# All Rights Reserved
# UNPUBLISHED, LICENSED SOFTWARE.
#
# CONFIDENTIAL AND PROPRIETARY INFORMATION
# WHICH IS THE PROPERTY OF T-Systems.
#
# ========================================================================
#

import json
import argparse
import libscrc
import sys
import os

prog_description = "JSON to dataset converter, v.1.2.0"
output_filename = "dataset-v.2.0.bin"
input_filename = "default-dataset_mod.json"

DATASETLEN = 2564
CRC32LEN = 4

EventSceneSize     = 1

SznGroupSize      = 1
SznSchritteSize    = 1 
Szn_tSize          = 1
Szn_AnimationSize  = 1

AniZiel_tSize      = 2
AniZiel_FabreSize  = 1
AniTypeeSize        = 1
Anit_tSize         = 2
AniPosStartSize    = 1
AniPosEndeSize     = 1
AniRotDirSize      = 1

AniLichtPosSize    = 1
LichtAusTypeSize   = 1
LichtTreiberSize   = 1
LichtVerzSize      = 1
LichtAdresseSize   = 1
LichtMaxSize       = 1
LichtFarbTypeSize  = 1

PosWinkelSize      = 2
PosRadiusSize      = 2

FarbeHueSize       = 2
FarbeSatSize       = 1
FarbeValSize       = 1

DelayMaxSize       = 1
NumScenesSize      = 1
NumAnimationsSize = 1
NumLuminariesSize    = 1
NumFarbenSize      = 1
NumPositionsSize  = 1

try:
    method = os.environ['REQUEST_METHOD']
except:
    method = None

#if method == None our script called from the command line
if method == None:
    argprs = argparse.ArgumentParser(prog = 'json2ds', description = prog_description)

    argprs.add_argument('-i', '--input',        type = str, metavar = '<input_file>', required = True,
                        help = 'Input JSON file')
    argprs.add_argument('-e', '--errorignore', action = 'store_true',
                        help = 'Ignore errors')
    argprs.add_argument('-o', '--output',       type = str, metavar = '<output_file>', required = False,
                        help = 'Output dataset file')

    args = argprs.parse_args()
    input_filename = args.input

    if None != args.output:
        output_filename = args.output

    fpjson = open(input_filename, "r")

else:
    content_len = int(os.environ["CONTENT_LENGTH"])

if method == None:
    jsondata = fpjson.read()
else:
    jsondata = sys.stdin.read(content_len)

fp = open(output_filename, "w+b")

# parse file
obj = json.loads(jsondata)

limits = obj['limits']

delay_max      = limits['delay_max']
events_max     = limits['events_max']
scenes_max     = limits['scenes_max']
substeps_max   = limits['substeps_max']
animations_max = limits['animations_max']
lights_max     = limits['lights_max']
colors_max     = limits['colors_max']
positions_max  = limits['positions_max']
dimm2bin       = limits['dimm2bin']

date_len       = limits['date_len']
info_len       = limits['info_len']

#date_len = 22  # should be removed for proper work with final JSON version

date = obj['date']

fp.write(bytes(date, encoding = 'utf-8'))
fp.seek(date_len,0)

version = obj['version']

majorver = version['major']
fp.write(majorver.to_bytes(2, byteorder = 'little'))
minorver = version['minor']
fp.write(minorver.to_bytes(2, byteorder = 'little'))

#save dataset name

info = obj['info']

fp.write(bytes(info, encoding = 'utf-8'))
fp.seek(date_len + 4 + info_len, 0)

#save event scenes

event_scenes = obj['event_scenes']

event_scenes_offset = 0
for event_scenesidx in event_scenes:
    if type(event_scenesidx) is dict: # new "dictionary" format
        scenes = event_scenesidx['scenes']
    else:                             # old format
        scenes = event_scenesidx
    for scenesidx in scenes:
        event_scenes_offset += EventSceneSize
        fp.write(scenesidx.to_bytes(EventSceneSize, byteorder = 'little'))

# scenes save

sceneslist = obj['scenes']

total_scenes = len(sceneslist)

if total_scenes > scenes_max:
    for i in range(total_scenes - scenes_max):
        del sceneslist[-1]
    total_scenes = scenes_max

actual_scenes = scenes_max

#fill scenes list up to scenes_max
for i in range(0, scenes_max - total_scenes):
    sceneslist.append({'group': 0, 'substeps': []})

grouplist = []
substeplist = []
delaylist = []
aniidlist = []
scenelenlist = []

for idxscenes in sceneslist:
    group = idxscenes['group']
    substeps = idxscenes['substeps']

    grouplist.append(group)
    substeps_len = len(substeps)
    scenelenlist.append(substeps_len)

    if 0 == substeps_len:
        for i in range(0, substeps_max):
            delaylist.append(0)
            aniidlist.append(0)
    else:    
        for idxsubsteps in substeps:
            delaylist.append(idxsubsteps['delay'])
            aniidlist.append(idxsubsteps['animation_id'])

        for i in range(0, substeps_max - substeps_len):
            delaylist.append(0)
            aniidlist.append(0)

group_offset = len(grouplist) * SznGroupSize
for idxgrouplist in grouplist:
    fp.write(idxgrouplist.to_bytes(SznGroupSize, byteorder = 'little'))

scenelen_offset = len(scenelenlist) * SznSchritteSize
for idxscenelenlist in scenelenlist:
    fp.write(idxscenelenlist.to_bytes(SznSchritteSize, byteorder = 'little'))

#enable follow code for proper work with final JSON version
ssdelay_offset = len(delaylist) * Szn_tSize
#ssdelay_offset = 0                          # should be removed for proper work with final JSON version
#idx = 0                                     # should be removed for proper work with final JSON version
for idxdelaylist in delaylist:
#    if idx < 320:                           # should be removed for proper work with final JSON version
#        ssdelay_offset += Szn_tSize         # should be removed for proper work with final JSON version
#        idx += 1                            # should be removed for proper work with final JSON version
    fp.write(idxdelaylist.to_bytes(Szn_tSize, byteorder = 'little'))        

#enable the follow code for proper work with final JSON version
ssaniid_offset = len(aniidlist) * Szn_AnimationSize
#ssaniid_offset = 0                          # should be removed for proper work with final JSON version
#idx = 0                                     # should be removed for proper work with final JSON version
for idxaniidlist in aniidlist:
#    if idx < 320:                           # should be removed for proper work with final JSON version
#        ssaniid_offset += Szn_AnimationSize # should be removed for proper work with final JSON version
#        idx += 1                            # should be removed for proper work with final JSON version
    fp.write(idxaniidlist.to_bytes(Szn_AnimationSize, byteorder = 'little'))        

#animations save

actulal_animations = 0

ttlist = []
tclist = []
typelist = []
atlist = []
stposlist = []
enposlist = []
rotdirlist = []

animationslist = obj['animations']
actulal_animations = len(animationslist)

#fill animation list up to animation_max
for i in range(0, animations_max - actulal_animations):
    animationslist.append({'target_time': 0, 'target_color': 0, 'type': 0, 'animation_time': 0, 'start_position_id': 0, 'end_position_id': 0, 'rotation_direction': 0})

for idxanimationslist in animationslist:
    ttlist.append(idxanimationslist['target_time'])
    tclist.append(idxanimationslist['target_color'])
    typelist.append(idxanimationslist['type'])
    atlist.append(idxanimationslist['animation_time'])
    stposlist.append(idxanimationslist['start_position_id'])
    enposlist.append(idxanimationslist['end_position_id'])
    rotdirlist.append(idxanimationslist['rotation_direction'])

tt_offset = len(ttlist) * AniZiel_tSize
for idxttlist in ttlist:
    fp.write(idxttlist.to_bytes(AniZiel_tSize, byteorder = 'little'))

tc_offset = len(tclist) * AniZiel_FabreSize
for idxtclist in tclist:
    fp.write(idxtclist.to_bytes(AniZiel_FabreSize, byteorder = 'little'))

type_offset = len(typelist) * AniTypeeSize
for idxtypelist in typelist:
    fp.write(idxtypelist.to_bytes(AniTypeeSize, byteorder = 'little'))

at_offset = len(atlist) * Anit_tSize
for idxatlist in atlist:
    fp.write(idxatlist.to_bytes(Anit_tSize, byteorder = 'little'))

stpos_offset = len(stposlist) * AniPosStartSize
for idxstposlist in stposlist:
    fp.write(idxstposlist.to_bytes(AniPosStartSize, byteorder = 'little'))

endpos_offset = len(enposlist) * AniPosEndeSize
for idxenposlist in enposlist:
    fp.write(idxenposlist.to_bytes(AniPosEndeSize, byteorder = 'little'))

rotdir_offset = len(rotdirlist) * AniRotDirSize
for idxrotdirlist in rotdirlist:
    fp.write(idxrotdirlist.to_bytes(AniRotDirSize, byteorder = 'little'))

#lights save

lightslist = obj['lights']
actual_lights = len(lightslist)

#fill lights list up to lights_max
for i in range(0, lights_max - actual_lights):
    lightslist.append({'position_id': 0, 'type': 0, 'driver': 0, 'delay': 0, 'address': 0, 'groups': [0, 0, 0, 0, 0, 0, 0, 0], 'color_space': 0})

posidlist = []
typelist = []
driverlist = []
delaylist = []
addrlist = []
groupslist = []
cslist = []

for idxlightslist in lightslist:
    posidlist.append(idxlightslist['position_id'])
    typelist.append(idxlightslist['type'])
    driverlist.append(idxlightslist['driver'])
    delaylist.append(idxlightslist['delay'])
    addrlist.append(idxlightslist['address'])

    groups = idxlightslist['groups']
    for idxgroups in groups:
        groupslist.append(idxgroups)
    
    cslist.append(idxlightslist['color_space'])

posid_offset = len(posidlist) * AniLichtPosSize
for idxposidlist in posidlist:
    fp.write(idxposidlist.to_bytes(AniLichtPosSize, byteorder = 'little'))

lighttype_offset = len(typelist) * LichtAusTypeSize 
for idxtypelist in typelist:
    fp.write(idxtypelist.to_bytes(LichtAusTypeSize, byteorder = 'little'))

driver_offset = len(driverlist) * LichtTreiberSize
for idxdriverlist in driverlist:
    fp.write(idxdriverlist.to_bytes(LichtTreiberSize, byteorder = 'little'))

delay_offset = len(delaylist) * LichtVerzSize
for idxdelaylist in delaylist:
    fp.write(idxdelaylist.to_bytes(LichtVerzSize, byteorder = 'little'))

addr_offset = len(addrlist) * LichtAdresseSize
for idxaddrlist in addrlist:
    fp.write(idxaddrlist.to_bytes(LichtAdresseSize, byteorder = 'little'))

lightgroup_offset = len(groupslist) * LichtMaxSize
for idxgroupslist in groupslist:
    fp.write(idxgroupslist.to_bytes(LichtMaxSize, byteorder = 'little'))

cs_offset = len(cslist) * LichtFarbTypeSize
for idxcslist in cslist:
    fp.write(idxcslist.to_bytes(LichtFarbTypeSize, byteorder = 'little'))

#save positions

positionslist = obj['positions']

actual_positions = len(positionslist)

#fill positions list up to positions_max
for i in range(0, positions_max - actual_positions):
    positionslist.append({'angle': 0, 'radius': 0})

anglelist = []
radlist = []

for idxpositions in positionslist:
    anglelist.append(idxpositions['angle'])
    radlist.append(idxpositions['radius'])

angle_offset = len(anglelist) * PosWinkelSize
for idxanglelist in anglelist:
    fp.write(idxanglelist.to_bytes(PosWinkelSize, byteorder = 'little'))

rad_offset = len(radlist) * PosRadiusSize
for idxradlist in radlist:
    fp.write(idxradlist.to_bytes(PosRadiusSize, byteorder = 'little'))

#save colors

colorslist = obj['colors']

actual_colors = len(colorslist)

#fill colors list up to lights_max
for i in range(0, colors_max - actual_colors):
    colorslist.append({'hue': 0, 'saturation': 0, 'value': 0})

huelist = []
satlist = []
vallist = []

for idxcolors in colorslist:
    huelist.append(idxcolors['hue'])
    satlist.append(idxcolors['saturation'])
    vallist.append(idxcolors['value'])

hue_offset = len(huelist) * FarbeHueSize
for idxhuelist in huelist:
    fp.write(idxhuelist.to_bytes(FarbeHueSize, byteorder = 'little'))

sat_offset = len(satlist) * FarbeSatSize
for idxsatlist in satlist:
    fp.write(idxsatlist.to_bytes(FarbeSatSize, byteorder = 'little'))

val_offset = len(vallist) * FarbeValSize
for idxvallist in vallist:
    fp.write(idxvallist.to_bytes(FarbeValSize, byteorder = 'little'))

#save limits

fp.write(delay_max.to_bytes(1, byteorder = 'little'))
fp.write(actual_scenes.to_bytes(1, byteorder = 'little'))
fp.write(actulal_animations.to_bytes(1, byteorder = 'little'))
fp.write(actual_lights.to_bytes(1, byteorder = 'little'))
fp.write(actual_colors.to_bytes(1, byteorder = 'little'))
fp.write(actual_positions.to_bytes(1, byteorder = 'little'))
fp.write(dimm2bin.to_bytes(1, byteorder = 'little'))

fp.flush()

fp.seek(DATASETLEN - CRC32LEN - 1, 0)
dummy = 0
fp.write(dummy.to_bytes(1, byteorder = 'little'))

fp.seek(0, 0)
filedata = fp.read(DATASETLEN - CRC32LEN)

crc32 = libscrc.crc32(filedata)

fp.seek(DATASETLEN - CRC32LEN, 0)
fp.write(crc32.to_bytes(CRC32LEN, byteorder = 'little'))

if method != None:
    fp.seek(0, 0)
    filedata = fp.read(DATASETLEN + CRC32LEN)

    sys.stdout.write("Content-type: application/octet-stream\n")
    sys.stdout.write("Content-Disposition: attachment; filename=\"dataset.bin\"\n\n")
    sys.stdout.flush()
    sys.stdout.buffer.write(filedata)
    sys.stdout.flush()

fpoffsets = open("offsets.h", "w")

print("/* This file is automatically generated by json2ds.py script */", file = fpoffsets)
print("#ifndef PARAM_OFFSETS_H_", file = fpoffsets)
print("#define PARAM_OFFSETS_H_", file = fpoffsets)
print("namespace ParamOffset", file = fpoffsets)
print("{", file = fpoffsets)
print("    enum ParamOffset", file = fpoffsets)
print("    {", file = fpoffsets)

offset = 0
print("        Date            = ", offset, "u,", file = fpoffsets, sep = '')

offset += date_len
print("        Version         = ", offset, "u,", file = fpoffsets, sep = '')

offset += 4
print("        Name            = ", offset, "u,", file = fpoffsets, sep = '')

offset += info_len
print("        EventsScene     = ", offset, "u,", file = fpoffsets, sep = '')

offset += event_scenes_offset
print("        SznGroup       = ", offset, "u,", file = fpoffsets, sep = '')

offset += group_offset
print("        SznSteps     = ", offset, "u,", file = fpoffsets, sep = '')

offset += scenelen_offset
print("        SznT            = ", offset, "u,", file = fpoffsets, sep = '')

offset += ssdelay_offset
print("        SznAnimation    = ", offset, "u,", file = fpoffsets, sep = '')

offset += ssaniid_offset
print("        AniTargetT        = ", offset, "u,", file = fpoffsets, sep = '')

offset += tt_offset
print("        AniTargetColor    = ", offset, "u,", file = fpoffsets, sep = '')

offset += tc_offset
print("        AniType          = ", offset, "u,", file = fpoffsets, sep = '')

offset += type_offset
print("        AniT            = ", offset, "u,", file = fpoffsets, sep = '')

offset += at_offset
print("        AniPosStart     = ", offset, "u,", file = fpoffsets, sep = '')

offset += stpos_offset
print("        AniPosEnd      = ", offset, "u,", file = fpoffsets, sep = '')

offset += endpos_offset
print("        AniRotDir       = ", offset, "u,", file = fpoffsets, sep = '')

offset += rotdir_offset
print("        LightPos        = ", offset, "u,", file = fpoffsets, sep = '')

offset += posid_offset
print("        LightOffType     = ", offset, "u,", file = fpoffsets, sep = '')

offset += lighttype_offset
print("        LightDriver    = ", offset, "u,", file = fpoffsets, sep = '')

offset += driver_offset
print("        LightDelay       = ", offset, "u,", file = fpoffsets, sep = '')

offset += delay_offset
print("        LightAddress    = ", offset, "u,", file = fpoffsets, sep = '')

offset += addr_offset
print("        LightMax        = ", offset, "u,", file = fpoffsets, sep = '')

offset += lightgroup_offset
print("        LightColorType    = ", offset, "u,", file = fpoffsets, sep = '')

offset += cs_offset
print("        PosAngle       = ", offset, "u,", file = fpoffsets, sep = '')

offset += angle_offset
print("        PosRadius       = ", offset, "u,", file = fpoffsets, sep = '')

offset += rad_offset
print("        ColorHue        = ", offset, "u,", file = fpoffsets, sep = '')

offset += hue_offset
print("        ColorSat        = ", offset, "u,", file = fpoffsets, sep = '')

offset += sat_offset
print("        ColorVal        = ", offset, "u,", file = fpoffsets, sep = '')

offset += val_offset
print("        DelayMax        = ", offset, "u,", file = fpoffsets, sep = '')

offset += 1
print("        NumScenes       = ", offset, "u,", file = fpoffsets, sep = '')

offset += 1
print("        NumAnimations  = ", offset, "u,", file = fpoffsets, sep = '')

offset += 1
print("        NumLuminaries     = ", offset, "u,", file = fpoffsets, sep = '')

offset += 1
print("        NumColors       = ", offset, "u,", file = fpoffsets, sep = '')

offset += 1
print("        NumPositions   = ", offset, "u,", file = fpoffsets, sep = '')

offset += 1
print("        Dimm2Bin        = ", offset, "u", file = fpoffsets, sep = '')

print("    };", file = fpoffsets)
print("};", file = fpoffsets)
print("#endif //PARAM_OFFSETS_H_", file = fpoffsets)

fpoffsets.close()
fp.close()
exit(0)
