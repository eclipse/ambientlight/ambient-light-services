/**
 * @file scenes.js
 * @brief Javascript to handle the HTML page 'Events' 
 * @author Boris Guzhov
 */


var event_list, scene_list, animation_list, sc_removed;
var $sc_table = $('#scene-table'), sc_table_content = [], currentSceneRow;
var $st_table = $('#step-table'), currentStepRow;

var modal, modal_mode;
const modal_mode_st_edit = 1, modal_mode_st_add = 2;


// Checks if the step list of a scene is empty
function isStepListEmpty(st_table_content) {
    if (st_table_content.length == 0) {
        st_bootstrap_alert.warning('The step list is empty');
        return true;
    }
    return false;
}

// Checks that a step is selected before adding/editing steps
function isStepSelected(message) {
    if (message == undefined)
        message = 'No step selected';
    if (currentStepRow == undefined) {
        st_bootstrap_alert.warning(message);
        return false;
    }
    return true;
}


// Checks that scene is selected before adding/editing steps
function isSceneSelected() {
    let ret = true;
    if ( currentSceneRow == undefined ) {
        // No selected scene
        ret = false;
    }
    else if ( scene_list[currentSceneRow.sc_id].name == "" ) { 
        // Empty scen selected
        ret = false;
    }
    else if (sc_removed == true) {
        ret = false;
    }

    if (ret == false) {
        st_bootstrap_alert.warning('A valid scene must be selected first');
    }

    return ret;
}

function isSceneListEmpty() {
    s = 0;

    for (sc=0; sc < sc_table_content.length; sc++) {
        if (sc_table_content[sc].sc_name != "" ||  sc_table_content[sc].sc_group != 0) {
            s++;
        }
    }

    return s > 0 ? false : true;   
}


function highlight_table_row(table, row) {
    let rows = document.getElementById(table).getElementsByTagName("tr");

    if (table == "scene-table" && isSceneListEmpty() )
        return;

    rows[++row].className = "success";
}

function fill_step_animation_option() {
    let step_animation = document.getElementById("st-edit-step-animation");
    for (ani=0; ani < animation_list.length; ani++) {
        if (animation_list[ani].name != "") {
            let a = ani + " - " + animation_list[ani].name;
            step_animation.options[step_animation.options.length] = new Option(a, ani);
        }
    }
}



// Update  dataset with added scenes and redraw tables
function update_added_scenes(sc) {
    scene_list[sc].name = sc_table_content[sc].sc_name;
    scene_list[sc].group = parseInt(sc_table_content[sc].sc_group);

    fill_step_table_content(sc);

    $sc_table.bootstrapTable('load', sc_table_content);
    highlight_table_row("scene-table", currentSceneRow.sc_id);
}

// get the list of events to which the scene is mapped
function get_scene_events() {
    let event_string = "";
    let sc_id = currentSceneRow.sc_id;

    for (ev=0; ev < event_list.length; ev++) {
        if (event_list[ev].event == "")
            continue;

        for (es=0; es < event_list[ev].scenes.length; es++) {
            if (event_list[ev].scenes[es] == sc_id) {
                event_string +=  ev + " - '" + event_list[ev].event + "'<br>";
            }
        }
    }
    return event_string;
}

// Update  dataset with removed scenes and redraw tables
function update_removed_scenes() {
    for (sc=0; sc < sc_table_content.length; sc++) {
        scene_list[sc].name = sc_table_content[sc].sc_name;
        scene_list[sc].group = sc_table_content[sc].sc_group == "" ? 0 : sc_table_content[sc].sc_group;

        // clear substebs and also unmap events if any
        if (sc_table_content[sc].sc_name == "" && sc_table_content[sc].sc_group == 0) {

            // clear substebs
            //scene_list[sc].substeps.splice(0,scene_list[sc].substeps.length);
            scene_list[sc].substeps.length = 0;
            sc_table_content[sc].sc_steps = "";

            //unmap the removed scene 
            for (ev=0; ev < event_list.length; ev++) {
                if (event_list[ev].event == "")
                    continue;

                for (es=0; es < event_list[ev].scenes.length; es++) {
                    if (event_list[ev].scenes[es] == sc)
                        event_list[ev].scenes[es] = 255;
                }
            }
        }
    }
    
    $sc_table.bootstrapTable('load', sc_table_content);
    highlight_table_row("scene-table", currentSceneRow.sc_id);
    
    fill_step_table_content(currentSceneRow.sc_id);
}


// Update  dataset with reordered scenes and redraw tables
function update_reordered_scenes() {
    let changed_list = [];
    
    find_changes();
    fix_mapping();
    reorder_scene_list();

    $sc_table.bootstrapTable('load', sc_table_content);
    highlight_table_row("scene-table", currentSceneRow.sc_id);
    fill_step_table_content(currentSceneRow.sc_id);

    // Find and keep changes into changed_list[]
    function find_changes() {
        let index = 0;
        for (sc=0; sc < sc_table_content.length; sc++) {
            if (sc != sc_table_content[sc].sc_id && sc_table_content[sc].sc_name != "" ) {  // Scene Id changed
                changed_list[index] = { old_id: sc_table_content[sc].sc_id, new_id: sc };
                index++;
            }
        }
    }

    // fix event mapping
    function fix_mapping() {
        // Fix event mapping
        for (ev=0; ev < event_list.length; ev++) {
            if (event_list[ev].event == "")
                continue;

            for (es=0; es < event_list[ev].scenes.length; es++) {
                for (ch=0; ch < changed_list.length; ch++) {
                    if (changed_list[ch].old_id == event_list[ev].scenes[es]) {
                        event_list[ev].scenes[es] = changed_list[ch].new_id;
                        break;
                    }
                }
            }
        }
    }
    
    // Update  scene list (it's needed because the rows in sc_table_content reordered)
    function reorder_scene_list() {
        for (sc=0; sc < sc_table_content.length; sc++) {
            sc_table_content[sc].sc_id = sc; // fix id if rows reordered

            scene_list[sc].name = sc_table_content[sc].sc_name;
            scene_list[sc].group = sc_table_content[sc].sc_group == "" ? 0 : sc_table_content[sc].sc_group;

            let st_table_content = sc_table_content[sc].st_table_content;
            let substeps = scene_list[sc].substeps;

            substeps.length = 0;
            
            for (st=0; st<st_table_content.length; st++) {
                substeps[st] = { delay: 0, animation_id: 0 };
                substeps[st].delay = st_table_content[st].st_delay / currentDataset.units.time_unit;
                substeps[st].animation_id = parseInt(st_table_content[st].st_ani_id);
            }
        }
    }
}


function fill_scene_table_content() {
    fill_step_animation_option();
    for (sc=0; sc < scene_list.length; sc++) {
        sc_table_content[sc] = { sc_id: 0, sc_name: "", sc_group: "", sc_steps: "", st_table_content:[] };
        sc_table_content[sc].sc_id = sc;
        sc_table_content[sc].sc_name = scene_list[sc].name;

        if (scene_list[sc].group != 0) {
            sc_table_content[sc].sc_group = scene_list[sc].group;
            sc_table_content[sc].sc_steps = scene_list[sc].substeps.length;
        }

        fill_step_table_content(sc);
    }
}


// fill substep table for selected scene and redraw step table
function fill_step_table_content(sc) {
    let st_table_content = sc_table_content[sc].st_table_content;
    st_table_content.length = 0;
    for (step=0; step < scene_list[sc].substeps.length; step++) {
        st_table_content[step] = { st_id: 0, st_delay: 0, st_ani_name: "", st_ani_id: 0, st_ani_string: "" };
        st_table_content[step].st_id = step;
        st_table_content[step].st_delay = scene_list[sc].substeps[step].delay * currentDataset.units.time_unit;
        st_table_content[step].st_ani_id = scene_list[sc].substeps[step].animation_id;
        st_table_content[step].st_ani_name = animation_list[scene_list[sc].substeps[step].animation_id].name;
        st_table_content[step].st_ani_string = "" + st_table_content[step].st_ani_id + ' - ' + st_table_content[step].st_ani_name;
    }

    sc_table_content[sc].sc_steps = (scene_list[sc].name == "") ? "" : st_table_content.length;
    
    $st_table.bootstrapTable('load', st_table_content);
}

function update_steps() {
    scene_list[currentSceneRow.sc_id].substeps.length = 0;
    let sc_table_row = sc_table_content[currentSceneRow.sc_id];
    let st_table_content = sc_table_row.st_table_content;

    for (st=0; st<st_table_content.length; st++) {
        st_table_content[st].st_id = st;
        scene_list[currentSceneRow.sc_id].substeps[st] = { delay: 0, animation_id: 0 };
        scene_list[currentSceneRow.sc_id].substeps[st].delay = st_table_content[st].st_delay / currentDataset.units.time_unit;
        scene_list[currentSceneRow.sc_id].substeps[st].animation_id = parseInt(st_table_content[st].st_ani_id);
    }

    // Redraw the table
    $st_table.bootstrapTable('load', st_table_content);
    if (currentStepRow != undefined) {
        highlight_table_row("step-table", currentStepRow.st_id);
    }

    // update nsteps in sc table row
    sc_table_row.sc_steps = st_table_content.length;
    $sc_table.bootstrapTable('updateRow', {index: currentSceneRow.sc_id, row: sc_table_row});
    highlight_table_row("scene-table", currentSceneRow.sc_id);
}

// Warning/error message
sc_bootstrap_alert = function () { }
sc_bootstrap_alert.warning = function (message) {
    $('#sc-alert-placeholder').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>')
}
// Warning/error message
st_bootstrap_alert = function () { }
st_bootstrap_alert.warning = function (message) {
    $('#st-alert-placeholder').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>')
}

// The function fills the fields in 'Scenes' page from localStorage
$(document).ready(function () {
    if (getDatasetFromLocalStorage()) {
        sc_removed = false;
        event_list = currentDataset.event_scenes;
        scene_list = currentDataset.scenes;
        animation_list = currentDataset.animations;

        // Set current scene
        fill_scene_table_content();
        $sc_table.bootstrapTable({ data: sc_table_content});
        currentSceneRow = sc_table_content[0];
        highlight_table_row("scene-table", 0);

        // Set current step
        $st_table.bootstrapTable({ data: [] });
        fill_step_table_content(0);
        if(sc_table_content[0].sc_name != "" && sc_table_content[0].sc_steps > 0) {
            currentStepRow = sc_table_content[0].st_table_content[0];
            highlight_table_row("step-table", 0);
        } else {
            currentStepRow = undefined;
        }
        
        $sc_table.on('click-row.bs.table', function (e, row, $element) {
            $('.success').removeClass('success');
            $($element).addClass('success');
            $('.success').css("background-color", "white");
            currentSceneRow = row;
            currentStepRow = undefined;

            // fill substep table for selected scene
            fill_step_table_content(currentSceneRow.sc_id);
            sc_removed = false;
        });
        
        $sc_table.on('reorder-row.bs.table', function (e, table, $element) {
            console.log("Reorder");
            sc_table_content.length = 0;
            sc_table_content = table;
            currentSceneRow = $element;
            update_reordered_scenes();
        })

        $st_table.on('click-row.bs.table', function (e, row, $element) {
            $('.success').removeClass('success');
            $($element).addClass('success');
            $('.success').css("background-color", "white");
            currentStepRow = row;
            highlight_table_row("scene-table", currentSceneRow.sc_id);
        });

        $st_table.on('reorder-row.bs.table', function (e, table, $element) {
            st_table_content = sc_table_content[currentSceneRow.sc_id];
            st_table_content.length = 0;
            st_table_content = table;
            currentStepRow = $element;
            update_steps();
            
        })
    }

    // Scene [Edit] button
    $("#sc-edit").click(function () {
        if (currentSceneRow == undefined || sc_removed == true) {
            sc_bootstrap_alert.warning('No scene selected');
            return;
        }

        modal = $("#sc-edit-modal");
        $("#sc-edit-scene-name").val(scene_list[currentSceneRow.sc_id].name);
        modal.find('.modal-title').text('Scene ' + currentSceneRow.sc_id);
        modal.modal();

        $('#sc-save-scene-name').off('click').on('click',  function (ev) {
            if ($("#sc-edit-scene-name").val() == "") {
                sc_bootstrap_alert.warning('Scene ' + currentSceneRow.sc_id + ' not saved: - scene name must be filled!');
                return;
            }
            
            sc_table_content[currentSceneRow.sc_id].sc_name = $("#sc-edit-scene-name").val();
            
            // Fill the group (fixme in the future)
            sc_table_content[currentSceneRow.sc_id].sc_group = 1;

            update_added_scenes(currentSceneRow.sc_id);
        });
    });

    $("#sc-remove").click(function () {
        if (currentSceneRow == undefined) {
            sc_bootstrap_alert.warning('No scene selected');
            return;
        }

        if (currentSceneRow.sc_name == "") {
            sc_bootstrap_alert.warning('Scene doesn\'t exist');
            return;
        }

        
        modal = $("#sc-remove-modal");
        modal.find('.modal-title').text('Remove the scene ' + currentSceneRow.sc_id + '?' );

        let ev_str = get_scene_events();
        if ( ev_str != "" )  {
            let msg = "The scene " +
                currentSceneRow.sc_id +
                " is still mapped to following event(s):<br>" +
                ev_str +
                "<hr><bp>If you click <span class='text-success'>&nbspYes&nbsp</span>, the scene will be automaticly unmapped<br>" +
                "from the event(s) above and then removed!<br>" + 
                "Click <span class='text-danger'>&nbspNo&nbsp</span> if you want to keep the scene.<br>";

            modal.find('.modal-body').html(msg);
        }
        else {
            modal.find('.modal-body').text("Are you sure?");
        }
        modal.modal();

        $('#sc-remove-scene').off('click').on('click', function (ev) {
            remove_scene();

            function remove_scene() {
                sc_table_content[currentSceneRow.sc_id].sc_name = "";
                sc_table_content[currentSceneRow.sc_id].sc_group = "";
                update_removed_scenes();
                sc_removed = true;
            }
        });
    });


    $("#st-edit").click(function () {
        let st_table_content = sc_table_content[currentSceneRow.sc_id].st_table_content;

        // At first a Scene must be selected 
        if (isSceneSelected() == false) {
            return;
        }

        if (isStepListEmpty(st_table_content) == true) {
            return;
        }

        if (isStepSelected() == false) {
            return;
        }

        modal_mode = modal_mode_st_edit;

        modal = $("#st-edit-modal");
        $("#st-edit-step-delay").val(st_table_content[currentStepRow.st_id].st_delay);
        $("#st-edit-step-animation").val(st_table_content[currentStepRow.st_id].st_ani_id);
        modal.find('.modal-title').text('Edit Step ' + currentStepRow.st_id);
        modal.modal();
    });

    $('#st-edit-step-form').submit(function (ev) {
        modal.modal('toggle');

        if ($("#st-edit-step-animation").val() == undefined ) {
            st_bootstrap_alert.warning('No animation selected');
            return false;
        }

        let item_index;
        let st_table_content = sc_table_content[currentSceneRow.sc_id].st_table_content;

        // If 'Add' button is clicked()
        if (modal_mode == modal_mode_st_add) {
            if (currentStepRow != undefined) {
                item_index = currentStepRow.st_id;

                // new step is after selected step 
                item_index++;
            }
            else {
                // add first step to the scene step list
                item_index = 0;
            }
            // Add new item
            st_table_content.splice(item_index, 0, { st_id: item_index, st_delay: "", st_ani_name: "", st_ani_id: "" });
        }
        else {
            // If 'Edit' button is clicked()
            item_index = currentStepRow.st_id;
        }

        st_table_content[item_index].st_delay = $("#st-edit-step-delay").val();
        st_table_content[item_index].st_ani_id = $("#st-edit-step-animation").val();
        st_table_content[item_index].st_ani_name = animation_list[$("#st-edit-step-animation").val()].name;
        st_table_content[item_index].st_ani_string = "" + st_table_content[item_index].st_ani_id + ' - ' + st_table_content[item_index].st_ani_name;

        currentStepRow = st_table_content[item_index];

        update_steps();

        return false;
    });


    $("#st-remove").click(function () {
        st_table_content = sc_table_content[currentSceneRow.sc_id].st_table_content;
        
        if (isSceneSelected() == false) {
            return;
        }

        if (isStepListEmpty(st_table_content) == true) {
            return;
        }

        if (isStepSelected() == false) {
            return;
        }

        modal = $("#st-remove-modal");
        modal.find('.modal-title').text('Remove the step ' + currentStepRow.st_id);
        modal.modal();
        
        $('#st-remove-step').off('click').on('click', function (ev) {

            // remove current row
            st_table_content.splice(currentStepRow.st_id, 1);

            // Calculate step row to be selected
            if (st_table_content.length > 0) {
                let id = currentStepRow.st_id;
                if( id >= st_table_content.length ) {
                    currentStepRow = st_table_content[--id];
                }
                else {
                    currentStepRow = st_table_content[id];
                }
            }
            else {
                currentStepRow = undefined;
            }

            update_steps();
        });
    });

    $("#st-add").click(function () {

        if (isSceneSelected() == false) {
            return;
        }

        // check steps' limit
        if ( scene_list[currentSceneRow.sc_id].substeps.length >= currentDataset.limits.substeps_max ) {
            st_bootstrap_alert.warning('The step list is full');
            return;
        }

        let st_id;

        // Check the step list is not empty
        if (scene_list[currentSceneRow.sc_id].substeps.length != 0) {
            // If there is at least one step, a step must be selected
            if (isStepSelected("Select the step after which you want to add a new step") == false) {
                return;
            }
            st_id =  currentStepRow.st_id;
        }
        else {
            // For empty step list
            st_id =  0;
        }

        modal_mode = modal_mode_st_add;

        $("#st-edit-step-animation").val("Select animation");
        modal = $("#st-edit-modal");
        modal.find('.modal-title').text('Add a new step after the step ' + st_id);
        modal.modal();
    });
});

//# sourceURL=scenes.js
