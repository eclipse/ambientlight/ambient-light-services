/**
 * @file project.js
 * @brief Javascrtipt to handle the page 'Project' 
 * @author: Boris Guzhov
 */

var prj_name;
var prj_info;
var prj_version_major;
var prj_version_minor;
var prj_mod_date;
var prj_offsetsh;

// Warning/error message
project_alert_w = function () { }
project_alert_w.warning = function (message) {
    $("#project-alert-placeholder").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>');
}
project_alert_ok = function () { }
project_alert_ok.success = function (message) {
    $("#project-alert-placeholder").html('<div class="alert alert-success"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>');
}
project_gen_ok = function () { }
project_gen_ok.success = function (message) {
    $("#project-gen-placeholder").html('<div class="alert alert-success"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>');
}
project_gen_w = function () { }
project_gen_w.warning = function (message) {
    $("#project-gen-placeholder").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>');
}



function loadProjectInfo()
{
    //console.log(currentDatasetStatus, currentDataset);
}

function fillProjectInfo()
{
    
    prj_name = document.getElementById("prj_name");
    prj_info = document.getElementById("prj_info");
    prj_version_major = document.getElementById("prj_version_major");
    prj_version_minor = document.getElementById("prj_version_minor");
    prj_mod_date = document.getElementById("prj_mod_date");

    if (currentDataset == undefined ) {
        prj_name.value = "";
        prj_info.value = "";
        prj_version_major.value = "";
        prj_version_minor.value = "";
        prj_mod_date.value = "";
    }
    else {
        // limits
        prj_name.maxLength = 64;
        prj_version_major.maxLength = currentDataset.limits.ver_major_len;
        prj_version_minor.maxLength = currentDataset.limits.ver_minor_len;
        prj_info.maxLength = currentDataset.limits.info_len;
        prj_mod_date.maxLength = currentDataset.limits.date_len;
        
        prj_name.value = currentDataset.name;
        prj_info.value = currentDataset.info;
        prj_version_major.value = currentDataset.version.major;
        prj_version_minor.value = currentDataset.version.minor;
        prj_mod_date.value = currentDataset.date;
    }
    
}

function check_ver(ver, str) {
    if ( ver == "") {
        project_alert_w.warning("The " + str + " version number must be between 0 and 99.");
        return false;
    }
    return true;
}

function getProjectInfo() {
    prj_name = document.getElementById("prj_name");
    prj_info = document.getElementById("prj_info");
    prj_version_major = document.getElementById("prj_version_major");
    prj_version_minor = document.getElementById("prj_version_minor");
    prj_mod_date = document.getElementById("prj_mod_date");

    currentDataset.name = prj_name.value;
    currentDataset.info = prj_info.value;

    if (check_ver(prj_version_major.value, "major")  )
        currentDataset.version.major = prj_version_major.value;
    else
        prj_version_major.value = currentDataset.version.major;
    
    if (check_ver(prj_version_minor.value, "minor"))
        currentDataset.version.minor = prj_version_minor.value;
    else
        prj_version_minor.value = currentDataset.version.minor;

    currentDataset.date = prj_mod_date.value;
}

// Button 'New'
function createNewProject() {
    var c = true;
    if (currentDataset != undefined) {
        if (currentDataset.project_state.saved == false) {
            var modal = $("#confirm-continue-modal");
            c = false;
            modal.modal();

            $("#confirm-continue").off('click').on('click', function () {
                createPrj()
            });
        }
    }
        
    if (c == true ) {
        createPrj()
    }

    function createPrj() {
        $.getJSON("scripts/default-dataset.json", function(json) {
            msg = validateDataset(json);

            if ( msg != "" ) {
                project_alert_w.warning("Bad project template 'scripts/default-dataset.json': " + msg);
            }
            else {
                currentDataset = json;
                
                fillProjectInfo();
                // The project is not saved at the moment
                currentDataset.project_state.saved = false;
                project_alert_ok.success("New project has been created.");

                // remove outdated project from local storage
                localStorage.removeItem("LaSDataset");

                apply_project_settings();
            }
        });
    }
}


// Button 'Save'
function saveProject() {
    if(currentDataset == undefined) {
        project_alert_w.warning("First create or open a project");
        return;
    }

    fillDate();
    updateSavedStatus(true);
    let sDs = JSON.stringify(currentDataset);
    let file = new Blob([sDs], {type: "application/json"});
    let a = document.createElement("a"), // a reference
    url = URL.createObjectURL(file);
    a.href = url;
    a.download = currentDataset.name;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);  
    }, 0);
}

function fillDate()
{
    let date = new Date(); 
    currentDataset.date = date.toUTCString();
    fillProjectInfo();
}

// Button 'Close'
function closeProject() {
    if(currentDataset == undefined) {
        project_alert_w.warning("No open project");
        return;
    }
    var c = true;
    if (currentDataset != undefined) {
        if (currentDataset.project_state.saved == false) {
            var modal = $("#confirm-continue-modal");
            c = false;
            modal.modal();

            $("#confirm-continue").off('click').on('click', function () {
                closePrj()
            });
        }
    }
        
    if (c == true ) {
        closePrj()
    }

    function closePrj() {
        currentDataset = undefined;
        localStorage.removeItem("LaSDataset");
        fillProjectInfo();
        apply_default_project_settings();
    }
}

// Button 'Generate'
function generateProject() {
    if(currentDataset == undefined) {
        project_alert_w.warning("First create or open a project");
        return;
    }
    msg = validateDataset(currentDataset);
    if( msg != "" ) {
        project_alert_w.warning("Bad dataset data: " + msg);
        return;
    }

    var ds_binary=[];
    ds_binary = json2ds(currentDataset);
    
    var modal = $("#generate-dataset-modal");
    modal.modal();

    $("#generate-ds-binary").off('click').on('click', function (ev) {
        let file = new Blob([ds_binary], {type: "application/octet-binary"});
        let a = document.createElement("a"); // a reference
        url = URL.createObjectURL(file);
        a.href = url;
        a.download = currentDataset.name+'.bin';
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0);
    });
    
   $("#generate-ds-header").off('click').on('click', function (ev) {
        let file = new Blob([prj_offsetsh], {type: "text/plain"});
        let a = document.createElement("a"); // a reference
        url = URL.createObjectURL(file);
        a.href = url;
        a.download = "ParamOffset.h";
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0);
    });
}

function updateSavedStatus(saved_status)
{
    currentDataset.project_state.saved = saved_status;
    saved_ds_string = JSON.stringify(window.currentDataset);
}


// The function fills the fields in 'Project' page from localStorage
$(document).ready(function () {
    if (getDatasetFromLocalStorage()) {
        fillProjectInfo();
    }

    $('#open-project-button').off('click').on('click', function (ev) {
        tryToOpenProject();
    });

    // The function reads the selected file (button "Open")
    $('input[id="open-project-input"]').change(function (e) {
        openProjectFile(e.target.files[0]);
        event.target.value = '';  // clear event to be able reopen the same file
    });

    // The function updates currentDataset if any 'Project' field has been changed.
    $(document).change(function () {
        if( currentDataset == undefined ) {
            fillProjectInfo();
            project_alert_w.warning("Please create a new project or open an existing one before any changes!");
            return;
        }
        getProjectInfo();
        updateSavedStatus(false);
    });  
});


function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


// File input - it's called in  tryToOpenProject by $('#open-project-input').click()
function openProjectFile(file) {
    fr = new FileReader();
    fr.onload = receivedText;
    fr.readAsText(file);

    project_alert_ok.success();
    
    function receivedText(e) {
        let lines = e.target.result;
        currentDataset = undefined;

        if ( !isJson(lines) ) {
            project_alert_w.warning("The file '" + file.name + "' you are trying to open doesn't contain JSON data!");
            return;
        }
        
        msg = validateDataset(JSON.parse(lines));
        
        if ( msg != "" ) {
            project_alert_w.warning("The project file '" + file.name + "' you are trying to open contans an error: " + msg + "!");
            return;
        }
     
        currentDataset = JSON.parse(lines);
        project_alert_ok.success("The project file '" + file.name + "' is opened successfully");
        
        fillProjectInfo();
        updateSavedStatus(true);

        // remove outdated project from local storage
        localStorage.removeItem("LaSDataset");

        apply_project_settings();
    }
}

// Button 'Open'
function tryToOpenProject() {
    if ( (currentDataset != undefined) && (currentDataset.project_state.saved == false)) {
        var modal = $("#confirm-continue-modal");
        modal.modal();

        $("#confirm-continue").off('click').on('click', function () {
            $('#open-project-input').click();
        });
    }
    else {
        $('#open-project-input').click();
    }

}


// Validate DS before generating
function validateDataset(ds) {
    var limits = ds.limits;
    var events = ds.event_scenes;
    var scenes = ds.scenes;
    var animations = ds.animations;
    var colors = ds.colors;

    if ( (ds == undefined) ||
         (limits == undefined) ||
         (events == undefined) ||
         (scenes == undefined) ||
         (animations == undefined) ||
         (colors == undefined) )
    {
        return "Bad dataset data";
    }

    msg =  CheckLimits();

    return msg;
    
    function CheckLimits() {
        if ( events.length != limits.events_max )
            return "Event list contains wrong numbers of items: " + events.length + " (it must be " + limits.events_max + ")."
        if ( scenes.length != limits.scenes_max )
            return "Scene list contains wrong numbers of items: " + scenes.length + " (it must be " + limits.scenes_max + ")."
        if ( animations.length != limits.animations_max )
            return "Animation list contains wrong numbers of items: " + animatons.length + " (it must be " + limits.anumations_max + ")."
        if ( colors.length != limits.colors_max )
            return "Color list contains wrong numbers of items: " + colors.length + " (it must be " + limits.colors_max + ")."

        msg = checkEvents();
        if ( msg != "" )
            return msg;

        msg = checkScenes();
        if ( msg != "" )
            return msg;

        msg = checkAnimations();
        if ( msg != "" )
            return msg;

        msg = checkColors();
        if ( msg != "" )
            return msg;

        return "";
    }
    

    function checkEvents() {
        for(e=0; e < events.length; e++) {
            sN = events[e].scenes.length;
            
            if (sN != 4)
                return "Event "+ e +" contains wrong number of items: " + sN + " (it must be 4).";

        }
        return "";
    }

    function checkScenes() {
        for(s=0; s < scenes.length; s++) {

            if (scenes[s].substeps.length > limits.substeps_max)
                return s + "-scene '" + scenes[s].name + "' contains too much steps (" +  scenes[s].substeps.length + ").";

            if (scenes[s].name != "" && scenes[s].substeps.length == 0)
                return s + "-scene '" + scenes[s].name + "' doesn't contain any steps";
        }

        return "";
    }
    
    function checkAnimations() {return "";}
    function checkColors() {return "";}
}


//# sourceURL=projects.js

//document.getElementById('openProject').click();
