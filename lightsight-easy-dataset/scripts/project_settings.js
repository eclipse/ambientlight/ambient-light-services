/**
 * @file project_settings.js
 * @brief Javascrtipt to handle the dialog 'Project settings' 
 * @author: Boris Guzhov
 */

function apply_project_settings()
{
    if ( currentDataset.project_settings == undefined )
        return;
    
    // Apply tooltip settings
    if ( currentDataset.project_settings.tooltips != undefined ) {
        if (currentDataset.project_settings.tooltips.enabled == true)  {
            $('[data-toggle="tooltip"]').tooltip( {'trigger':'hover', 'delay': { 'show': '400', 'hide': '100' } });
            $('[data-toggle="tooltip"]').tooltip('enable');
        }
        else {
            $('[data-toggle="tooltip"]').tooltip('disable');
        }
    }
}


function  apply_default_project_settings() {
    $('[data-toggle="tooltip"]').tooltip('disable');
}

function projectSettings() {
    let tooltipMode;
    var modal = $("#project-settings-modal"), mode;
    modal.find('.modal-title').text('Project settings');

    if(currentDataset == undefined) {
        project_alert_w.warning("First create or open a project");
        return;
    }

    tooltipMode = tooltipOption();
    
    modal.modal();

    function tooltipOption() {
        mode  = currentDataset.project_settings.tooltips.enabled;

        $('#tooltipSwitch').prop('checked', mode);
        return mode;
    }

    $('#project-setting-form').submit(function (ev) {
        modal.modal('toggle');
        
        currentDataset.project_settings.tooltips.enabled = $('#tooltipSwitch').prop('checked');
        apply_project_settings();

        return false;
    });
}
