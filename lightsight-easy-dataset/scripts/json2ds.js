/**
 * @file json2ds.js
 * @brief Javascrtipt to generate dataset binary ready for flashing from the project JSON data
 * @author: Dmitrii Fedotov
 */

function crc32(bytes, crc) {

    var crc32table = "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D";

    var n = 0;
    var x = 0;

    crc = crc ^ (-1);
    
    for( let i = 0; i < (bytes.byteLength - 4); i++ ) { 
        n = ( crc ^ bytes.getUint8(i) ) & 0xFF; 
        x = "0x" + crc32table.substr( n * 9, 8 ); 
        crc = ( crc >>> 8 ) ^ x; 
    } 

    crc = crc ^ (-1)
    if (crc < 0) {crc += 4294967296}

    return crc; 
}

function json2ds(json){

    var dslen = 2564;
    var crclen = 4;

    var EventSceneSize     = 1;

    var SznGruppeSize      = 1;
    var SznSchritteSize    = 1; 
    var Szn_tSize          = 1;
    var Szn_AnimationSize  = 1;

    var AniZiel_tSize      = 2;
    var AniZiel_FabreSize  = 1;
    var AniTypeSize        = 1;
    var Anit_tSize         = 2;
    var AniPosStartSize    = 1;
    var AniPosEndeSize     = 1;
    var AniRotDirSize      = 1;

    var AniLichtPosSize    = 1;
    var LichtAusTypeSize   = 1;
    var LichtTreiberSize   = 1;
    var LichtVerzSize      = 1;
    var LichtAdresseSize   = 1;
    var LichtMaxSize       = 1;
    var LichtFarbTypeSize  = 1;

    var PosWinkelSize      = 2;
    var PosRadiusSize      = 2;

    var FarbeHueSize       = 2;
    var FarbeSatSize       = 1;
    var FarbeValSize       = 1;

    var DelayMaxSize       = 1;
    var AnzSzenenSize      = 1;
    var AnzAnimationenSize = 1;
    var AnzLeuchtenSize    = 1;
    var AnzFarbenSize      = 1;
    var AnzPositionenSize  = 1;

    var obj = json;

    let limits = obj.limits;

    var delay_max      = limits.delay_max;
    var events_max     = limits.events_max;
    var scenes_max     = limits.scenes_max;
    var substeps_max   = limits.substeps_max;
    var animations_max = limits.animations_max;
    var lights_max     = limits.lights_max;
    var colors_max     = limits.colors_max;
    var positions_max  = limits.positions_max;
    var dimm2bin       = limits.dimm2bin;

    var date_len       = limits.date_len;
    var info_len       = limits.info_len;

    //var name = obj.name;
    var info = obj.info;

    var date = obj.date;

    let version = obj.version;

    var majorver = version.major;
    var minorver = version.minor;

    var event_scenes_offset = 0;
    let event_scenes = obj.event_scenes;

    var event_scenes_list = new Array();

    for(let i = 0; i < event_scenes.length; i++) {
        let es = event_scenes[i].scenes;
        let scenes = (typeof es == 'undefined') ? event_scenes[i] : event_scenes[i].scenes;

        for(let j = 0; j < scenes.length; j++) {
            event_scenes_offset += EventSceneSize;
            event_scenes_list.push(scenes[j]);
        }
    }

    let sceneslist = obj.scenes;
    let total_scenes = sceneslist.length;
    
    var actual_scenes = scenes_max;
    
    for(let i = 0; i < (scenes_max - total_scenes); i++ ) {
        sceneslist.push({"group": 0, "substeps": []});
    }

    var grouplist = new Array();
    var substeplist = new Array();
    var scenelenlist = new Array();

    var delaylist = new Array();
    var aniidlist = new Array();
    
    for(let i = 0; i < scenes_max; i++) {
        let group = sceneslist[i].group;
        let substeps = sceneslist[i].substeps;
        
        grouplist.push(group);
        substeplist.push(substeps);

        let substeps_len = substeps.length;
        scenelenlist.push(substeps_len);

        if(0 == substeps_len) {
            for(let j = 0; j < substeps_max; j++) {
                delaylist.push(0);
                aniidlist.push(0);
            }
        }
        else {
            for(let j = 0; j < substeps_len; j++) {
                delaylist.push(substeps[j].delay);
                aniidlist.push(substeps[j].animation_id);
            }
            for(let j = 0; j < (substeps_max - substeps_len); j++) {
                delaylist.push(0);
                aniidlist.push(0);
            }
        }
    }

    var group_offset = grouplist.length * SznGruppeSize;
    var scenelen_offset = scenelenlist.length * SznSchritteSize;
    var ssdelay_offset = delaylist.length * Szn_tSize;
    var ssaniid_offset = aniidlist.length * Szn_AnimationSize

    var ttlist = new Array();
    var tclist = new Array();
    var typelist = new Array();
    var atlist = new Array();
    var stposlist = new Array();
    var enposlist = new Array();
    var rotdirlist = new Array();
    
    let animationslist = obj.animations;
    var actual_animations = animationslist.length;

    for(let i = 0; i < (animations_max - actual_animations); i++) {
        animationslist.push({"target_time": 0, "target_color": 0, "type": 0, "animation_time': 0, 'start_position_id": 0, "end_position_id": 0, "rotation_direction": 0});
    }

    for(let i = 0; i < animations_max; i++) {
        ttlist.push(animationslist[i].target_time);
        tclist.push(animationslist[i].target_color);
        typelist.push(animationslist[i].type);
        atlist.push(animationslist[i].animation_time);
        stposlist.push(animationslist[i].start_position_id);
        enposlist.push(animationslist[i].end_position_id);
        rotdirlist.push(animationslist[i].rotation_direction);
    }

    var tt_offset = ttlist.length * AniZiel_tSize;
    var tc_offset = tclist.length * AniZiel_FabreSize;
    var type_offset = typelist.length * AniTypeSize;
    var at_offset = atlist.length * Anit_tSize;
    var stpos_offset = stposlist.length * AniPosStartSize;
    var endpos_offset = enposlist.length * AniPosEndeSize;
    var rotdir_offset = rotdirlist.length * AniRotDirSize;

    let lightslist = obj.lights;
    var actual_lights = lightslist.length;

    for(let i = 0; i < (lights_max - actual_lights); i++) {
        lightslist.push({"position_id": 0, "type": 0, "driver": 0, "delay": 0, "address": 0, "groups": [0, 0, 0, 0, 0, 0, 0, 0], "color_space": 0});
    }

    var lightposidlist = new Array();
    var lighttypelist = new Array();
    var lightdriverlist = new Array();
    var lightdelaylist = new Array();
    var lightaddrlist = new Array();
    var lightgroupslist = new Array();
    var lightcslist = new Array();

    for(let i = 0; i < lights_max; i++) {
        lightposidlist.push(lightslist[i].position_id);
        lighttypelist.push(lightslist[i].type);
        lightdriverlist.push(lightslist[i].driver);
        lightdelaylist.push(lightslist[i].delay);
        lightaddrlist.push(lightslist[i].address);

        let groups = lightslist[i].groups;

        for(let j = 0; j < groups.length; j++) {
            lightgroupslist.push(groups[j]);
        }

        lightcslist.push(lightslist[i].color_space);
    }

    var lightposid_offset = lightposidlist.length * AniLichtPosSize;
    var lighttype_offset = lighttypelist.length * LichtAusTypeSize;
    var lightdriver_offset = lightdriverlist.length * LichtTreiberSize;
    var lightdelay_offset = lightdelaylist.length * LichtVerzSize;
    var lightaddr_offset = lightaddrlist.length * LichtAdresseSize;
    var lightgroup_offset = lightgroupslist.length * LichtMaxSize;
    var lightcs_offset = lightcslist.length * LichtFarbTypeSize;
    
    let positionslist = obj.positions;
    var actual_positions = positionslist.length;

    for(let i = 0; i < (positions_max - actual_positions); i++) {
        positionslist.push({"angle": 0, "radius": 0});
    }

    var anglelist = new Array();
    var radlist = new Array();

    for(let i = 0; i < positions_max; i++) {
        anglelist.push(positionslist[i].angle);
        radlist.push(positionslist[i].radius);
    }

    var angle_offset = anglelist.length * PosWinkelSize;
    var rad_offset = radlist.length * PosRadiusSize;

    let colorslist = obj.colors;
    var actual_colors = colorslist.length;

    for(let i = 0; i < (colors_max - actual_colors); i++) {
        colorslist.push({"hue": 0, "saturation": 0, "value": 0});
    }

    var huelist = new Array();
    var satlist = new Array();
    var vallist = new Array();

    for(let i = 0; i < colors_max; i++) {
        huelist.push(colorslist[i].hue);
        satlist.push(colorslist[i].saturation);
        vallist.push(colorslist[i].value);
    }

    var hue_offset = huelist.length * FarbeHueSize;
    var sat_offset = satlist.length * FarbeSatSize;
    var val_offset = vallist.length * FarbeValSize;
    
    var ds = new ArrayBuffer(dslen);
    var dsview = new DataView(ds, 0);

    var dsh = "";

    dsh += "/* This file is generated by javascript */" + "\n";
    dsh += "#ifndef PARAM_OFFSETS_H_" + "\n";
    dsh += "#define PARAM_OFFSETS_H_" + "\n";
    dsh += "namespace ParamOffset" + "\n";
    dsh += "{" + "\n";
    dsh += "    enum ParamOffset" + "\n";
    dsh += "    {" + "\n";

    var offset = 0;

    for(let i = 0; i < date.length; i++) {
        dsview.setUint8(offset + i,date[i].charCodeAt());
    }

    dsh += "        Date            = " + offset + "u," + "\n";
    offset += date_len;

    dsview.setUint16(offset, majorver, true);
    dsview.setUint16(offset + 2, minorver, true);

    dsh += "        Version         = " + offset + "u," + "\n";
    offset += 4;

    for(let i = 0; i < info.length; i++) {
        dsview.setUint8(offset + i, info[i].charCodeAt());
    }

    dsh += "        Name            = " + offset + "u," + "\n";
    offset += info_len;

    for(let i = 0; i < event_scenes_list.length; i++) {
        dsview.setUint8(offset + i, event_scenes_list[i]);
    }

    dsh += "        EventsSzene     = " + offset + "u," + "\n";
    offset += event_scenes_offset;

    for(let i = 0; i < grouplist.length; i++) {
        dsview.setUint8(offset + i, grouplist[i]);
    }

    dsh += "        SznGruppe       = " + offset + "u," + "\n";
    offset += group_offset;

    for(let i = 0; i < scenelenlist.length; i++) {
        dsview.setUint8(offset + i, scenelenlist[i]);
    }

    dsh += "        SznSchritte     = " + offset + "u," + "\n";
    offset += scenelen_offset;

    for(let i = 0; i < delaylist.length; i++) {
        dsview.setUint8(offset + i, delaylist[i]);
    }

    dsh += "        SznT            = " + offset + "u," + "\n";
    offset += ssdelay_offset;

    for(let i = 0; i < aniidlist.length; i++) {
        dsview.setUint8(offset + i, aniidlist[i]);
    }

    dsh += "        SznAnimation    = " + offset + "u," + "\n";
    offset += ssaniid_offset;

    for(let i = 0; i < ttlist.length; i++) {
        dsview.setUint16(offset + 2 * i, ttlist[i], true);
    }

    dsh += "        AniZielT        = " + offset + "u," + "\n";
    offset += tt_offset;

    for(let i = 0; i < tclist.length; i++) {
        dsview.setUint8(offset + i, tclist[i]);
    }

    dsh += "        AniZielFarbe    = " + offset + "u," + "\n";
    offset += tc_offset;

    for(let i = 0; i < typelist.length; i++) {
        dsview.setUint8(offset + i, typelist[i]);
    }

    dsh += "        AniTyp          = " + offset + "u," + "\n";
    offset += type_offset;

    for(let i = 0; i < atlist.length; i++) {
        dsview.setUint16(offset + 2 * i, atlist[i], true);
    }

    dsh += "        AniT            = " + offset + "u," + "\n";
    offset += at_offset;

    for(let i = 0; i < stposlist.length; i++) {
        dsview.setUint8(offset + i, stposlist[i]);
    }

    dsh += "        AniPosStart     = " + offset + "u," + "\n";
    offset += stpos_offset;

    for(let i = 0; i < enposlist.length; i++) {
        dsview.setUint8(offset + i, enposlist[i]);
    }

    dsh += "        AniPosEnde      = " + offset + "u," + "\n";
    offset += endpos_offset;

    for(let i = 0; i < rotdirlist.length; i++) {
        dsview.setUint8(offset + i, rotdirlist[i]);
    }

    dsh += "        AniRotDir       = " + offset + "u," + "\n";
    offset += rotdir_offset;

    for(let i = 0; i < lightposidlist.length; i++) {
        dsview.setUint8(offset + i, lightposidlist[i]);
    }

    dsh += "        LichtPos        = " + offset + "u," + "\n";
    offset += lightposid_offset;

    for(let i = 0; i < lighttypelist.length; i++) {
        dsview.setUint8(offset + i, lighttypelist[i]);
    }

    dsh += "        LichtAusTyp     = " + offset + "u," + "\n";
    offset += lighttype_offset;

    for(let i = 0; i < lightdriverlist.length; i++) {
        dsview.setUint8(offset + i, lightdriverlist[i]);
    }

    dsh += "        LichtTreiber    = " + offset + "u," + "\n";
    offset += lightdriver_offset;

    for(let i = 0; i < lightdelaylist.length; i++) {
        dsview.setUint8(offset + i, lightdelaylist[i]);
    }

    dsh += "        LichtVerz       = " + offset + "u," + "\n";
    offset += lightdelay_offset;

    for(let i = 0; i < lightaddrlist.length; i++) {
        dsview.setUint8(offset + i, lightaddrlist[i]);
    }

    dsh += "        LichtAdresse    = " + offset + "u," + "\n";
    offset += lightaddr_offset;

    for(let i = 0; i < lightgroupslist.length; i++) {
        dsview.setUint8(offset + i, lightgroupslist[i]);
    }

    dsh += "        LichtMax        = " + offset + "u," + "\n";
    offset += lightgroup_offset;

    for(let i = 0; i < lightcslist.length; i++) {
        dsview.setUint8(offset + i, lightcslist[i]);
    }

    dsh += "        LichtFarbTyp    = " + offset + "u," + "\n";
    offset += lightcs_offset;

    for(let i = 0; i < anglelist.length; i++) {
        dsview.setUint16(offset + 2 * i, anglelist[i], true);
    }

    dsh += "        PosWinkel       = " + offset + "u," + "\n";
    offset += angle_offset;

    for(let i = 0; i < radlist.length; i++) {
        dsview.setUint16(offset + 2 * i, radlist[i], true);
    }

    dsh += "        PosRadius       = " + offset + "u," + "\n";
    offset += rad_offset;

    for(let i = 0; i < huelist.length; i++) {
        dsview.setUint16(offset + 2 * i, huelist[i], true);
    }

    dsh += "        FarbeHue        = " + offset + "u," + "\n";
    offset += hue_offset;

    for(let i = 0; i < satlist.length; i++) {
        dsview.setUint8(offset + i, satlist[i]);
    }

    dsh += "        FarbeSat        = " + offset + "u," + "\n";
    offset += sat_offset;

    for(let i = 0; i < vallist.length; i++) {
        dsview.setUint8(offset + i, vallist[i]);
    }

    dsh += "        FarbeVal        = " + offset + "u," + "\n";
    offset += val_offset;

    dsh += "        DelayMax        = " + offset + "u," + "\n";
    dsview.setUint8(offset++, delay_max);

    dsh += "        AnzSzenen       = " + offset + "u," + "\n";
    dsview.setUint8(offset++, actual_scenes);

    dsh += "        AnzAnimationen  = " + offset + "u," + "\n";
    dsview.setUint8(offset++, actual_animations);

    dsh += "        AnzLeuchten     = " + offset + "u," + "\n";
    dsview.setUint8(offset++, actual_lights);

    dsh += "        AnzFarben       = " + offset + "u," + "\n";
    dsview.setUint8(offset++, actual_colors);

    dsh += "        AnzPositionen   = " + offset + "u," + "\n";
    dsview.setUint8(offset++, actual_positions);

    dsh += "        Dimm2Bin        = " + offset + "u" + "\n";
    dsview.setUint8(offset, dimm2bin);    

    dsh += "    };" + "\n";
    dsh += "};" + "\n";
    dsh += "#endif //PARAM_OFFSETS_H_" + "\n";
    
    prj_offsetsh = dsh;

    var crc32value = crc32(dsview, 0);
    dsview.setUint32(dslen - crclen, crc32value, true);
        
    return ds;
}
