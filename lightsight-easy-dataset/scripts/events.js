/**
 * @file events.js
 * @brief Javascript to handle the HTML page 'Events' 
 * @author Boris Guzhov
 */

var event_list, scene_list;
var event_table = [];
const select_scene_string = "Select scene";
var modal_reason; 

function fill_event_option() {
    let event_select = document.getElementById("Event");
    for (ev in event_list) {
        if (event_list[ev].event != "") {
            let e = ev + " - " + event_list[ev].event;
            event_select.options[event_select.options.length] = new Option(e, ev);
        }
    }
}

function fill_scene_options() {
    let scene_select = [];
    scene_select[0] = document.getElementById("Scene_1");
    scene_select[1] = document.getElementById("Scene_2");
    scene_select[2] = document.getElementById("Scene_3");
    scene_select[3] = document.getElementById("Scene_4");
    for (sceneId=0; sceneId < scene_list.length; sceneId++) {
        if (scene_list[sceneId].group != 0) {
            let optionName = sceneId + " - " + scene_list[sceneId].name;
            for (sel=0; sel<scene_select.length; sel++) {
                scene_select[sel].options[scene_select[sel].options.length] = new Option(optionName, sceneId);
            }
        }
    }
}

function check_selected_event(evId) {
    if (evId == "Select event") {
        bootstrap_alert.warning('No event selected');
        return false;
    }
    return true;
}

function check_selected_scenes(array) {
    s = 0;
    for (a in array) {
        if (array[a] == select_scene_string)
            s++;
    }
    if (s == 4) {
        bootstrap_alert.warning('No any scene selected');
        return false;
    }

    for (i = 0; i < 4; i++) {
        for (j = i + 1; j < 4; j++) {
            if ((array[i] == array[j]) && (array[i] != select_scene_string)) {
                bootstrap_alert.warning('The same scenes selected');
                return false;
            }
        }
    }

    return true;
}

// Warning/error message
bootstrap_alert = function () { }
bootstrap_alert.warning = function (message) {
    $('#alert_placeholder').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>')
}

// Info message
bootstrap_alert.info = function (message) {
    $('#alert_placeholder').html('<div class="alert alert-info"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>')
}

function prepareEventTable() {
    let line = 0;
    for (item=0; item < event_list.length; item++) {
        event_table[line] = { ev_id: 0, line_id: 0, ev_name: "", ev_scene0: "", ev_scene1: "", ev_scene2: "", ev_scene3: "" };

        // Unknown event
        if (event_list[item].event == "")
            continue;

        event_table[line].line_id = line; 
        event_table[line].ev_id = item;
        event_table[line].ev_name = event_list[item].event;
        event_table[line].ev_scene0 = ev_scene(item, 0);
        event_table[line].ev_scene1 = ev_scene(item, 1);
        event_table[line].ev_scene2 = ev_scene(item, 2);
        event_table[line].ev_scene3 = ev_scene(item, 3);

        line++;
    }

    function ev_scene(item, sceneNum) {
        if (event_list[item].scenes[sceneNum] == 255) {
            return "";
        }
        return event_list[item].scenes[sceneNum] + ' - ' + scene_list[event_list[item].scenes[sceneNum]].name;
    }
}


// The function fills the fields in 'Events' page from localStorage
$(document).ready(function () {
    if (getDatasetFromLocalStorage()) {
        event_list = currentDataset.event_scenes;
        scene_list = currentDataset.scenes;
        modal_reason = "";
        
        fill_event_option();
        fill_scene_options();
        prepareEventTable();
        $('#event_table').bootstrapTable({
            data: event_table
        });

        page_changed = false;

        // Click map. The function maps scenes to desired event in currentDataset json data.
        $("#map").click(function () {
            var evId = $("#Event").val();
            if (check_selected_event(evId) == false)
                return;

            var scene = [], sceneName = [];
            scene[0] = $("#Scene_1").val();
            scene[1] = $("#Scene_2").val();
            scene[2] = $("#Scene_3").val();
            scene[3] = $("#Scene_4").val();

            for (s in scene) {
                if (scene[s] == select_scene_string)
                    event_list[evId].scenes[s] = 255;
                else
                    event_list[evId].scenes[s] = parseInt(scene[s]);
            }

            prepareEventTable();
            $('#event_table').bootstrapTable('load', event_table);
            highlight_table_row("event_table", get_line_id($("#Event").val()), false);
            
            page_changed = false;
        });

        $("#unmap").click(function () {
            let evId = $("#Event").val();

            if (check_selected_event(evId) == false)
                return;
            
            $("#Scene_1").val(select_scene_string);
            $("#Scene_2").val(select_scene_string);
            $("#Scene_3").val(select_scene_string);
            $("#Scene_4").val(select_scene_string);

            for (s=0; s<event_list[evId].scenes.length; s++) {
                event_list[evId].scenes[s] = 255;
            }

            prepareEventTable();

            $('#event_table').bootstrapTable('load', event_table);
            page_changed = false;
        });
        
        
    }

    let row_ev_id, $el;
    
    $('#event_table').on('click-row.bs.table', function (e, row, $element) {
        update_reason = "click";
        row_ev_id = row.ev_id;
        $el = $element;
        if (page_changed == true) {
            modal = $("#event-change-modal");
            modal.modal();
        }
        else {
            update_view();
        }
    });

    // can be called from 'row click' event or from 'event'.change
    function update_view() {
        if (update_reason == "click") {
            $('.success').removeClass('success');
            $($el).addClass('success');
            $('.success').css("background-color", "white");
        
            $("#Event").val(row_ev_id);  
            update_scenes(row_ev_id);
        }
        else if (update_reason == "change") {
            id = $("#Event").val();
            update_scenes(id);
            
            highlight_table_row("event_table", get_line_id(id), true);
           
        }
    }

    function get_line_id(ev) {
        for(l=0; l<event_table.length; l++) {
            if(event_table[l].ev_id == ev) {
                return event_table[l].line_id;
            }
        }
        return undefined;
    }

    $('#event-change-row').off('click').on('click', function (ev) {
        update_view();
        page_changed = false;
    });
    
    $("#Event").change(function () {
        let id = $(this).val();
        if (check_selected_event(id) == false)
            return;

        update_reason = "change";
        if (page_changed == true) {
            modal = $("#event-change-modal");
            modal.modal();
        }
        else {
            update_view();
        }
    });

    function update_scenes(ev_id) {
        $("#Scene_1").val(ev_scene_id(0));
        $("#Scene_2").val(ev_scene_id(1));
        $("#Scene_3").val(ev_scene_id(2));
        $("#Scene_4").val(ev_scene_id(3));

        function ev_scene_id(sceneNum) {
            if (event_list[ev_id].scenes[sceneNum] == 255) {
                return select_scene_string;
            }
            return event_list[ev_id].scenes[sceneNum];
        }
    }

    $("#Scene_1").change(function () {
        page_changed = true;
    });
    $("#Scene_2").change(function () {
        page_changed = true;
    });
    $("#Scene_3").change(function () {
        page_changed = true;
    });
    $("#Scene_4").change(function () {
        page_changed = true;
    });
});

function highlight_table_row(table, row, scroll) {
    if ( row == undefined ) {
        return;
    }
    $('.success').removeClass('success');
    let rows = document.getElementById(table).getElementsByTagName("tr");
    rows[++row].className = "success";

    if ( scroll == true ) {
    // scroll to th row
        rows[row].scrollIntoView({
            behavior: 'smooth',
            block: 'center'
        });
    }
}


//# sourceURL=events.js
