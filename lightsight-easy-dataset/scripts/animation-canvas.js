let lightsInfo = [];

let lightDrivers = {
    sal: 0,
    tsg: 1,
    dli: 2,
    isbbr: 3
};

let geometryTypes = {
    "global": 0,
    "ring": 1,
    "segment": 2,
    "dynamicRing": 3,
    "dynamicSegment": 4,
    "point": 5
};

let polarSystem = {
    center: {
        x: 0,
        y: 0,
    },
    radius: 0,
    meters: 10
};

let areaTypes = {
    none: 0,
    innerCircle: 1,
    outerCircle: 2,
    startArm: 3,
    endArm: 4,
    point: 5
};

let views = {
    default: 0,
    ring: 1,
    sector: 2,
    point: 3
};

lightImage = {
    width: 0,
    height: 0
};

let areas = {
    hidden: false,
    dynamic: false,
    view: 0,
    dragging: false,
    active: null,
    point: {
        current: {
            x: 0,
            y: 0,
        },
        caught: {
            x: 0,
            y: 0
        }
    },
    ring: {
        swapCircles: true,
        innerCircle: null,
        outerCircle: null,
        endCircle: {
            radius: 0,
            fillColor: "",
            color: "rgba(71, 142, 223, 1)",
            center: {
                x: 0,
                y: 0
            },
        },
        startCircle: {
            radius: 0,
            fillColor: "",
            color: "rgba(231, 84, 104, 1)",
            center: {
                x: 0,
                y: 0
            },
        },
    },
    sector: {
        clockwise: false,
        radius: 0,
        dragArea: {
            deviationDegrees: 1 * Math.PI / 180,
            captured: 0
        },
        center: {
            x: 0,
            y: 0,
        },
        start: {
            x: 0,
            y: 0,
        },
        end: {
            x: 0,
            y: 0,
        },
        positive: true,
        colors: {
            start: "rgba(231, 84, 104, 1)",
            end: "rgba(71, 142, 223, 1)",
            arc: "rgba(71, 142, 223, 1)",
            fill: "rgba(71, 142, 223, 0.5)"
        }
    }
};

$(document).on("geometryChanged", function (event) {
    console.log("New geometry");
});

/**
 * Context initialization is done here
 */
$(document).ready(function () {
    setTimeout(init, 100);
    function init() {
        lightsInfo = lightsExpandWithPositions(currentDataset.lights, currentDataset.positions);
        let popUps = createPopUps(lightsInfo);
        canvasSetToDefault();
        changeView(views.ring, areas);
        attachEvents();
        $('[data-toggle="popover"]').popover();
    }
});

/**
 * Registers event handlers
 */
function attachEvents() {
    /**
     * Erase button click handler
     */
    $("#eraseButton").click(function () {
        switch (areas.view) {
            case views.ring:
                circularAreaSetToDefault(areas);
                break;
            case views.sector:
                sectorAreaSetToDefault(areas);
                break;
            case views.point:
                pointAreaSetToDefault(areas);
                canvasClear();
                axesDraw();
                break;
            default:
                break;
        }
        changeView(areas.view, areas);
        page_changed = true;
    });

    /**
     * Ring button click handler
     */
    $("#ringButton").change(function () {
        changeView(views.ring, areas);
        page_changed = true;
    });

    /**
     * Sector button click handler
     */
    $("#sectorButton").change(function () {
        changeView(views.sector, areas);
        page_changed = true;
    });

    /**
     * Point button click handler
     */
    $("#pointButton").change(function () {
        changeView(views.point, areas);
        page_changed = true;
    });

    /**
     * Handles mouse button hold
     */
    $("#animationCanvas").mousedown(function () {
        areas.dragging = true;
        page_changed = true;
    });

    /**
     * Handles mouse button release
     */
    $("#animationCanvas").mouseup(function () {
        areas.dragging = false;
        document.body.style.cursor = "default";
        if (areas.ring.swapCircles == true) {
            [areas.ring.outerCircle, areas.ring.innerCircle] = [areas.ring.innerCircle, areas.ring.outerCircle]
            areas.ring.swapCircles = false;
        }
        page_changed = true;
    });

    /**
     * Rotation switch handler
     */
    $("#rotationSwitch").change(function () {
        sectorChangeRotation(areas.sector);
        page_changed = true;
    });

    /**
     * Type switch handler
     */
    $("#typeSwitch").change(function () {
        areas.dynamic = $(this).prop('checked');
        page_changed = true;
    });

    /**
     * Inclusion switch handler
     */
    $("#inclusionSwitch").change(function () {
        if ($(this).prop('checked')) {
            areas.hidden = true;
            $("#sectorTweaks").addClass('d-none');
            $("#motionTweaks").addClass('d-none');
        } else {
            areas.hidden = false;
            $("#sectorTweaks").removeClass('d-none');
            $("#motionTweaks").removeClass('d-none');
        }
        areas.view = getSelectedView();
        changeView(areas.view, areas);
        page_changed = true;
    });

    /**
     * Handles click on a canvas
     */
    $("#animationCanvas").click(function () {
        if (areas.view == views.point && areas.hidden == false) {
            areas.point.caught.x = areas.point.current.x;
            areas.point.caught.y = areas.point.current.y;
            canvasClear();
            axesDraw();
            locateLights(lightsInfo, areas);
        }
        page_changed = true;
    });
}

/**
 * Unregisters event handlers
 */
function detachEvents() {
    $("#eraseButton").unbind('click');
    $("#ringButton").unbind('change');
    $("#sectorButton").unbind('change');
    $("#pointButton").unbind('change');
    $("#animationCanvas").unbind('mousedown');
    $("#animationCanvas").unbind('mouseup');
    $("#rotationSwitch").unbind('change');
    $("#typeSwitch").unbind('change');
    $("#inclusionSwitch").unbind('change');
    $("#animationCanvas").unbind('click');
}

/**
 * This handler is called right after a table row is clicked
 */
function readGeometry() {
    let animationType = currentAniRow.ani_type_id;
    let startCoordinates;
    let endCoordinates;
    let animationIndex = parseInt(currentAniRow.ani_id, 10);
    const canvas = document.getElementById('animationCanvas');
    let expandSector = (sector) => {
        let angle = 5 * Math.PI / 180;
        let startVector = {
            x: sector.start.x - sector.center.x,
            y: sector.start.y - sector.center.y
        }
        let endVector = {
            x: sector.end.x - sector.center.x,
            y: sector.end.y - sector.center.y
        }
        if (sector.clockwise == false) {
            sector.start.x = (startVector.x * Math.cos(angle) - startVector.y * Math.sin(angle)) + sector.center.x;
            sector.start.y = (startVector.x * Math.sin(angle) + startVector.y * Math.cos(angle)) + sector.center.y;
            sector.end.x = (endVector.x * Math.cos(-angle) - endVector.y * Math.sin(-angle)) + sector.center.x;
            sector.end.y = (endVector.x * Math.sin(-angle) + endVector.y * Math.cos(-angle)) + sector.center.y;
        } else {
            sector.start.x = (startVector.x * Math.cos(-angle) - startVector.y * Math.sin(-angle)) + sector.center.x;
            sector.start.y = (startVector.x * Math.sin(-angle) + startVector.y * Math.cos(angle)) + sector.center.y;
            sector.end.x = (endVector.x * Math.cos(angle) - endVector.y * Math.sin(angle)) + sector.center.x;
            sector.end.y = (endVector.x * Math.sin(angle) + endVector.y * Math.cos(angle)) + sector.center.y;
        }
    }
    detachEvents();
    canvasSetToDefault();

    if (currentAniRow.ani_idle) {
        $('#typeSwitch').prop('checked', false);
        $("#rotationSwitch").prop('checked', false);
        $("#inclusionSwitch").prop('checked', true);
        $("#ringButton").parent().button('toggle');
        areas.view = views.default;
        areas.hidden = true;
    } else {
        try {
            $("#inclusionSwitch").prop('checked', false);
            areas.hidden = false;
            switch (animationType) {
                case geometryTypes.global:
                    break;
                case geometryTypes.dynamicRing:
                    areas.dynamic = true;
                    $('#typeSwitch').prop('checked', areas.dynamic);
                    areas.view = views.ring;
                    $("#ringButton").parent().button('toggle');
                    startCoordinates = lightsInfo[currentAniRow.ani_start_position_id].pos;
                    endCoordinates = lightsInfo[currentAniRow.ani_end_position_id].pos;
                    if (startCoordinates.radius < endCoordinates.radius) {
                        areas.ring.startCircle.radius = startCoordinates.radius * polarSystem.radius / polarSystem.meters - lightImage.width * 0.1;
                        areas.ring.endCircle.radius = endCoordinates.radius * polarSystem.radius / polarSystem.meters + lightImage.width * 0.1;
                    } else {
                        areas.ring.startCircle.radius = startCoordinates.radius * polarSystem.radius / polarSystem.meters + lightImage.width * 0.1;
                        areas.ring.endCircle.radius = endCoordinates.radius * polarSystem.radius / polarSystem.meters - lightImage.width * 0.1;
                    }
                    break;
                case geometryTypes.ring:
                    areas.dynamic = false;
                    $('#typeSwitch').prop('checked', areas.dynamic);
                    areas.view = views.ring;
                    $("#ringButton").parent().button('toggle');
                    startCoordinates = lightsInfo[currentAniRow.ani_start_position_id].pos;
                    endCoordinates = lightsInfo[currentAniRow.ani_end_position_id].pos;
                    if (startCoordinates.radius < endCoordinates.radius) {
                        areas.ring.startCircle.radius = startCoordinates.radius * polarSystem.radius / polarSystem.meters - lightImage.width * 0.1;
                        areas.ring.endCircle.radius = endCoordinates.radius * polarSystem.radius / polarSystem.meters + lightImage.width * 0.1;
                    } else {
                        areas.ring.startCircle.radius = startCoordinates.radius * polarSystem.radius / polarSystem.meters + lightImage.width * 0.1;
                        areas.ring.endCircle.radius = endCoordinates.radius * polarSystem.radius / polarSystem.meters - lightImage.width * 0.1;
                    }
                    break;
                case geometryTypes.dynamicSegment:
                    areas.dynamic = true;
                    $('#typeSwitch').prop('checked', areas.dynamic);
                    areas.view = views.sector;
                    $("#sectorButton").parent().button('toggle');
                    areas.sector.clockwise = !!currentAniRow.ani_rotation_id;
                    $("#rotationSwitch").prop('checked', areas.sector.clockwise);
                    startCoordinates = polarToCartesian(lightsInfo[currentAniRow.ani_start_position_id].pos);
                    endCoordinates = polarToCartesian(lightsInfo[currentAniRow.ani_end_position_id].pos);
                    areas.sector.start = startCoordinates;
                    areas.sector.end = endCoordinates;
                    expandSector(areas.sector);
                    break;
                case geometryTypes.segment:
                    areas.dynamic = false;
                    $('#typeSwitch').prop('checked', areas.dynamic);
                    areas.view = views.sector;
                    $("#sectorButton").parent().button('toggle');
                    areas.sector.clockwise = !!currentAniRow.ani_rotation_id;
                    $("#rotationSwitch").prop('checked', areas.sector.clockwise);
                    startCoordinates = polarToCartesian(lightsInfo[currentAniRow.ani_start_position_id].pos);
                    endCoordinates = polarToCartesian(lightsInfo[currentAniRow.ani_end_position_id].pos);
                    areas.sector.start = startCoordinates;
                    areas.sector.end = endCoordinates;
                    expandSector(areas.sector);
                    break;
                case geometryTypes.point:
                    areas.view = views.point;
                    $("#pointButton").parent().button('toggle');
                    startCoordinates = lightsInfo[currentAniRow.ani_start_position_id].pos;
                    areas.point.caught = polarToCartesian(startCoordinates);
                    break;
                default:
                    $('#typeSwitch').prop('checked', false);
                    $("#rotationSwitch").prop('checked', false);
                    $("#ringButton").parent().button('toggle');
                    areas.view = views.ring;
                    console.log("Unknown animation type");
                    break;
            }
        } catch (err) {
            canvasSetToDefault();
            if (err instanceof TypeError)
                console.log("An invalid field has been found, draw the default view");
            else
                console.log(err);
        }
    }
    changeView(areas.view, areas);
    attachEvents();
}

/**
 * Fill geometry in the dataset and in the animation table if 'Save button' is clicked
 */
function fillGeometry() {
    let boundaries;
    let animationIndex = parseInt(currentAniRow.ani_id, 10);
    let oldRecord = { ...currentAniRow };

    /**
     * An empty area
     */
    if (areas.hidden) {
        currentAniRow.ani_start_position_id = currentDataset.animations.length - 1;
        currentAniRow.ani_end_position_id = currentDataset.animations.length - 1;
        currentAniRow.ani_rotation_id = 0;
        currentAniRow.ani_type_id = geometryTypes.point;
        currentAniRow.ani_idle = true;
        areas.dynamic = false;
        currentDataset.animations[currentAniRow]
        updateDatasetRecords(currentAniRow);
        updateTableContents(currentAniRow);
        return true;
    }

    /**
     * An occupied area
     */
    switch (areas.view) {
        case views.sector:
            currentAniRow.ani_rotation_id = +areas.sector.clockwise;
            if (areas.dynamic == true)
                currentAniRow.ani_type_id = geometryTypes.dynamicSegment;
            else
                currentAniRow.ani_type_id = geometryTypes.segment;
            boundaries = determineSectorBoundaries(lightsInfo, areas.sector);
            break;
        case views.ring:
            currentAniRow.ani_rotation_id = 0;
            if (areas.dynamic == true)
                currentAniRow.ani_type_id = geometryTypes.dynamicRing;
            else
                currentAniRow.ani_type_id = geometryTypes.ring;
            boundaries = determineRingBoundaries(lightsInfo, areas.ring);
            break;
        case views.point:
            currentAniRow.ani_rotation_id = 0;
            currentAniRow.ani_type_id = geometryTypes.point;
            boundaries = determinePointBoundaries(lightsInfo, areas.point.caught);
            break;
        default:
            break;
    }
    currentAniRow.ani_idle = false;
    if (boundaries.startIndex != null && boundaries.endIndex != null) {
        currentAniRow.ani_start_position_id = boundaries.startIndex;
        currentAniRow.ani_end_position_id = boundaries.endIndex;
        updateDatasetRecords(currentAniRow);
        updateTableContents(currentAniRow);
        return true;
    } else {
        Object.assign(currentAniRow, oldRecord);
        ani_bootstrap_alert.warning('None of lights are selected');
        return false;
    }
}

$(document).ready(function () {
    $("#animationCanvas").on('mouseleave', function (e) {
        $("#posPopOver").popover('hide');
    })
});

/** 
 * Handles mouse movements 
 */
$(document).ready(function () {
    $("#animationCanvas").mousemove(function (event) {
        if (areas.hidden == false) {
            const canvas = document.getElementById('animationCanvas');
            let rect = canvas.getBoundingClientRect();
            let cursorPos = {
                x: Math.round(event.pageX - $(this).offset().left),
                y: Math.round(event.pageY - $(this).offset().top)
            }
            updatePositionInfo(lightsInfo, cursorPos, { x: Math.round(event.pageX), y: Math.round(event.pageY) });
            // console.log("X: " + cursorPos.x, "Y: " + cursorPos.y);
            switch (areas.view) {
                case views.ring:
                    if (areas.dragging == false) {
                        if (isWithinCircle(cursorPos, areas.ring.innerCircle)) {
                            areas.active = areaTypes.innerCircle;
                            changeRingCursor(cursorPos, areas);
                        }
                        else if (isWithinCircle(cursorPos, areas.ring.outerCircle)) {
                            areas.active = areaTypes.outerCircle;
                            changeRingCursor(cursorPos, areas);
                        } else {
                            areas.active = areaTypes.none;
                            changeRingCursor(cursorPos, areas);
                        }
                    } else {
                        resizeArea(cursorPos, areas);
                    }
                    break;
                case views.sector:
                    if (areas.dragging == false) {
                        let dragArea = {
                            center: areas.sector.center,
                            radius: areas.sector.radius,
                            start: {
                                x: 0,
                                y: 0
                            },
                            end: {
                                x: 0,
                                y: 0
                            }
                        }
                        /* Locate the cursor relatively to the start vector */
                        startVector = {
                            x: areas.sector.start.x - areas.sector.center.x,
                            y: areas.sector.start.y - areas.sector.center.y
                        }
                        dragArea.start.x = (startVector.x * Math.cos(areas.sector.dragArea.deviationDegrees) - startVector.y * Math.sin(areas.sector.dragArea.deviationDegrees)) + areas.sector.center.x;
                        dragArea.start.y = (startVector.x * Math.sin(areas.sector.dragArea.deviationDegrees) + startVector.y * Math.cos(areas.sector.dragArea.deviationDegrees)) + areas.sector.center.y;
                        dragArea.end.x = areas.sector.start.x;
                        dragArea.end.y = areas.sector.start.y;
                        if (isWithinSector(dragArea, cursorPos)) {
                            document.body.style.cursor = "grab";
                            areas.active = areaTypes.startArm;
                        } else {
                            document.body.style.cursor = "default";
                            areas.active = areaTypes.none;
                        }
                        /* Locate the cursor relatively to the end vector */
                        endVector = {
                            x: areas.sector.end.x - areas.sector.center.x,
                            y: areas.sector.end.y - areas.sector.center.y
                        }
                        dragArea.end.x = (endVector.x * Math.cos(-areas.sector.dragArea.deviationDegrees) - endVector.y * Math.sin(-areas.sector.dragArea.deviationDegrees)) + areas.sector.center.x;
                        dragArea.end.y = (endVector.x * Math.sin(-areas.sector.dragArea.deviationDegrees) + endVector.y * Math.cos(-areas.sector.dragArea.deviationDegrees)) + areas.sector.center.y;
                        dragArea.start.x = areas.sector.end.x;
                        dragArea.start.y = areas.sector.end.y;
                        if (isWithinSector(dragArea, cursorPos)) {
                            document.body.style.cursor = "grab";
                            areas.active = areaTypes.endArm;
                        }
                    } else {
                        document.body.style.cursor = "grabbing";
                        resizeArea(cursorPos, areas);
                    }
                    break;
                case views.point:
                    areas.point.current.x = cursorPos.x;
                    areas.point.current.y = cursorPos.y;
                    break;
                default:
                    break;
            }
        }
    });
});

function updatePositionInfo(lightsArray, cursorPos, popOverPos) {
    for (let i = 0; i < lightsArray.length; i++) {
        let cartesianPos = polarToCartesian(lightsArray[i].pos);
        let rect = {
            x: cartesianPos.x - lightImage.width / 2,
            y: cartesianPos.y - lightImage.height / 2,
            width: lightImage.width,
            height: lightImage.height
        }
        if (isWithinRect(rect, cursorPos)) {
            $('#posPopOver').css('left', popOverPos.x + 10)
            $('#posPopOver').css('top', popOverPos.y - 85);
            $('#posPopOver').attr('data-content', '(' + lightsArray[i].pos.radius + '; '
                + lightsArray[i].pos.angle + '°)');
            $("#posPopOver").popover('show');
            break;
        }
        else {
            $("#posPopOver").popover('hide');
        }
    }
}

/**
 * Creates a set of pop-ups with lights info
 * @param {array} lightsArray - Available lights 
 */
function createPopUps(lightsArray) {
    let popUps = [];
    for (let i = 0; i < lightsArray.length; i++) {
        popUps.push("radius: " + lightsArray[i].pos.radius + "m" + ", angle: " + lightsArray[i].pos.angle + "°");
    }
    return popUps
}
/**
 * Creates a comprehensive light array from the list of available lights and list of available positions 
 * @param {object} lightsList - List of available lights
 * @param {object} posList - List of available positions
 */
function lightsExpandWithPositions(lightsList, posList) {
    let newList = [];
    console.log("Light number: " + lightsList.length);
    for (let i = 0; i < lightsList.length; i++) {
        if (lightsList[i].name != undefined && lightsList[i].name != "") {
            newList[i] = { ...lightsList[i] };
            newList[i].pos = {
                angle: Math.round(posList[lightsList[i].position_id].angle * currentDataset.units.angle_unit),
                radius: posList[lightsList[i].position_id].radius * currentDataset.units.radius_unit
            };
        }
    }
    return newList;
}

/**
 * Modifies a corresponding animation record in the dataset info
 * @param {object} newInfo - A new record 
 */
function updateDatasetRecords(newInfo) {
    let index = parseInt(newInfo.ani_id, 10);
    animation_list[index].type = newInfo.ani_type_id;
    animation_list[index].start_position_id = newInfo.ani_start_position_id;
    animation_list[index].end_position_id = newInfo.ani_end_position_id;
    animation_list[index].rotation_direction = newInfo.ani_rotation_id;
}

/**
 * Clears geometry info in a project file
 */
function clearDatasetRecords(newInfo) {
    let index = parseInt(newInfo.ani_id, 10);
    animation_list[index].type = 0;
    animation_list[index].start_position_id = 0;
    animation_list[index].end_position_id = 0;
    animation_list[index].rotation_direction = 0;
}

/**
 * Refreshes the page with new geometry info
 * @param {*} newInfo - New geometry info
 */
function updateTableContents(newInfo) {
    let index = parseInt(newInfo.ani_id, 10);
    newInfo.ani_type = currentDataset.animation_types[newInfo.ani_type_id];
    newInfo.ani_start_position_angle = Math.round(currentDataset.positions[newInfo.ani_start_position_id].angle * currentDataset.units.angle_unit);
    newInfo.ani_start_position_radius = currentDataset.positions[newInfo.ani_start_position_id].radius * currentDataset.units.radius_unit;
    newInfo.ani_end_position_angle = Math.round(currentDataset.positions[newInfo.ani_end_position_id].angle * currentDataset.units.angle_unit);
    newInfo.ani_end_position_radius = currentDataset.positions[newInfo.ani_end_position_id].radius * currentDataset.units.radius_unit;
    if ((newInfo.ani_type_id == geometryTypes.segment) || (newInfo.ani_type_id == geometryTypes.dynamicSegment)) {
        newInfo.ani_rotation = currentDataset.rotations[newInfo.ani_rotation_id];
    } else
        newInfo.ani_rotation = "";
    $ani_table.bootstrapTable('load', ani_table_content);
    highlight_table_row("animation-table", currentAniRow.ani_id);
}

/**
 * Clears a current table record
 * @param {object} newInfo - New geometry info
 */
function clearGeometryContents(newInfo) {
    let index = parseInt(newInfo.ani_id, 10);
    newInfo.ani_type = "";
    newInfo.ani_start_position_angle = "";
    newInfo.ani_start_position_radius = "";
    newInfo.ani_end_position_angle = "";
    newInfo.ani_end_position_radius = "";
    newInfo.ani_rotation = "";
    newInfo.ani_end_position_id = "";
    newInfo.ani_start_position_id = "";
    newInfo.ani_type_id = "";
    newInfo.ani_rotation_id = "";
    $ani_table.bootstrapTable('load', ani_table_content);
    highlight_table_row("animation-table", currentAniRow.ani_id);
}

/**
 * Sets point coordinates to their default values
 */
function pointAreaSetToDefault(areasInfo) {
    areasInfo.point.caught.x = 0;
    areasInfo.point.caught.y = 0;
}

/**
 * Sets circle sizes to their default values
 */
function circularAreaSetToDefault(areasInfo) {
    const image = document.getElementById('carImage');

    areasInfo.ring.startCircle.center.x = image.width / 2;
    areasInfo.ring.startCircle.center.y = image.height / 2;
    areasInfo.ring.endCircle.center.x = image.width / 2;
    areasInfo.ring.endCircle.center.y = image.height / 2;
    areasInfo.ring.endCircle.radius = 40;
    areasInfo.ring.startCircle.radius = 20;
    areasInfo.ring.innerCircle = areasInfo.ring.startCircle;
    areasInfo.ring.outerCircle = areasInfo.ring.endCircle;
}

/**
 * Set sector's size to it's default value
 */
function sectorAreaSetToDefault(areasInfo) {
    const image = document.getElementById('carImage');

    areasInfo.sector.clockwise = false;
    areasInfo.sector.center.x = image.width / 2;
    areasInfo.sector.center.y = image.height / 2;
    areasInfo.sector.start.x = image.width * 0.4;
    areasInfo.sector.start.y = image.height * 0.3;
    areasInfo.sector.end.x = image.width * 0.37;
    areasInfo.sector.end.y = image.height * 0.3;
    if (image.width > image.height)
        areasInfo.sector.radius = image.height / 2;
    else
        areasInfo.sector.radius = image.width / 2;
}

/**
 * Sets canvas settings to their default value
 */
function canvasSetToDefault() {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');
    const image = document.getElementById('carImage');
    // Fit canvas to an image size
    canvas.width = image.width;
    canvas.height = image.height;
    /* Set basis for a polar coordinate system */
    polarSystem.center.x = image.width / 2;
    polarSystem.center.y = image.height / 2;
    polarSystem.radius = image.width >= image.height ? image.height / 2 * 0.9 : image.width / 2 * 0.9;
    pointAreaSetToDefault(areas);
    circularAreaSetToDefault(areas);
    sectorAreaSetToDefault(areas);
    lightImage.width = image.width * 0.1;
    lightImage.height = image.height * 0.1;
}

/**
 * Looks for a lights which are the closest to ring area boundaries
 * @param {array} lightsArray - An array of light descriptions
 * @param {object} ring - Ring area description 
 */
function determineRingBoundaries(lightsArray, ring) {
    let startPos = ring.endCircle.radius * polarSystem.meters / polarSystem.radius;
    let endPos = ring.startCircle.radius * polarSystem.meters / polarSystem.radius;
    let startIndex = null;
    let endIndex = null;

    for (let i = 0; i < lightsArray.length; i++) {
        if (lightsArray[i].name = "")
            continue;
        if (ring.startCircle.radius < ring.endCircle.radius) {
            if (lightsArray[i].pos.radius <= startPos && lightsArray[i].pos.radius >= ring.startCircle.radius * polarSystem.meters / polarSystem.radius) {
                startPos = lightsArray[i].pos.radius
                startIndex = i;
            }
            if (lightsArray[i].pos.radius >= endPos && lightsArray[i].pos.radius <= ring.endCircle.radius * polarSystem.meters / polarSystem.radius) {
                endPos = lightsArray[i].pos.radius
                endIndex = i;
            }
        } else {
            if (lightsArray[i].pos.radius >= startPos && lightsArray[i].pos.radius <= ring.startCircle.radius * polarSystem.meters / polarSystem.radius) {
                startPos = lightsArray[i].pos.radius
                startIndex = i;
            }
            if (lightsArray[i].pos.radius <= endPos && lightsArray[i].pos.radius >= ring.endCircle.radius * polarSystem.meters / polarSystem.radius) {
                endPos = lightsArray[i].pos.radius
                endIndex = i;
            }
        }
    }
    let result = {
        startIndex: startIndex,
        endIndex: endIndex
    };
    return result;
}

/**
 * Looks for a light which matches a selected point
 * @param {array} lightsArray - An array of light descriptions
 * @param {object} ring - Point coordinates  
 */
function determinePointBoundaries(lightsArray, point) {
    let startIndex = null;
    let endIndex = null;
    for (let i = 0; i < lightsArray.length; i++) {
        let cartesianPos = polarToCartesian(lightsArray[i].pos);
        let rect = {
            x: cartesianPos.x - lightImage.width / 2,
            y: cartesianPos.y - lightImage.height / 2,
            width: lightImage.width,
            height: lightImage.height
        }
        if (isWithinRect(rect, point))
            startIndex = i;
    }
    let result = {
        startIndex: startIndex,
        endIndex: startIndex
    };
    return result;
}

/**
 * Looks for a lights closest to ring area boundaries
 * @param {array} lightsArray - An array of light descriptions
 * @param {object} sector - Ring area description 
 */
function determineSectorBoundaries(lightsArray, sector) {
    let endBound = cartesianToPolar(sector.start).angle;
    let startBound = cartesianToPolar(sector.end).angle;
    let startIndex = null;
    let endIndex = null;

    if (sector.clockwise == true && endBound < startBound) {
        endBound += 360;
    }
    if (sector.clockwise == false && endBound > startBound) {
        startBound += 360;
    }

    for (let i = 0; i < lightsArray.length; i++) {
        let startAngle = cartesianToPolar(sector.start).angle;
        let endAngle = cartesianToPolar(sector.end).angle;
        if (lightsArray.name == "")
            continue;
        let lightAngle = lightsArray[i].pos.angle;
        if (sector.clockwise == true && startAngle < endAngle) {
            if (lightAngle <= startAngle)
                lightAngle += 360;
            startAngle += 360;
        }
        if (sector.clockwise == false && startAngle > endAngle) {
            if (lightAngle <= endAngle)
                lightAngle += 360;
            endAngle += 360;
        }

        if (endAngle > startAngle) {
            if (lightAngle <= startBound && lightAngle >= startAngle) {
                startBound = lightAngle;
                startIndex = i;
            }
            if (lightAngle >= endBound && lightAngle <= endAngle) {
                endBound = lightAngle;
                endIndex = i;
            }
        } else {
            if (lightAngle >= startBound && lightAngle <= startAngle) {
                startBound = lightAngle;
                startIndex = i;
            }
            if (lightAngle <= endBound && lightAngle >= endAngle) {
                endBound = lightAngle;
                endIndex = i;
            }
        }
    }
    let result = {
        startIndex: startIndex,
        endIndex: endIndex
    }
    return result;
}

/**
 * Checks whether headlights take part in animation as other lights
 * @param {object} lightsArray - An array of light descriptions
 */
function areLightsMixed(lightsArray) {
    let headlightsEngaged = false;
    let otherLightsEngaged = false;

    for (let i = 0; i < lightsArray.length; i++) {
        if (lightsArray[i].driver != lightDrivers.dli && headlightsEngaged == true &&
            lightsArray[i].selected == true ||
            lightsArray[i].driver == lightDrivers.dli && otherLightsEngaged == true &&
            lightsArray[i].selected == true)
            return true;
        if (lightsArray[i].driver == lightDrivers.dli && lightsArray[i].selected == true)
            headlightsEngaged = true;
        if (lightsArray[i].driver != lightDrivers.dli && lightsArray[i].selected == true)
            otherLightsEngaged = true;

    }

    return false;
}

/**
 * Gathers indexes of all lights that take part in a current animation
 */
function getSelectedLights() {
    let indexes = [];
    for (let i = 0; i < lightsInfo.length; i++) {
        if (lightsInfo[i].selected == true)
            indexes.push(i);
    }
    return indexes;
}

/**
 * Highlights active lights
 * @param {array} lightsArray - Available lights 
 * @param {Object} areasInfo - Geometry description
 */
function locateLights(lightsArray, areasInfo) {
    areasInfo.ring.startLight = null;
    areasInfo.ring.endLight = null;

    for (let i = 0; i < lightsArray.length; i++) {
        switch (areasInfo.view) {
            case views.ring:
                if (isWithinRing(areasInfo.ring, polarToCartesian(lightsArray[i].pos))) {
                    lightDraw(lightsArray[i], false);
                    lightsArray[i].selected = true;
                }
                else {
                    lightDraw(lightsArray[i], true);
                    lightsArray[i].selected = false;
                }
                break;
            case views.sector:
                let startPoint = cartesianToPolar(areasInfo.sector.start);
                let endPoint = cartesianToPolar(areasInfo.sector.end);
                if (isBetweenAngles(lightsArray[i].pos, startPoint.angle, endPoint.angle, areasInfo.sector.clockwise)) {
                    lightDraw(lightsArray[i], false);
                    lightsArray[i].selected = true;
                }
                else {
                    lightDraw(lightsArray[i], true);
                    lightsArray[i].selected = false;
                }
                break;
            case views.point:
                let lightPosCartesian = polarToCartesian(lightsArray[i].pos);
                let rect = {
                    x: lightPosCartesian.x - lightImage.width / 2,
                    y: lightPosCartesian.y - lightImage.height / 2,
                    width: lightImage.width,
                    height: lightImage.height
                }
                if (isWithinRect(rect, areasInfo.point.caught)) {
                    lightDraw(lightsArray[i], false);
                    lightsArray[i].selected = true;
                }
                else {
                    lightDraw(lightsArray[i], true);
                    lightsArray[i].selected = false;
                }
                break;
            default:
                break;
        }
    }
}

/**
 * Converts polar coordinates to cartesian ones
 * @param {object} coordinates - Polar coordinates 
 */
function polarToCartesian(coordinates) {
    let radius = coordinates.radius * polarSystem.radius / polarSystem.meters;
    let cartesianCoordinates = {
        x: Math.round(radius * Math.cos(-coordinates.angle * Math.PI / 180 - Math.PI / 2)),
        y: Math.round(radius * Math.sin(-coordinates.angle * Math.PI / 180 - Math.PI / 2))
    }
    cartesianCoordinates.x += polarSystem.center.x;
    cartesianCoordinates.y += polarSystem.center.y;

    return cartesianCoordinates;
}

/**
 * Converts cartesian coordinates to polar ones
 * @param {object} coordinates - Cartesian coordinates 
 */
function cartesianToPolar(coordinates) {

    let polarCoordinates = {
        radius: getVectorLength(polarSystem.center, coordinates) * polarSystem.meters / polarSystem.radius
    }
    polarCoordinates.angle = -Math.atan2(coordinates.y - polarSystem.center.y, coordinates.x - polarSystem.center.x);
    /* Scale to the range 0-360*/
    polarCoordinates.angle = polarCoordinates.angle >= 0 ? polarCoordinates.angle : polarCoordinates.angle + 2 * Math.PI;
    /* Rotate coordinate system by 90 degrees */
    polarCoordinates.angle = (polarCoordinates.angle >= 0 && polarCoordinates.angle < Math.PI / 2) ? polarCoordinates.angle + 1.5 * Math.PI : polarCoordinates.angle - Math.PI / 2;
    polarCoordinates.angle = polarCoordinates.angle * 180 / Math.PI;
    return polarCoordinates;
}

/**
 * Draws selected light in a canvas
 * @param {object} light - Light description 
 * @param {disabled} disabled - Set to true to show light as disabled
 */
function lightDraw(light, disabled) {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');

    drawImage(light, disabled, lightImage.width, lightImage.height);
    if (light.doubled == true) {
        let mirrorCopy = lightGetMirrorCopy(light);
        drawImage(mirrorCopy, disabled, lightImage.width, lightImage.height);
        drawLink(light.pos, mirrorCopy.pos, lightImage.width / 2);
    }

}

/**
 * Shows a link between the ref light and it's double  
 * @param {object} refLight - Coordinates of a reference light
 * @param {object} double - Coordinates of it's double
 * @param {object} radius - Radius of a highlighted area
 */
function drawLink(refLight, double, radius) {
    let canvas = document.getElementById('animationCanvas');
    let ctxt = canvas.getContext('2d');
    let refLightCartesian = polarToCartesian(refLight);
    let doubleCartesian = polarToCartesian(double);
    ctxt.strokeStyle = "rgba(80,80,80, 1)";
    /* Draw an outline of a double */
    ctxt.beginPath();
    ctxt.setLineDash([5, 5]);
    ctxt.arc(doubleCartesian.x, refLightCartesian.y, radius, 0, 2 * Math.PI);
    ctxt.closePath();
    ctxt.stroke();

    /* Restore the defaults */
    ctxt.strokeStyle = "rgba(0, 0, 0, 1)";
    ctxt.setLineDash([]);
}

/**
 * Creates a light copy mirrored horizontally
 * @param {object} light - Light description
 */
function lightGetMirrorCopy(light) {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');
    let mirrorCopy = Object.assign({}, light);
    let center = {
        x: canvas.width / 2,
        y: canvas.height / 2
    };
    let cartesianCoordinates = polarToCartesian(light.pos);

    if (cartesianCoordinates.x < center.x) {
        cartesianCoordinates.x = center.x + (center.x - cartesianCoordinates.x);
    } else {
        cartesianCoordinates.x = center.x - (cartesianCoordinates.x - center);
    }
    mirrorCopy.pos = cartesianToPolar(cartesianCoordinates);

    return mirrorCopy;
}

/**
 * Draws a light in a canvas
 * @param {object} light - Light info
 * @param {boolean} grayscale - Color palette. Set to true to use monochrome one 
 */
function drawImage(light, grayscale, width, height) {
    let canvas = document.getElementById('animationCanvas');
    let image = document.getElementById(light.image);
    let ctxt = canvas.getContext('2d');
    let cartesianCoordinates = polarToCartesian(light.pos);

    ctxt.drawImage(image, cartesianCoordinates.x - width / 2, cartesianCoordinates.y - height / 2, width, height);
    if (grayscale == true) {
        imageData = ctxt.getImageData(cartesianCoordinates.x - width / 2, cartesianCoordinates.y - height / 2, width, height);
        imageToGrayscale(imageData);
        ctxt.putImageData(imageData, cartesianCoordinates.x - width / 2, cartesianCoordinates.y - height / 2);
    }
}

/**
 * Converts an image to a grayscale
 * @param {object} imageData - bitmap data 
 */
function imageToGrayscale(imageData) {
    for (let y = 0; y < imageData.height; y++) {
        for (let x = 0; x < imageData.width; x++) {
            let i = (y * 4) * imageData.width + x * 4;
            let avg = (imageData.data[i] + imageData.data[i + 1] + imageData.data[i + 2]) / 3;
            imageData.data[i] = avg;
            imageData.data[i + 1] = avg;
            imageData.data[i + 2] = avg;
        }
    }
}

/**
 * Shows usage tips for a sector view
 */
function legendDraw(colors) {
    const canvas = document.getElementById('animationCanvas');
    let auxCanvasSize = {
        width: canvas.width * 0.2,
        height: canvas.height * 0.2
    }
    let point = {
        x: canvas.width - auxCanvasSize.width,
        y: canvas.height - auxCanvasSize.height
    };
    let ctxt = canvas.getContext('2d');
    ctxt.fillStyle = "rgba(0, 0, 0, 1)";
    ctxt.beginPath();
    ctxt.moveTo(point.x, point.y);
    ctxt.lineTo(point.x + auxCanvasSize.width / 2, point.y);
    ctxt.closePath();
    ctxt.strokeStyle = colors.start;
    ctxt.stroke();
    ctxt.font = '15px serif';
    ctxt.fillText('Start', point.x + 1 + auxCanvasSize.width / 2, point.y);

    point.y = canvas.height - auxCanvasSize.height / 2;
    ctxt.beginPath();
    ctxt.moveTo(point.x, point.y);
    ctxt.lineTo(point.x + auxCanvasSize.width / 2, point.y);
    ctxt.closePath();
    ctxt.strokeStyle = colors.end;
    ctxt.stroke();
    ctxt.font = '15px serif';
    ctxt.fillText('End', point.x + 1 + auxCanvasSize.width / 2, point.y);
    ctxt.strokeStyle = "#000";
}

/**
 * Resizes a selected area
 * @param {object} cursorPos - Cursor's coordinates
 */
function resizeArea(cursorPos, areasInfo) {
    const canvas = document.getElementById('animationCanvas');
    let newRadius;
    switch (areasInfo.active) {
        case areaTypes.innerCircle:
            newRadius = Math.sqrt(Math.pow(cursorPos.x - areasInfo.ring.endCircle.center.x, 2) + Math.pow(cursorPos.y - areasInfo.ring.endCircle.center.x, 2));
            /* Selected areas can't exceed the canvas size */
            if (newRadius * 2 < canvas.width && newRadius * 2 < canvas.height) {
                if (newRadius > areasInfo.ring.outerCircle.radius) {
                    areasInfo.ring.swapCircles = true;
                }
                areasInfo.ring.innerCircle.radius = newRadius;
                canvasClear();
                axesDraw();
                circleDraw(areasInfo.ring.innerCircle);
                circleDraw(areasInfo.ring.outerCircle);
                locateLights(lightsInfo, areasInfo);
                legendDraw({ start: areasInfo.ring.startCircle.color, end: areasInfo.ring.endCircle.color });
            }
            break;
        case areaTypes.outerCircle:
            /* Selected areas can't exceed the canvas size */
            newRadius = Math.sqrt(Math.pow(cursorPos.x - areasInfo.ring.startCircle.center.x, 2) + Math.pow(cursorPos.y - areasInfo.ring.startCircle.center.y, 2));
            if (newRadius * 2 < canvas.width && newRadius * 2 < canvas.height) {
                if (newRadius < areasInfo.ring.innerCircle.radius) {
                    areasInfo.ring.swapCircles = true;
                }
                areasInfo.ring.outerCircle.radius = newRadius;
                canvasClear();
                axesDraw();
                circleDraw(areasInfo.ring.innerCircle);
                circleDraw(areasInfo.ring.outerCircle);
                locateLights(lightsInfo, areasInfo);
                legendDraw({ start: areasInfo.ring.startCircle.color, end: areasInfo.ring.endCircle.color });
            }
            break;
        case areaTypes.startArm:
            areasInfo.sector.start = cursorPos;
            canvasClear();
            axesDraw();
            sectorDraw(areasInfo.sector);
            locateLights(lightsInfo, areasInfo);
            legendDraw(areasInfo.sector.colors);
            break;
        case areaTypes.endArm:
            areasInfo.sector.end = cursorPos;
            canvasClear();
            axesDraw();
            sectorDraw(areasInfo.sector);
            locateLights(lightsInfo, areasInfo);
            legendDraw(areasInfo.sector.colors);
            break;
        default:
            break;
    }
}

/**
 * Draws a new circle area
 * @param {object} circle - Circle description
 */
function circleDraw(circle) {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');
    ctxt.beginPath();
    ctxt.arc(circle.center.x, circle.center.y, circle.radius, 0, 2 * Math.PI);
    ctxt.closePath();
    ctxt.strokeStyle = circle.color;
    ctxt.stroke();
    if (circle.fillColor != "") {
        ctxt.fillStyle = circle.fillColor;
        ctxt.fill();
    }
    ctxt.fillStyle = "#0000000";
    ctxt.strokeStyle = "rgb(0, 0, 0)";
}

/**
 * Clears selected circular area
 * @param {object} circle - Circular area to clear
 */
function circleClear(circle) {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');
    ctxt.globalCompositeOperation = 'destination-out';
    ctxt.beginPath();
    ctxt.arc(circle.center.x, circle.center.y, circle.radius, 0, Math.PI * 2, true);
    ctxt.closePath();
    ctxt.fill();
    ctxt.globalCompositeOperation = 'source-over';
}

/**
 * Draws coordinate system
 */
function axesDraw() {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');
    /* Y-axis */
    ctxt.beginPath();
    ctxt.moveTo(canvas.width / 2, canvas.height);
    ctxt.lineTo(canvas.width / 2, 0);
    ctxt.closePath();
    ctxt.stroke();
    /* X-axis */
    ctxt.beginPath();
    ctxt.moveTo(0, canvas.height / 2);
    ctxt.lineTo(canvas.width, canvas.height / 2);
    ctxt.closePath();
    ctxt.stroke();
}

/**
 * Computes vector length
 * @param {object} startPoint - Start point coordinates 
 * @param {object} endPoint - End point coordinates
 */
function getVectorLength(startPoint, endPoint) {
    return Math.sqrt(Math.pow(endPoint.x - startPoint.x, 2) + Math.pow(endPoint.y - startPoint.y, 2));
}

/**
 * 
 * @param {object} sector - Sector description 
 */
function sectorChangeRotation(sector) {
    sector.clockwise = !sector.clockwise;
    canvasClear();
    axesDraw();
    sectorDraw(sector);
    locateLights(lightsInfo, areas);
    legendDraw(sector.colors);
}

/**
 * Draws a sector between two specified radius vectors
 * @param {object} sector - Sector description 
 */
function sectorDraw(sector) {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');
    let computeVector = (startPoint, endPoint, len) => {
        let vectorLen = getVectorLength(startPoint, endPoint);
        let resultVector = {
            x: (endPoint.x - startPoint.x) / vectorLen * len + startPoint.x,
            y: (endPoint.y - startPoint.y) / vectorLen * len + startPoint.y
        };
        return resultVector;
    }

    /* Compute left and right arm-vectors */
    let startPoint = computeVector(sector.center, sector.start, sector.radius);
    let endPoint = computeVector(sector.center, sector.end, sector.radius);

    /* Compute arc angles */
    let yAxisVector = {
        x: sector.center.x,
        y: 0
    };
    let endVector = {
        x: endPoint.x - sector.center.x,
        y: endPoint.y - sector.center.y
    };

    let startVector = {
        x: startPoint.x - sector.center.x,
        y: startPoint.y - sector.center.y
    };

    let startAngle = Math.atan2(startVector.y, startVector.x);
    let endAngle = Math.atan2(endVector.y, endVector.x);
    let sectorAngle = Math.atan2(startVector.x * endVector.y - startVector.y * endVector.x, startVector.x * endVector.x + startVector.y * endVector.y);
    /* Fill path */
    fillPath = new Path2D();
    fillPath.moveTo(startPoint.x, startPoint.y);
    fillPath.lineTo(sector.center.x, sector.center.y);
    fillPath.lineTo(endPoint.x, endPoint.y);
    // fillPath.arc(sector.center.x, sector.center.y, sector.radius, startAngle, endAngle, true);
    // ctxt.fillStyle = sector.colors.fill;
    // ctxt.fill(fillPath);
    // ctxt.fillStyle = "#000";

    /* Draw Lines */
    lineDraw(sector.center, startPoint, sector.colors.start);
    lineDraw(sector.center, endPoint, sector.colors.end);
    /* Draw an arc */
    ctxt.beginPath();
    ctxt.arc(sector.center.x, sector.center.y, sector.radius, startAngle, endAngle, !sector.clockwise);
    ctxt.strokeStyle = sector.colors.arc;
    ctxt.stroke();
    ctxt.closePath();
    /* Restore the default stroke color */
    ctxt.strokeStyle = "#000";

}

/**
 * Draws a line
 * @param {object} start - Start point coordinates
 * @param {object} end - End point coordinates
 * @param {string} style - Defines a stroke color and thickness 
 */
function lineDraw(start, end, style) {
    const canvas = document.getElementById('animationCanvas');
    const ctxt = canvas.getContext('2d');

    ctxt.beginPath();
    ctxt.moveTo(start.x, start.y);
    ctxt.lineTo(end.x, end.y);
    ctxt.strokeStyle = style;
    ctxt.closePath();
    ctxt.stroke();
    ctxt.strokeStyle = "#000";
}

/**
 * Removes any data from the initial sketch
 */
function canvasClear() {
    const canvas = document.getElementById('animationCanvas');
    const image = document.getElementById('carImage');
    const ctxt = canvas.getContext('2d');
    ctxt.clearRect(0, 0, canvas.width, canvas.height);
    ctxt.drawImage(image, 0, 0);
}

/**
 * Draws all lights in a grayscale
 * @param {array} lightsArray - An array of available lights 
 */
function drawIncaciveLights(lightsArray) {
    for (let i = 0; i < lightsArray.length; i++) {
        lightDraw(lightsArray[i], true);
    }
}

/**
 * Changes cursor type depending on it's location
 * @param {object} cursorPos Cursor's coordinates
 */
function changeRingCursor(cursorPos, areasInfo) {
    switch (areasInfo.active) {
        case areaTypes.innerCircle:
            if (cursorPos.x > areasInfo.ring.innerCircle.center.x) {
                if (cursorPos.y > areasInfo.ring.innerCircle.center.y)
                    document.body.style.cursor = "nw-resize";
                else
                    document.body.style.cursor = "sw-resize";
            } else {
                if (cursorPos.y > areasInfo.ring.innerCircle.center.y)
                    document.body.style.cursor = "sw-resize";
                else
                    document.body.style.cursor = "nw-resize";

            }
            break;
        case areaTypes.outerCircle:
            if (cursorPos.x > areasInfo.ring.outerCircle.center.x) {
                if (cursorPos.y > areasInfo.ring.outerCircle.center.y)
                    document.body.style.cursor = "nw-resize";
                else
                    document.body.style.cursor = "sw-resize";
            } else {
                if (cursorPos.y > areasInfo.ring.outerCircle.center.y)
                    document.body.style.cursor = "sw-resize";
                else
                    document.body.style.cursor = "nw-resize";

            }
            break;
        default:
            document.body.style.cursor = "default";
            break;
    }
}

/**
 * Checks whether a point is located within a specified angle or not
 * @param {object} point - Point coordinates
 * @param {number} startAngle - Start angle 
 * @param {number} endAngle - End angle
 * @param {boolean} clockwise - Rotation direction
 */
function isBetweenAngles(point, startAngle, endAngle, clockwise) {
    let pointAngle = point.angle;
    /* Angles should be scaled to a full circle under certain circumstances */
    if (clockwise == true && startAngle < endAngle) {
        if (pointAngle <= startAngle) {
            pointAngle += 360;

        }
        startAngle += 360;
    }
    if (clockwise == false && startAngle > endAngle) {
        if (pointAngle <= endAngle) {
            pointAngle += 360;
        }
        endAngle += 360;
    }

    /* Locate light */
    if (endAngle >= startAngle) {
        if (pointAngle >= startAngle && pointAngle <= endAngle)
            return true;
    } else {
        if (pointAngle <= startAngle && pointAngle >= endAngle)
            return true;
    }

    return false;
}

/**
 * 
 * Checks whether a selected point lies within a circle or not
 * @param {object} point  - Point's x coordinates
 * @param {number} circle - Circle object
 */
function isWithinCircle(point, circle) {
    let rEquaution = Math.pow(circle.radius, 2);
    let lEquation = Math.pow(point.x - circle.center.x, 2) + Math.pow(point.y - circle.center.y, 2)
    // console.log(lEquation + "=" + rEquaution);
    if (lEquation <= rEquaution) {
        return true;
    }

    return false;
}

/**
 * Checks whether a selected point is located withing a specified ring
 * @param {object} ring - Ring description 
 * @param {object} point - Point coordinates
 */
function isWithinRing(ring, point) {
    if (ring.startCircle.radius <= ring.endCircle.radius) {
        if (!isWithinCircle(point, ring.startCircle) &&
            isWithinCircle(point, ring.endCircle)) {
            return true;
        }
    }
    else {
        if (isWithinCircle(point, ring.startCircle) &&
            !isWithinCircle(point, ring.endCircle)) {
            return true;
        }
    }
    return false;
}

/**
 * Checks whether a selected point is located withing a specified rectangle
 * @param {object} rect - Rectangle description
 * @param {object} point - Point coordinates
 */
function isWithinRect(rect, point) {
    if (point.x >= rect.x && point.x <= rect.x + rect.width &&
        point.y >= rect.y && point.y <= rect.y + rect.height)
        return true;
    return false;
}



/**
 * Checks whether a selected point is located within a specified sector
 * @param {object} sector - Sector descriptors (center coordinates, start point coordinates, end point coordinates, radius)
 * @param {object} point - Point coordinates
 */
function isWithinSector(sector, point) {
    let pointVector = {
        x: point.x - sector.center.x,
        y: point.y - sector.center.y
    };
    let startVector = {
        x: sector.start.x - sector.center.x,
        y: sector.start.y - sector.center.y
    };
    let endVector = {
        x: sector.end.x - sector.center.x,
        y: sector.end.y - sector.center.y
    };
    let areClockWise = (vector1, vector2) => (-vector1.x) * vector2.y + vector1.y * vector2.x < 0;

    // let len = getVectorLength(sector.center, point);
    // let CW = areClockWise(endVector, pointVector);
    // let NCW = !areClockWise(startVector, pointVector);
    // console.log("len: " + len + " CW: " + CW + " NCW: ", NCW);

    return !areClockWise(startVector, pointVector) &&
        areClockWise(endVector, pointVector) &&
        getVectorLength(sector.center, point) <= sector.radius;
}

/**
 * Draws draggable areas on the top of a car sketch
 * @param {number} view - View ID
 */
function changeView(view, areasInfo) {
    areasInfo.view = view;
    if (areasInfo.hidden != true) {
        switch (view) {
            case views.ring:
                canvasClear();
                axesDraw();
                circleDraw(areasInfo.ring.outerCircle,);
                circleDraw(areasInfo.ring.innerCircle);
                locateLights(lightsInfo, areasInfo);
                legendDraw({ start: areasInfo.ring.startCircle.color, end: areasInfo.ring.endCircle.color });
                $("#sectorTweaks").addClass('d-none');
                $("#motionTweaks").removeClass('d-none');
                break;
            case views.sector:
                canvasClear();
                axesDraw();
                sectorDraw(areasInfo.sector);
                locateLights(lightsInfo, areasInfo);
                legendDraw(areasInfo.sector.colors);
                $("#sectorTweaks").removeClass('d-none');
                $("#motionTweaks").removeClass('d-none');
                break;
            case views.point:
                canvasClear();
                axesDraw();
                locateLights(lightsInfo, areasInfo);
                $("#sectorTweaks").addClass('d-none');
                $("#motionTweaks").addClass('d-none');
                break;
            default:
                canvasClear();
                axesDraw();
                break;
        }
    } else {
        // canvasSetToDefault();
        canvasClear();
        axesDraw();
        drawIncaciveLights(lightsInfo);
        $("#sectorTweaks").addClass('d-none');
        $("#motionTweaks").addClass('d-none');
    }
}

function getSelectedView() {
    let allBtns = $('#viewButtonGroup').find('input');
    let activeView;
    allBtns.each(function (index, button) {
        if ($(button).prop('checked') == true)
            activeView = index + 1;
    })
    return activeView;
}
//# sourceURL=animation-canvas.js
