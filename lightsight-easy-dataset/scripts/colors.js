/**
 * @file colors.js
 * @brief Javascript to handle the HTML page 'Colors' 
 * @author Boris Guzhov
 */

var animation_list, color_list, units, eye_state, removed;
var $color_table = $('#color-table'), color_table_content = [], currentColorRow;

// Checks that animation is selected before editing
function isColorSelected() {
    if (currentColorRow == undefined || removed == true) {
        color_bootstrap_alert.warning('A color must be selected first');
        return false;
    }
    return true;
}

function isColorFilled() {
    s=0;
    for(c=0; c<color_list.length; c++)
        if (color_list[c].name != "")
            s++;

    return s>0? true : false;
}


function highlight_table_row(table, row) {
    let rows = document.getElementById(table).getElementsByTagName("tr");

    if( isColorFilled() ) {
        rows[++row].className = "success";
    }
}

function fill_eye_state_option() {
    let eye = document.getElementById("color-edit-eye-state");
    for (i=0; i < eye_state.length; i++) {
        eye.options[eye.options.length] = new Option(eye_state[i], i);
    }
}


function hsv2dli(c) {
    let h = color_list[c].hue;
    let s = color_list[c].saturation;

    color_table_content[c].color_target_brightness = color_list[c].value;
    color_table_content[c].color_target_time = (h & 0xff) * units.time_unit;
    color_table_content[c].color_target_position_x = (h >> 8) & 7;
    color_table_content[c].color_target_position_y = (s) & 7;
    let es = (s >> 3) & 1;
    color_table_content[c].color_target_eye = eye_state[es];
    color_table_content[c].color_target_eye_id = es;
}

function dli2hsv(c) {
    let b = color_table_content[c].color_target_brightness;
    let t = color_table_content[c].color_target_time / units.time_unit;
    let x = color_table_content[c].color_target_position_x;
    let y = color_table_content[c].color_target_position_y;
    let e = color_table_content[c].color_target_eye_id;

    color_list[c].hue = ( (x&7) << 8) | (t & 0xff);
    color_list[c].saturation = (e << 3) | (y & 7);
    color_list[c].value = parseInt(b);
}

// Update  dataset with colors and redraw table
function update_colors() {
    // Update the dataset from color_table_content
    color_list.length = 0; // clean  array in dataset 

    for( color=0; color < color_table_content.length; color++ ) {
        color_table_content[color].color_id = color; // fix id if rows reordered
        
        color_list[color] = {
            name: "",
            type: "",
            hue: 0,
            saturation: 0,
            value: 0
        };

        color_list[color].name = color_table_content[color].color_name;
        color_list[color].type =  color_table_content[color].color_light_type;

        if (color_list[color].type == "SAL") {
            color_list[color].value = parseInt(color_table_content[color].color_target_brightness);
            color_list[color].hue = 0; 
            color_list[color].saturation = 0; 
        }
        else if (color_list[color].type == "DLI") {
            dli2hsv(color);
        }
    }
    
    $color_table.bootstrapTable('load', color_table_content);
    fill_color_light_type();

    highlight_table_row("color-table", currentColorRow.color_id);
}

function clear_color_table_row(color) {
    color_table_content[color] = {
        color_id: color,
        color_light_type: "",
        color_name: "",
        color_target_brightness: "",
        color_target_time: "",
        color_target_position_x: "",
        color_target_position_y: "",
        color_target_eye: "",
        color_target_eye_id: ""
    };
}


function fill_color_table_content() {
    color_table_content.length = 0;

    for( color=0; color < color_list.length; color++ ) {
        clear_color_table_row(color);

        if (color_list[color].name != "") {
            color_table_content[color].color_id = color;
            color_table_content[color].color_name = color_list[color].name;
            color_table_content[color].color_light_type = color_list[color].type;

            if (color_list[color].type == "SAL") {
                color_table_content[color].color_target_brightness =
                    color_list[color].value;
            }
            else if (color_list[color].type == "DLI") {
                hsv2dli(color); 
            }
        }
    }
}

function fill_color_light_type() {
    nrows = $("#color-table tr").length;

    if ( (nrows-1) != color_table_content.length )
        return;

    color_table_content.forEach( function(item, index) {
        if (color_table_content[index].color_name == "")
            return;

        x = $("#color-table tr")[index+1].cells[1];

        if ( color_list[index].type == "DLI" ) 
            x.innerHTML = '<i class="fas fa-eye"></i>';
        else
            x.innerHTML = '<i class="fas fa-sun"></i>';
    });
}


// Warning/error message
color_bootstrap_alert = function () { }
color_bootstrap_alert.warning = function (message) {
    $("#color-alert-placeholder").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>');
}

function dli_options_disable(yes) {
    $("#color-edit-target-time").prop("disabled", yes);
    $("#color-edit-position-x").prop("disabled", yes);
    $("#color-edit-position-y").prop("disabled", yes);
    $("#color-edit-eye-state").prop("disabled", yes);
}


// The function fills the fields in 'Animations' page from localStorage
$(document).ready(function () {
    if (getDatasetFromLocalStorage()) {
        removed = false;
        animation_list = currentDataset.animations;
        color_list = currentDataset.colors;
        units = currentDataset.units;
        eye_state = currentDataset.eye_state;

        fill_eye_state_option();
        
        // Draw the color table from dataset
        fill_color_table_content();
        $color_table.bootstrapTable({  data: color_table_content  });
        fill_color_light_type();

    }

    // Select table row
    $color_table.on('click-row.bs.table', function (e, row, $element) {
        $('.success').removeClass('success');
        $($element).addClass('success');
        $('.success').css("background-color", "white");
        currentColorRow = row;
        removed = false;
    });

    $color_table.on('reorder-row.bs.table', function (e, table, $element) {

        currentColorRow = $element;

        // 1. Colors reordered => colors IDs changed => need to fix color IDs in animations in dataset.
        update_animations();
        
        // 2. Update colors in dataset. NB: The color ids in animations  must be fixed before!!!
        color_table_content.length = 0;
        color_table_content = table;
        update_colors();

        function update_animations() {
            let index = 0;
            let changed_list = [];

            // Find and keep changes into changed_list[]
            for (color=0; color < color_table_content.length; color++) {
                if (color != color_table_content[color].color_id) {  // Color Id changed
                    changed_list[index] = { old_id: color_table_content[color].color_id, new_id: color };
                    index++;
                }
            }
            fix_colors_ids();

            function fix_colors_ids() {
                for (ani=0; ani < animation_list.length; ani++) {
                    for (ch=0; ch < changed_list.length; ch++ ) {
                        if (changed_list[ch].old_id == animation_list[ani].target_color) {
                            animation_list[ani].target_color = changed_list[ch].new_id;
                            break;
                        }
                    }
                }
            }
        }

    });
    

    $("#color-edit").click(function () {
        if (isColorSelected() == false) {
            return;
        }

        var modal = $("#color-edit-modal"), mode;
        modal.find('.modal-title').text('Edit color ' + currentColorRow.color_id);

        $("#color-edit-name").val(currentColorRow.color_name);
        $("#color-edit-brightness").val(currentColorRow.color_target_brightness);
        $("#color-edit-target-time").val(currentColorRow.color_target_time);
        $("#color-edit-position-x").val(currentColorRow.color_target_position_x);
        $("#color-edit-position-y").val(currentColorRow.color_target_position_y);
        $("#color-edit-eye-state").val(currentColorRow.color_target_eye_id);

        mode = currentColorRow.color_light_type;
        if(mode == "") {
            mode = "SAL"
            currentColorRow.color_light_type = mode;
        }

        // Set radio buttons and disable/enable DLI items depends on mode
        if( mode != "DLI" ) {
            dli_options_disable(true);
            $('#radio-dli').prop('checked',false);
            $('#radio-sal').prop('checked',true);
        }
        else {
            dli_options_disable(false);
            $('#radio-dli').prop('checked',true);
            $('#radio-sal').prop('checked',false);
        }

        modal.modal();

        $('input[name="color_light-type"]').off('click').on( "click", function() {

            mode =  $('input[name="color_light-type"]:checked').val();

//            $("#color-edit-name").val(currentColorRow.color_name);
//            $("#color-edit-brightness").val(currentColorRow.color_target_brightness);

            if ( mode != "DLI") {
                $("#color-edit-target-time").val("");
                $("#color-edit-position-x").val("");
                $("#color-edit-position-y").val("");
                $("#color-edit-eye-state").val("");
            }
            else {

                $("#color-edit-target-time").val(currentColorRow.color_target_time);
                $("#color-edit-position-x").val(currentColorRow.color_target_position_x);
                $("#color-edit-position-y").val(currentColorRow.color_target_position_y);
                $("#color-edit-eye-state").val(currentColorRow.color_target_eye_id);
            }

            dli_options_disable(mode != "DLI");
        });
        
        $('#color-edit-form').submit(function (ev) {

            modal.modal('toggle');

            currentColorRow.color_name = $("#color-edit-name").val();
            currentColorRow.color_target_brightness= $("#color-edit-brightness").val();
            currentColorRow.color_light_type = mode;

            if ( mode == "DLI" ) {
                currentColorRow.color_target_time = $("#color-edit-target-time").val();
                currentColorRow.color_target_position_x = $("#color-edit-position-x").val();
                currentColorRow.color_target_position_y = $("#color-edit-position-y").val();
                currentColorRow.color_target_eye_id = $("#color-edit-eye-state").val();
                currentColorRow.color_target_eye = eye_state[currentColorRow.color_target_eye_id];
            }
            else {
                currentColorRow.color_target_time = "";
                currentColorRow.color_target_position_x = "";
                currentColorRow.color_target_position_y = "";
                currentColorRow.color_target_eye_id = "";
                currentColorRow.color_target_eye = "";
            }
            
            // Update the dataset and redraw the table
            update_colors();

            return false;
        });

    });

    $("#color-remove").off('click').on('click', function (ev) {
        if (isColorSelected() == false) {
            return;
        }

        if( currentColorRow.color_name == "" ) {
            color_bootstrap_alert.warning('Selected color doesn\'t exist');
            return;
        }

        if ( isSelectedColorUsed() ) {
            return;
        }

        
        var modal = $("#color-remove-modal");
        modal.find('.modal-title').text('Remove color ' + currentColorRow.color_id);
        modal.modal();

        $('#color-remove-row').off('click').on('click', function (ev) {
            clear_color_table_row(currentColorRow.color_id);
            update_colors();
            removed = true;
        });

        function isSelectedColorUsed() {
            cnt = 0;
            first_ani = 0;
            for( ani=0; ani<animation_list.length; ani++ ) {

                if (animation_list[ani].name == "")
                    continue;
                
                if( animation_list[ani].target_color == currentColorRow.color_id ) {
                    if ( cnt == 0 ) {
                        first_ani = ani;
                    }
                    cnt ++;
                }
            }

            if ( cnt > 0 ) {
                msg = 'Cannot remove the selected color: it is used in the animation '+ first_ani;

                s = "";
                if (cnt == 2)
                    s = ' and in one more';
                else if (cnt > 2)
                    s  = ' and in ' + cnt + ' more';
                
                color_bootstrap_alert.warning(msg + s + '!');
                return true;
            }

            return false;
        }
    });
});

//# sourceURL=colors.js
