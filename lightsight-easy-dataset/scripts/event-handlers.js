/**
 * @file event-handlers.js
 * @brief Javascrtipt to handle 'unload'/'beforeunload' events.
 * The scripts saves currentDataset to localStorage if the page is unloaded.
 */

var currentDataset;
var saved_ds_string;
var page_changed;

window.addEventListener('unload', function(event) {
    if( page_changed != undefined) {
        page_changed = false;
    }
    if (window.currentDataset != undefined)
        putDatasetToLocalStorage();
});

window.addEventListener('beforeunload', function(event) {
    if (page_changed == true) {
        message = "You have made some changes which you might want to save.";
        event.preventDefault();
        event.returnValue = message;
        return message;
    }

    return null;
});

function putDatasetToLocalStorage()
{
    if ( window.currentDataset.project_state.saved == true ) {
        ds_string = JSON.stringify(window.currentDataset);
        if( saved_ds_string  !=  ds_string )  {
            window.currentDataset.project_state.saved = false;
        }
    }

    localStorage.setItem("LaSDataset", JSON.stringify(window.currentDataset));
}

function getDatasetFromLocalStorage()
{
    let ds = localStorage.getItem("LaSDataset");

    if (ds) {
        window.currentDataset = JSON.parse(ds);
        saved_ds_string = JSON.stringify(window.currentDataset);
        apply_project_settings();
        return 1;
    }
    else {
        apply_default_project_settings();
    }

    saved_ds_string = undefined;
    window.currentDataset = undefined;

    return 0;
}

window.onerror = function(message, url, lineNumber) {
   alert("JS exception has been happened :( !\n" +
     "Message: " + message + "\n(" + url + ":" + lineNumber + ")");
};

//# sourceURL=event-handlers.js
