/**
 * @file animations.js
 * @brief Javascript to handle the HTML page 'Animations' 
 * @author Boris Guzhov
 */

var scene_list, animation_list, color_list, ani_type_list, rotations, units, ani_removed;
var $ani_table = $('#animation-table'), ani_table_content = [], currentAniRow;



/**
 * Fill the select option with target colors
 */
function fill_animation_target_color_option() {
    let target_color = document.getElementById("ani-edit-target-color");

    for (color=0; color < color_list.length; color++) {
        if (color_list[color].name != "") {
            target_color.options[target_color.options.length] = new Option(color +  ' - ' + color_list[color].name, color);
        }
    }
}

/**
 * Checks that animation is selected before editing
 */
function isAniSelected() {
    if (currentAniRow == undefined || ani_removed == true) {
        ani_bootstrap_alert.warning('A valid animation must be selected first');
        return false;
    }
    return true;
}

/**
 * Is an animation line filled?
 */
function isAniFilled() {
    s=0;
    for(ani=0; ani<animation_list.length; ani++)
        if (animation_list[ani].name != "")
            s++;

    return s>0? true : false;
}

/**
 * Highlight animation table row
 */
function highlight_table_row(table, row) {
    let rows = document.getElementById(table).getElementsByTagName("tr");
    if( isAniFilled() ) {
        rows[++row].className = "success";
    }
}


/**
 * Update  dataset with animations and redraw table
 */
function update_animations() {
    // Update the dataset from ani_table_content
    animation_list.length = 0; // clean anaimation array in datast 

    for (ani=0; ani < ani_table_content.length; ani++) {
        ani_table_content[ani].ani_id = ani; // fix id if rows reordered

        animation_list[ani] = {
            name: "",
            target_time: 0,
            target_color: 0,
            type: 0,
            animation_time: 0,
            start_position_id: 0,
            end_position_id: 0,
            rotation_direction: 0,
            idle: false
        };

        if (ani_table_content[ani].ani_name != "") {
            animation_list[ani].name = ani_table_content[ani].ani_name;
            animation_list[ani].target_time = ani_table_content[ani].ani_target_time / units.time_unit;
            animation_list[ani].target_color = parseInt(ani_table_content[ani].ani_target_color_id);
            animation_list[ani].type = parseInt(ani_table_content[ani].ani_type_id);
            animation_list[ani].animation_time = ani_table_content[ani].ani_duration / units.time_unit;
            animation_list[ani].start_position_id = parseInt(ani_table_content[ani].ani_start_position_id);
            animation_list[ani].end_position_id = parseInt(ani_table_content[ani].ani_end_position_id);
            animation_list[ani].rotation_direction = parseInt(ani_table_content[ani].ani_rotation_id);
            animation_list[ani].idle = ani_table_content[ani].ani_idle;
        }
    }

    $ani_table.bootstrapTable('load', ani_table_content);
    highlight_table_row("animation-table", currentAniRow.ani_id);
}

/**
 * Clear animation table row
 */
function clear_ani_table_row(ani) {
    ani_table_content[ani] = {
        ani_id: ani,
        ani_name: "",
        ani_duration: "",
        ani_target_time: "",
        ani_target_color: "",
        ani_target_color_id: "",
        ani_type: "",
        ani_type_id: "",
        ani_start_position_angle: "",
        ani_start_position_radius: "",
        ani_start_position_id: "",
        ani_end_position_angle: "",
        ani_end_position_radius: "",
        ani_end_position_id: "",
        ani_rotation: "",
        ani_rotation_id: "",
        ani_idle: false
    };
}

/**
 * Fill animation table with values from the dataset
 */
function fill_ani_table_content() {
    ani_table_content.length = 0;
    positions = currentDataset.positions;

    for (ani=0; ani<animation_list.length; ani++) {
        clear_ani_table_row(ani);

        if (animation_list[ani].name != "") {
            ani_table_content[ani].ani_id = ani;
            ani_table_content[ani].ani_name = animation_list[ani].name;
            ani_table_content[ani].ani_duration = animation_list[ani].animation_time * units.time_unit;
            ani_table_content[ani].ani_target_time = animation_list[ani].target_time * units.time_unit;
            ani_table_content[ani].ani_target_color = color_list[animation_list[ani].target_color].name;
            ani_table_content[ani].ani_target_color_id = animation_list[ani].target_color;
            ani_table_content[ani].ani_type = ani_type_list[animation_list[ani].type];
            ani_table_content[ani].ani_type_id = animation_list[ani].type;

            ani_table_content[ani].ani_start_position_angle = Math.round(positions[animation_list[ani].start_position_id].angle * units.angle_unit);
            ani_table_content[ani].ani_start_position_radius = positions[animation_list[ani].start_position_id].radius * units.radius_unit;
            ani_table_content[ani].ani_start_position_id = animation_list[ani].start_position_id;

            ani_table_content[ani].ani_end_position_angle = Math.round(positions[animation_list[ani].end_position_id].angle * units.angle_unit);
            ani_table_content[ani].ani_end_position_radius = positions[animation_list[ani].end_position_id].radius * units.radius_unit;
            ani_table_content[ani].ani_end_position_id = animation_list[ani].end_position_id;

            ani_table_content[ani].ani_rotation_id = animation_list[ani].rotation_direction;
            ani_table_content[ani].ani_idle = animation_list[ani].idle;

            if ((animation_list[ani].type == 2) || (animation_list[ani].type == 4)) { //  geometry "sector/sector dynamic"
                ani_table_content[ani].ani_rotation = rotations[animation_list[ani].rotation_direction];
            }
        }
    }
}


/**
 * Warning/success messages
 */
ani_bootstrap_alert = function () { }
ani_bootstrap_alert.warning = function (message) {
    $("#ani-alert-placeholder").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>');
}
ani_bootstrap_alert.success = function (message) {
    $("#ani-alert-placeholder").html('<div class="alert alert-success"><a class="close" data-dismiss="alert"><i class="fas fa-window-close"></i></a><span>' + message + '</span></div>');
}

/**
 *  Fills the fields in 'Animations' page from localStorage
 */
$(document).ready(function () {
    if (getDatasetFromLocalStorage()) {
        scene_list = currentDataset.scenes;
        animation_list = currentDataset.animations;
        color_list = currentDataset.colors;
        ani_type_list = currentDataset.animation_types;
        rotations = currentDataset.rotations;
        units = currentDataset.units;

        fill_animation_target_color_option();
        fill_ani_table_content();

        $ani_table.bootstrapTable({
            data: ani_table_content
        });

        
        highlight_table_row("animation-table", 0);
        // fill_input() must be called after init() that is in animation-canvas.js
        setTimeout(fill_input, 150, ani_table_content[0]);

        /**
         *  Handles reordering rows in the animation table
         */
        $ani_table.on('reorder-row.bs.table', function (e, table, $element) {
            currentAniRow = $element;

            // 1. Animations reordered => animation IDs changed => need to fix animation IDs in scenes' substeps in dataset.
            let changed_list = [];
            update_scene_steps();

            // 2. Update animations in dataset. NB: The animation ids in scene steps must be fixed before!!!
            ani_table_content.length = 0;
            ani_table_content = table;
            update_animations();

            /**
             *  Updates scene steps with change animation IDs
             */
            function update_scene_steps() {
                let index = 0;
                // Find and keep changes into changed_list[]
                for (ani=0; ani < ani_table_content.length; ani++) {
                    if (ani != ani_table_content[ani].ani_id) {  // Ani Id changed
                        changed_list[index] = { old_id: ani_table_content[ani].ani_id, new_id: ani };
                        index++;
                    }
                }

                fix_steps();
            }

            /**
             * Fixes steps
             */
            function fix_steps() {
                for (sc=0; sc < scene_list.length; sc++) {
                    let step_list = scene_list[sc].substeps;
                    // Check all substebs 
                    for (st=0; st<step_list.length; st++) {
                        // Check if step.animation_id changed
                        for (ch=0; ch < changed_list.length; ch++) {
                            if (changed_list[ch].old_id == step_list[st].animation_id) {
                                step_list[st].animation_id = changed_list[ch].new_id;
                                break;
                            }
                        }
                    }
                }
            }

        });

        /**
         * Handles 'mouse click' on a row
         */
        $ani_table.on('click-row.bs.table', function (e, row, $element) {
            if ( (currentAniRow !=undefined) &&
                 (currentAniRow.ani_id != row.ani_id) &&
                 (page_changed == true) ) {
                modal = $("#ani-change-modal");
                modal.modal();
            }
            else {
                _fill_input();
            }

            /**
             *  Change anumation confirmation button
             */
            $('#ani-change-row').off('click').on('click', function (ev) {
                _fill_input();
            });

            function _fill_input() {
                $('.success').removeClass('success');
                $($element).addClass('success');
                $('.success').css("background-color", "white");
                

                fill_input(row);
            }
        });

        function fill_input(row) {
            currentAniRow = row;
            
            $("#ani-edit-name").val(currentAniRow.ani_name);
            $("#ani-edit-duration").val(currentAniRow.ani_duration);
            $("#ani-edit-target-time").val(currentAniRow.ani_target_time);
            $("#ani-edit-target-color").val(currentAniRow.ani_target_color_id);
            
            readGeometry();
            page_changed = false;
            ani_removed = false;
        }
    }

    /**
     *  Sumbit changes on the animation page
     */
    $('#ani-par-form').submit(function (ev) {

        let color_id = $("#ani-edit-target-color").val();

        if (color_id == undefined) {
            ani_bootstrap_alert.warning('Target color must be filled!');
            return false;
        }
        
        // Ensure that the returned array contains either only DLI
        // lights IDs or only other lights IDs (never together).
        let ani_lights = getSelectedLights();
        if (areLightsMixed(ani_lights) == true) {
            ani_bootstrap_alert.warning("Headlights can't take part in animation with other lights");
            return false;
        }

        if ( ani_lights.length != 0 ) {  // checking below are valid only for existing lights
            // Check that the target color type is "DLI" if the selected lights are DLI.
            if( (currentDataset.lights[ani_lights[0]].driver == 2) && color_list[color_id].type != "DLI" ) {
                ani_bootstrap_alert.warning('The target color with type=SAL is not suitable for the selected lights');
                return false;
            }
            
            // Check that the target color type is "SAL" if the selected lights are not DLI.
            if( (currentDataset.lights[ani_lights[0]].driver != 2) && color_list[color_id].type != "SAL" ) {
                ani_bootstrap_alert.warning('The target color with type=DLI is not suitable for the selected lights');
                return false;
            }
        }

        if( fillGeometry() == false ) {        
            return false;
        }

        
        var item_index = currentAniRow.ani_id;

        currentAniRow.ani_name = $("#ani-edit-name").val();
        currentAniRow.ani_duration = $("#ani-edit-duration").val();
        currentAniRow.ani_target_time = $("#ani-edit-target-time").val();
        currentAniRow.ani_target_color = color_list[$("#ani-edit-target-color").val()].name;
        currentAniRow.ani_target_color_id = $("#ani-edit-target-color").val();
        //      currentAniRow.ani_type = ani_type_list[$("#ani-edit-type").val()];

        update_animations();
        page_changed = false;

        ani_bootstrap_alert.success('The animation ' + currentAniRow.ani_id + ' has been saved.');
        return false;
    });

    $('#ani-par-form').change(function () {
        page_changed = true;
    });

    /**
     *  Remove anumation button
     */
    $("#ani-remove").click(function () {
        if (isAniSelected() == false) {
            return;
        }

        if (currentAniRow.ani_name == "") {
            ani_bootstrap_alert.warning('Animation doesn\'t exist');
            return;
        }

        if (check_steps() == false) {
            return;
        }

        modal = $("#ani-remove-modal");
        modal.find('.modal-title').text('Remove animation ' + currentAniRow.ani_id);
        modal.modal();

        
        /**
         *  Remove anumation confirmation button
         */
        $('#ani-remove-row').off('click').on('click', function (ev) {
            clear_ani_table_row(currentAniRow.ani_id);
            update_animations();
            ani_removed = true;
        });

        /**
         * Returns number of substeps which are using the animation to be removed
         */
        function check_steps() {
            let st_cnt = 0;
            let sc_first = 0, st_first = 0;
            
            for (sc=0; sc < scene_list.length; sc++) {
                let step_list = scene_list[sc].substeps;
                // Check all substebs 
                for (st=0; st < step_list.length; st++) {
                    if (currentAniRow.ani_id == step_list[st].animation_id) {
                        if (st_cnt == 0) {
                            st_first = st;
                            sc_first = sc;
                        }
                        st_cnt++;
                    }
                }
            }

            if (st_cnt > 0) {
                msg = 'Cannot remove the selected animation: it is used  in [scene ' +
                    sc_first + ', step ' + st_first + ']';

                s = "";
                if ( st_cnt == 2 ) 
                    s = ' and in one more step in a scene';
                else if ( st_cnt > 2 ) 
                    s = ' and in ' + st_cnt + ' steps more';
                ani_bootstrap_alert.warning(msg + s + '!');
                return false;
            }
            
            return true;
        }
    });
});

//# sourceURL=animations.js
