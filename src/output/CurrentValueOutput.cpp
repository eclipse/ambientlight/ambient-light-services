/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file CurrentValueOutput.cpp
 *  Implementation of the Class DataPixel
 *  Created on:      28-May-2020 12:48:05
 */

#include "CurrentValueOutput.h"

CurrentValueOutput::CurrentValueOutput(): initialized(false)
{
}

CurrentValueOutput::~CurrentValueOutput()
{
}

void CurrentValueOutput::input( Hsv &givenHSV, uint8 light, uint16 giventime )
{
    if( ( light < maxLights ) && 
        ( Driver::Driver::SAL == static_cast<Driver::Driver>( runtime->getActuatorDrivers()[light] ) ) )
    {
        targetTime[light] = giventime;
        targetHSV[light] = givenHSV;
    }
}

void CurrentValueOutput::cyclic()
{
    if ( false == initialized )
    {
        for(uint8 lightidx = 0; lightidx < maxLights; lightidx++)
        {
            currentHSV[lightidx].init();
            finalHSV[lightidx].init();

            startHSV[lightidx].init();
            targetHSV[lightidx].init();

            targetTime[lightidx] = 0;
            currentTime[lightidx] = 0; 

            finalTime[lightidx] = 0;

            isActive[lightidx] = false;
        }

        initialized = true;
    }
    else
    {
        for(uint8 lightidx = 0; lightidx < maxLights; lightidx++)
        {
            if( ( finalTime[lightidx] != ( targetTime[lightidx] * trgtclkmult ) ) || 
                ( finalHSV[lightidx] != targetHSV[lightidx] ) )
            {
                // Input parameters changed
                finalHSV[lightidx] = targetHSV[lightidx];
                finalTime[lightidx] = targetTime[lightidx] * trgtclkmult;

                startHSV[lightidx] = currentHSV[lightidx];
                currentTime[lightidx] = 0;
                isActive[lightidx] = true;
            }
            else
            {
                currentTime[lightidx] += cyclictime;
            }
            
            if(true == isActive[lightidx])
            {
                Hsv numstartHSV = startHSV[lightidx];
                Hsv numfinalHSV = finalHSV[lightidx];

                int mixratio;
                if (0 == finalTime[lightidx])
                {
                    mixratio = 100;
                }
                else
                {
                    mixratio = 1000 - 1000 * (finalTime[lightidx] - currentTime[lightidx]) / finalTime[lightidx];
                    mixratio /= 10;
                }

                /** @todo: check Mix & Envelope with Runtime */
                HsvMixing::calcHsvMixing( numstartHSV, numfinalHSV, EnvelopeChange::EnvelopeChange::linear, 
                                        Mix::ConstSat, mixratio, currentHSV[lightidx] );

                Rgbw rgb;
                Hsv2Rgbw h2r;

                ColorType::ColorType colorSpace = static_cast<ColorType::ColorType>(runtime->getActuatorColorTypes()[lightidx]);
                h2r.convHsv2Rgbw( currentHSV[lightidx], colorSpace , rgb );
                output( lightidx, rgb );
            
                // assume targetTime tick = 10ms, cyclic tick = 20ms
                // compare time in milliseconds
                if(currentTime[lightidx] >= finalTime[lightidx] )
                {
                    isActive[lightidx] = false;
                }
            }
        }            
    }
}

void CurrentValueOutput::output( uint8 light, Rgbw &rgb )
{
    switch(runtime->getActuatorAddresses()[light])
    {
        case 0u:
        {
            runtime->setEmblemBrightness( static_cast<uint8>( rgb.numWhite ) );
            break;
        }
        case 1u:
        {
            runtime->setSlatBrightness( static_cast<uint8>( rgb.numWhite ) );
            break;
        }
        case 2u:
        {
            runtime->setTailLightBrightness( static_cast<uint8>( rgb.numWhite ) );
            break;
        }
        case 3u:
        {
            runtime->setSignature1State( static_cast<uint8>( rgb.numWhite ) >= runtime->getLightTurnOnThreshold() );
            break;
        }
        case 4u:
        {
            runtime->setSignature2State( static_cast<uint8>( rgb.numWhite ) >= runtime->getLightTurnOnThreshold() );
            break;
        }
        case 5u:
        {
            runtime->setHeadlightsState( static_cast<uint8>( rgb.numWhite ) >= runtime->getLightTurnOnThreshold() );
            break;
        }
        case 6u:
        {
            runtime->setTailLightState( static_cast<uint8>( rgb.numWhite ) >= runtime->getLightTurnOnThreshold() );
            break;
        }
        case 7u:
        {
            runtime->setLicensePlateLightState( static_cast<uint8>( rgb.numWhite ) >= runtime->getLightTurnOnThreshold() );   
            break;
        }
        case 8u:
        {                
            runtime->setMarkerLightState( static_cast<uint8>( rgb.numWhite ) >= runtime->getLightTurnOnThreshold() );
            break;
        }
        default:
            break;
    }
}

