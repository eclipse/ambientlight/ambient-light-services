/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file Hsv.cpp
 *  @brief Implementation of the structure Hsv
 *  @date  2020-03-06
 */

#include "Hsv.h"

bool operator==(const Hsv& left, const Hsv& right)
{
    return ( ( left.numHue == right.numHue ) && ( left.numSat == right.numSat ) &&
             ( left.numVal == right.numVal ) );
}

bool operator!=(const Hsv& left, const Hsv& right)
{
    return !( left == right );
}

void Hsv::init()
{
    numHue = 0;
    numSat = 0;
    numVal = 0;
}
