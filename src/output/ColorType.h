/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file ColorType.h
 *  @brief Implementation of the enumeration ColorType.h
 *  @date  2020-28-02
 */


#if !defined(OUTPUT_COLORTYPE_INCLUDED_)
#define OUTPUT_COLORTYPE_INCLUDED_


/**
 * @brief The enumeration presents possible color types
 * 
 */

namespace ColorType
{
   enum ColorType
    {
        /**
         * @brief 'rgbw' color type
         */
        rgbw,
        
        /**
         * @brief 'classic' color type
         */
        classic
    };
}

#endif // !defined(OUTPUT_COLORTYPE_INCLUDED_)
