/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file DoorCtrlDimmingData.h
 * @brief DoorCtrl dimming data
 * @date 2020-03-12
 */

#if !defined(OUTPUT_DOORCTRL_DIMMINGDATA_INCLUDED_)
#define OUTPUT_DOORCTRL_DIMMINGDATA_INCLUDED_

#include "PlatformTypes.h"

struct DoorCtrlDimmingData
{
    DoorCtrlDimmingData():
        dimmingTime(0),
        value(0)
    {
    }
    
    ~DoorCtrlDimmingData(){}
    
    uint16 dimmingTime;
    uint8  value;

    static const uint8 doorCtrlPinNumber = 4;
    static const uint8 doorCtrlNumber = 4;
};

#endif //!defined(OUTPUT_DOORCTRL_DIMMINGDATA_INCLUDED_)
