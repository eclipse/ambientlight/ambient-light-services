/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file Hsv.h
 *  @brief Implementation of the structure Hsv
 *  @date  2020-02-28
 */

#if !defined(OUTPUT_HSV_INCLUDED_)
#define OUTPUT_HSV_INCLUDED_

#include "PlatformTypes.h"

/**
 * @brief The structure Hsv contains hue, saturation and value values
 * 
 */

struct Hsv
{
    /**
     * @brief Hue value
     * 
     */
    uint16 numHue;
    
    /**
     * @brief Saturation value
     * 
     */
    uint8 numSat;

    /**
     * @brief value of  Brightness
     * 
     */
    uint8 numVal;

    void init();
    friend bool operator==(const Hsv& left, const Hsv& right);
    friend bool operator!=(const Hsv& left, const Hsv& right);
};
#endif // !defined(OUTPUT_HSV_INCLUDED_)
