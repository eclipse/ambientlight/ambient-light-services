/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file DataPixel.cpp
 *  @brief Implementation of the Class DataPixel
 *  @date 2020-05-28
 */

#include "DataPixel.h"

DataPixel::DataPixel()
{
}

DataPixel::~DataPixel()
{
}

void DataPixel::input(uint8 light, uint8 group, Animation &ani)
{
    // Check input light is assigned to a specific group 
    uint8 lightGroup = runtime->getActuatorGroupInclusion()[group + 8u*light];

    if( generalGroup != lightGroup)
    {
        Hsv hsv;
        ani.getHsv(hsv);
        output(
            static_cast<Driver::Driver>( runtime->getActuatorDrivers()[light] ),
            runtime->getActuatorAddresses()[light],
            hsv );
    }
}

void DataPixel::output( Driver::Driver actuatorDriver, uint8 actuatorAddr, Hsv &hsv )
{
    switch( actuatorDriver )
    {
    case Driver::Driver::iSBBR:
        if( iSbbrAddr == actuatorAddr )
        {
            runtime->setAnimationProfile( hsv.numVal );
        }
        break;
            
    case Driver::Driver::AFS:
        // left AFS
        if( afsLeftAddr == actuatorAddr )
        {
            runtime->setAfsPosHorizontalL( hsv.numHue );
            runtime->setAfsPosVerticalL( (hsv.numSat << 8u) | hsv.numVal );
        }

        // right AFS
        if( afsRightAddr == actuatorAddr )
        {
            runtime->setAfsPosHorizontalR( hsv.numHue );
            runtime->setAfsPosVerticalR( (hsv.numSat << 8u) | hsv.numVal );
        }
            
        break;

    case Driver::Driver::SAL:
    case Driver::Driver::DCR:
    default:
        // SAL and DCR are not controlled by DataPixel
        break;
    }
}

