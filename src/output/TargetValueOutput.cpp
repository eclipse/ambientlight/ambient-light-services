/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file TargetValueOutput.cpp
 * @brief Implementation of class TargetValueOuptut
 * @date 2020-05-29
 */

#include "TargetValueOutput.h"
#include "ColorType.h"
#include "Driver.h"
#include "IRuntime.h"
#include "Output.h"
#include "Rgbw.h"

TargetValueOutput::TargetValueOutput(Output& outputRef) : output(outputRef) {}

TargetValueOutput::~TargetValueOutput() {}

void TargetValueOutput::input(Hsv& targetColor, uint8 lightNumber, uint16 dimmingTime)
{
    Rgbw rgbColor;
    uint8 lightAddress = runtime->getActuatorAddresses()[lightNumber];
    Driver::Driver lightDriver = static_cast<Driver::Driver>(runtime->getActuatorDrivers()[lightNumber]);
    ColorType::ColorType colorSpace = static_cast<ColorType::ColorType>(runtime->getActuatorColorTypes()[lightNumber]);
    uint8 mapping = 0;

    switch (lightDriver)
    {
    case Driver::DCR:
        colorConverter.convHsv2Rgbw(targetColor, colorSpace, rgbColor);
        if (DCRGroup::FrontLeft > lightAddress)
        {
            output.doorCtrlDimmingDataFL[lightAddress].dimmingTime = dimmingTime;
            output.doorCtrlDimmingDataFL[lightAddress].value = rgbColor.numWhite;
            mapping++;
        }
        else if ((DCRGroup::FrontLeft <= lightAddress) && (DCRGroup::FrontRight > lightAddress))
        {
            output.doorCtrlDimmingDataFR[lightAddress - DCRGroup::FrontLeft].dimmingTime = dimmingTime;
            output.doorCtrlDimmingDataFR[lightAddress - DCRGroup::FrontLeft].value = rgbColor.numWhite;
            mapping++;
        }
        else if ((DCRGroup::FrontRight <= lightAddress) && (DCRGroup::RearLeft > lightAddress))
        {
            output.doorCtrlDimmingDataBL[lightAddress - DCRGroup::FrontRight].dimmingTime = dimmingTime;
            output.doorCtrlDimmingDataBL[lightAddress - DCRGroup::FrontRight].value = rgbColor.numWhite;
            mapping++;
        }
        else if ((DCRGroup::RearLeft <= lightAddress) && ((DCRGroup::RearLeft + DoorCtrlDimmingData::doorCtrlPinNumber) > lightAddress))
        {
            output.doorCtrlDimmingDataBR[lightAddress - DCRGroup::RearLeft].dimmingTime = dimmingTime;
            output.doorCtrlDimmingDataBR[lightAddress - DCRGroup::RearLeft].value = rgbColor.numWhite;
            mapping++;
        }
        else
        {
            // Do nothing
        }
    default:
        // Only DCR matters
        break;
    }

    if( mapping )
    {
        output.signalMappingDoorCtrl.output();
    }
}
