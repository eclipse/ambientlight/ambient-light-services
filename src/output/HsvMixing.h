/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file HsvMixing.h
 * @brief HSV Mixing declarations
 * @date 2019-02-28
 */

#if !defined(OUTPUT_HSVMIXING_INCLUDED_)
#define OUTPUT_HSVMIXING_INCLUDED_

#include "Hsv.h"
#include "Change.h"
#include "Mix.h"

/**
 * @brief This class allows to blend two different colors
 * in HSV model
 * 
 */
class HsvMixing
{

public:
    /**
     * @brief Constructs a new HsvMixing object
     * 
     */
    HsvMixing();
    /**
     * @brief Destructs an existing HsvMixing object
     * 
     */
    ~HsvMixing();
    /**
     * @brief 
     * @param[in] start First color to blend 
     * @param[in] target Second color to blend
     * @param[in] change Color transition envelope
     * @param[in] mix Mixing Setup
     * @param[in] targetPart Mixing ratio
     * @param[out] result Result color
     */
    static void calcHsvMixing(Hsv& start, Hsv& target, EnvelopeChange::EnvelopeChange change, Mix mix, int targetPart, Hsv& result);

private:
};
#endif // !defined(OUTPUT_HSVMIXING_INCLUDED_)
