/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file Hsv2Rgbw.h
 *  @brief Implementation of the class Hsv2Rgbw
 *  @date  2020-02-28
 */

#if !defined(OUTPUT_HSV2GBW_INCLUDED_)
#define OUTPUT_HSV2GBW_INCLUDED_

#include "Rgbw.h"
#include "Hsv.h"
#include "ColorType.h"

class Hsv2Rgbw
{

public:
    /**
     * @brief Constructs a new Hsv2Rgbw object
     */
    Hsv2Rgbw();

    /**
     * @brief Destroys the Hsv2Rgbw object
     */
    ~Hsv2Rgbw();

    /**
     * @brief Converts HSV to RGBW
     * @attention We currently use only the HSV-Value-Part for the RGBW-White-Value (brightness)
     * @attention For colored animation its necessary to edit this function.
     * @param hsv reference to HSV (input parameter)
     * @param colorType color type - rgbw or classic (input parameter)
     * @param rgbw calculated rgbw (output parameter)
     * @return None
     */
    void convHsv2Rgbw(const Hsv &hsv, const ColorType::ColorType colorType, Rgbw &rgbw);

    /**
     * @brief Sets the gamma  
     * @param aGamma  the gamma
     * @return None
     */
    void setGamma(int aGamma) { gamma = aGamma; }

private:
    int gamma;
};
#endif // !defined(OUTPUT_HSV2GBW_INCLUDED_)
