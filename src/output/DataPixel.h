/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file DataPixel.h
 *  @brief Implementation of the Class DataPixel
 *  @date 2020-05-28
 */

#if !defined(OUTPUT_DATA_PIXEL_INCLUDED)
#define OUTPUT_DATA_PIXEL_INCLUDED

#include "Hsv.h"
#include "Driver.h"
#include "Dispatcher.h"

/**
 * @brief The DataPixel class is intended to contol AFSs
 */

class DataPixel
{
public:
    /**
     * @brief Constructs a new DataPixel object
     * 
     */
    DataPixel();
    /**
     * @brief Destroys the DataPixel object
     * 
     */
    virtual ~DataPixel();

    /**
     * @brief handle input signals for DataPixel
     * @param[in] pixel - light id
     * @param[in] ani - reference on current Animation
     * @param[in] group - scene group
     * @return None
     *
     */
    void input(uint8 pixel, uint8 group, Animation &ani);
    
    /**
     * @brief handle input signals for DataPixel
     * @param[in] actuatorDriver - type of actuator driver
     * @param[in] actuatorAddr - address of actuator
     * @param[in] hsv - Reference to HSV object with output DLI values
     * @return None
     *
     */
    void output( Driver::Driver actuatorDriver, uint8 actuatorAddr, Hsv &hsv );

private:
    const uint8 generalGroup = 0;

    enum AfsLightAddress
    {
        afsLeftAddr = 0u,
        afsRightAddr = 1u,
        iSbbrAddr = 0u
    };
};
#endif // !defined(OUTPUT_DATA_PIXEL_INCLUDED)
