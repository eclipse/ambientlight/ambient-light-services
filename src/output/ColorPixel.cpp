/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file ColorPixel.cpp
 * @brief ColorPixel class definitions
 * @date 2020-05-28
 *
 */

#include "ColorPixel.h"
#include "IRuntime.h"
#include "LightOutputType.h"
#include "Output.h"

ColorPixel::ColorPixel(Output& outputModule) : output(outputModule) {}

void ColorPixel::input(uint8 lightNumber, uint8 group, Animation& anim)
{
	uint8 lightGroup = runtime->getActuatorGroupInclusion()[group + 8u * lightNumber];

    /* A light shouldn't belong to a general group */
    if (lightGroup != generalGroup)
    {
        Hsv newColor;
        uint16 newTargetTime;
        uint8 lightType;

        anim.getHsv(newColor);
        newTargetTime = anim.dimmingTime();
        lightType = runtime->getActuatorTypes()[lightNumber];
        switch (lightType)
        {
        case LightOutputType::ACTUALValue:
			output.currentValueOutput.input(newColor, lightNumber, newTargetTime);
            break;
        case LightOutputType::TARGETValue:
            output.targetValueOutput.input(newColor, lightNumber, newTargetTime);
            break;
        default:
            break;
        }
    }
}
