/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file CurrentValueOutput.h
 *  Implementation of the Class DataPixel
 *  Created on:      28-May-2020 12:48:05
 */

#if !defined(CURRENT_VALUE_OUTPUT_INCLUDED)
#define CURRENT_VALUE_OUTPUT_INCLUDED

#include "Hsv.h"
#include "ColorType.h"
#include "Driver.h"
#include "Rgbw.h"
#include "PlatformTypes.h"
#include "Hsv2Rgbw.h"
#include "IRuntime.h"
#include "Change.h"
#include "Mix.h"
#include "HsvMixing.h"

class CurrentValueOutput
{
    /**
     * @brief Max number of lights
     */
    static const uint8 maxLights = 50u;
    /**
     * @brief Runnable cyclic time
     */
    static const uint8 cyclictime = AMBLS_RUNNABLE_CYCLE;
    /**
     * @brief Target clock multiplier 
     * Assume targetTime tick = 10ms, targetclk_in_ms = targetTime * trgtclkmult
     */
    static const uint8 trgtclkmult = AMBLS_DATASET_TIME_RESOLUTION;
    
public:
    CurrentValueOutput();
    virtual ~CurrentValueOutput();

    /**
     * @brief handle input signals for CurrentValueOutput
     * @param[in] givenHSV - target HSV color for interpolation
     * @param[in] light - light id
     * @param[in] giventime - total time for interpolation
     * @return None
     */
    void input( Hsv &givenHSV, uint8 light, uint16 giventime );

    /**
     * @brief handle output signals for CurrentValueOutput
     * @param[in] light - light id
     * @param[in] rgb - RGB value for light
     * @return None
     *
     */
    void output(uint8 light, Rgbw &rgb);

    /**
     * @brief cyclic function for CurrentValueOutput
     * @param None
     * @return None
     * 
     */
    void cyclic();

#if !defined(AMBLS_UNIT_TEST)  // Tests want to have access to private members
private:
#endif
    /** @todo: memory optimize */
    Hsv currentHSV[maxLights];

    Hsv startHSV[maxLights];       // Start
    Hsv finalHSV[maxLights];       // Final
    Hsv targetHSV[maxLights];      // Target

    uint16 finalTime[maxLights];
    uint16 targetTime[maxLights];

    uint16 currentTime[maxLights];
    uint16 isActive[maxLights];

    boolean initialized;
}; 
#endif // !defined(CURRENT_VALUE_OUTPUT_INCLUDED)

