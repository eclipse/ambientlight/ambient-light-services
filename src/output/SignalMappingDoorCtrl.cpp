/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file SignalMappingDoorCtrl.cpp
 * @brief Implementation of the class SignalMappingDoorCtrl
 * @date 2020-03-13
 */

#include "IRuntime.h"
#include "Output.h"
#include "SignalMappingDoorCtrl.h"
#include "DoorCtrlDimmingData.h"
#include "VehicleState.h"

SignalMappingDoorCtrl::SignalMappingDoorCtrl( Output *output):
    out(output),
    prevReqAmbLighting(FALSE)
{
}

SignalMappingDoorCtrl::~SignalMappingDoorCtrl()
{
}

void SignalMappingDoorCtrl::calcTimeBaseFactor( uint16 dTime, boolean& tBase, uint8& tFactor )
{
    dTime *= AMBLS_DATASET_TIME_RESOLUTION; // to ms

    if ( dTime <= runtime->getDoorCtrlTimeBase0() * maxMultiplier )
    {
        tFactor = static_cast<uint8>( dTime / runtime->getDoorCtrlTimeBase0() );
        tBase = FALSE;
    }
    else if ( dTime <= runtime->getDoorCtrlTimeBase1() * maxMultiplier )
    {
        tFactor = static_cast<uint8>( dTime / runtime->getDoorCtrlTimeBase1() );
        tBase = TRUE;
    }
    else
    {
        tFactor = maxMultiplier;
        tBase = TRUE;
    }
}

void SignalMappingDoorCtrl::getInputSignals(PinMask pMask, uint8 pin, InputSignals& inSig)
{
    switch(pMask)
    {
    case pinDoorCtrlFL:
        inSig.timeBase = (runtime->getTimeBaseVL())[pin] ;
        inSig.timeBaseFactor = (runtime->getTimeBaseFactorVL())[pin];
        inSig.targetBrightness = (runtime->getTargetBrightnessVL())[pin];
        break;

    case pinDoorCtrlFR:
        inSig.timeBase = (runtime->getTimeBaseVR())[pin];
        inSig.timeBaseFactor = (runtime->getTimeBaseFactorVR())[pin];
        inSig.targetBrightness = (runtime->getTargetBrightnessVR())[pin];
        break;

    case pinDoorCtrlRL:
        inSig.timeBase = (runtime->getTimeBaseHL())[pin];
        inSig.timeBaseFactor = (runtime->getTimeBaseFactorHL())[pin];
        inSig.targetBrightness = (runtime->getTargetBrightnessHL())[pin];
        break;

    case pinDoorCtrlRR:
        inSig.timeBase = (runtime->getTimeBaseHR())[pin];
        inSig.timeBaseFactor = (runtime->getTimeBaseFactorHR())[pin];
        inSig.targetBrightness = (runtime->getTargetBrightnessHR())[pin];
        break;

    default:
        break;
    }
}

void SignalMappingDoorCtrl::getOutputSignalsRef(PinMask pMask, OutputSignalsRef& outSigRef)
{
   switch(pMask)
    {
    case pinDoorCtrlFL:
        outSigRef.timeBase = runtime->getTimeBaseVLWriteRef();
        outSigRef.timeBaseFactor = runtime->getTimeBaseFactorVLWriteRef();
        outSigRef.targetBrightness = runtime->getTargetBrightnessVLWriteRef();
        break;

    case pinDoorCtrlFR:
        outSigRef.timeBase = runtime->getTimeBaseVRWriteRef();
        outSigRef.timeBaseFactor = runtime->getTimeBaseFactorVRWriteRef();
        outSigRef.targetBrightness = runtime->getTargetBrightnessVRWriteRef();
        break;

    case pinDoorCtrlRL:
        outSigRef.timeBase = runtime->getTimeBaseHLWriteRef();
        outSigRef.timeBaseFactor = runtime->getTimeBaseFactorHLWriteRef();
        outSigRef.targetBrightness = runtime->getTargetBrightnessHLWriteRef();
        break;

    case pinDoorCtrlRR:
        outSigRef.timeBase = runtime->getTimeBaseHRWriteRef();
        outSigRef.timeBaseFactor = runtime->getTimeBaseFactorHRWriteRef();
        outSigRef.targetBrightness = runtime->getTargetBrightnessHRWriteRef();
        break;

    default:
        break;
    }
}

void SignalMappingDoorCtrl::doorCtrlOutput( PinMask pMask, DoorCtrlDimmingData * const doorCtrlDimmingData )
{
    OutputSignalsRef outSigRef;
    InputSignals inSig;
    uint16 doorCtrlMapping = runtime->getDoorCtrlPinMapping();
    uint16 doorCtrlMappingAmbLighting = runtime->getDoorCtrlPinMappingAmbLighting();
    uint16 i;

    getOutputSignalsRef(pMask, outSigRef);

    for( i=0; i < DoorCtrlDimmingData::doorCtrlPinNumber; i++ )
    {
        // Check if pin is controlled by interior light
        if ( (doorCtrlMapping & (pMask << i)) == 0 )
        {
            getInputSignals(pMask, (uint8)i, inSig);
            outSigRef.timeBase[i] = inSig.timeBase;
            outSigRef.timeBaseFactor[i] = inSig.timeBaseFactor;
            outSigRef.targetBrightness[i] = inSig.targetBrightness;
        }
        else
        {
            boolean currReqAmbLighting = runtime->getReqAmbLighting();

            if ( ( ( doorCtrlMappingAmbLighting & (pMask << i)) != 0u ) &&
                 ( 0u == out->vehicleState->surroundAmbLSPrio ) )
            {
                // request to activate lights trough SAL 
                if ( TRUE == currReqAmbLighting )
                {
                    if ( ( doorCtrlDimmingData[i].value < maxBrightnessValue ) ||
                         doorCtrlDimmingData[i].dimmingTime > runtime->getDimmerTimeSALAmb() )
                    {
                        doorCtrlDimmingData[i].value = maxBrightnessValue;
                        doorCtrlDimmingData[i].dimmingTime = runtime->getDimmerTimeSALAmb();
                    }
                }
                else if ( TRUE == prevReqAmbLighting )  // the request goes 1->0
                {
                    doorCtrlDimmingData[i].dimmingTime = runtime->getDimmerTimeSALAmb();
                }
                else
                {
                    // nothing todo
                }
            }

            // calculate ZeitBasis and ZeitBasisFactor  for 'i' pin of given DoorCtrl
            calcTimeBaseFactor( doorCtrlDimmingData[i].dimmingTime, outSigRef.timeBase[i], outSigRef.timeBaseFactor[i] );
            outSigRef.targetBrightness[i] = doorCtrlDimmingData[i].value;

            prevReqAmbLighting = currReqAmbLighting;
        }
    }
}


void SignalMappingDoorCtrl::output()
{
    doorCtrlOutput( pinDoorCtrlFL, out->doorCtrlDimmingDataFL );
    doorCtrlOutput( pinDoorCtrlFR, out->doorCtrlDimmingDataFR );
    doorCtrlOutput( pinDoorCtrlRL, out->doorCtrlDimmingDataBL );
    doorCtrlOutput( pinDoorCtrlRR, out->doorCtrlDimmingDataBR );
}
