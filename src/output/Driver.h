/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file Driver.h
 *  @brief Implementation of the Enumeration Driver
 *  @date 28-May-2020 12:48:05
 */

#if !defined(OUTPUT_DRIVER_INCLUDED)
#define OUTPUT_DRIVER_INCLUDED

namespace Driver
{
    enum Driver
    {
        SAL = 0,
        DCR = 1,
        AFS = 2,
        iSBBR =3
    };
}
#endif // !defined(OUTPUT_DRIVER_INCLUDED)
