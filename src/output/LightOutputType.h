/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file LightOutputType.h
 * @brief Light output types definitions
 * @date 2020-05-29
 *
 */

#if !defined(LIGHT_OUTPUT_TYPE_INCLUDED)
#define LIGHT_OUTPUT_TYPE_INCLUDED

namespace LightOutputType
{
    /**
     * @brief Aviliable output light types
     *
     */
    enum LightOutputType
    {
        ACTUALValue = 0u,
        TARGETValue = 1u,
        Data = 2u,
        Reserved = 3u
    };
} 
#endif // LIGHT_OUTPUT_TYPE_INCLUDED