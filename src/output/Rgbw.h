/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file Rgbw.h
 *  @brief Implementation of the structure Rgbw
 *  @date  2020-02-28
 */

#if !defined(OUTPUT_RGBW_INCLUDED_)
#define OUTPUT_RGBW_INCLUDED_

/**
 * @brief The structure Rgbw contains red, green, blue and white color values
 * 
 */

struct Rgbw
{
    /**
     * @brief numRed red color value
     * 
     */
    int numRed;

    /**
     * @brief numRed green color value
     * 
     */
    int numGreen;

    /**
     * @brief numRed blue color value
     * 
     */
    int numBlue;
    
    /**
     * @brief numWhite white color value
     * 
     */
    int numWhite;
};
#endif // !defined(OUTPUT_RGBW_INCLUDED_)
