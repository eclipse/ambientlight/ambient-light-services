/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file TargetValueOutput.h
 * @brief TargetValueOuptut class declaration
 * @date 2020-05-29
 */

#if !defined(TARGET_VALUE_OUTPUT_INCLUDED)
#define TARGET_VALUE_OUTPUT_INCLUDED

#include "ColorType.h"
#include "Driver.h"
#include "Hsv.h"
#include "PlatformTypes.h"
#include "Hsv2Rgbw.h"

class Output;

/**
 * @brief This class allows to prepare light transition parameters for
 * "Signal Mapping TSG" module
 *
 */
class TargetValueOutput
{

public:
    /**
     * @brief Construct a new Target Value Output object
     *
     */
    TargetValueOutput(Output& outputRef);
    /**
     * @brief Destroy the Target Value Output object
     *
     */
    ~TargetValueOutput();
    /**
     * @brief Sets light control parameters for TSG endpoint such as:
     *  - Dimming time
     *  - Target color
     * 
     * All modified parameters are used by "Signal mapping TSG module" later on
     * 
     * @param targetColor Target color which light should smoothly take
     * @param lightNumber Light number
     * @param dimmingTime Color transition duration
     */
    void input(Hsv& targetColor, uint8 lightNumber, uint16 dimmingTime);

private:
    Output& output;
    Hsv2Rgbw colorConverter;
    enum DCRGroup 
    {
        FrontLeft = 4u,
        FrontRight = 8u,
        RearLeft = 12u,
        RearRight = 16u
    };
};
#endif // !defined(TARGET_VALUE_OUTPUT_INCLUDED)
