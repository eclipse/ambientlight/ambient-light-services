/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Output.h
 * @brief Implementation of the class Output
 * @date 2020-03-12
 */

#if !defined(OUTPUT_OUTPUT_INCLUDED_)
#define OUTPUT_OUTPUT_INCLUDED_

#if !defined(MOCK_Output)

#include "DoorCtrlDimmingData.h"
#include "SignalMappingDoorCtrl.h"
#include "ColorPixel.h"
#include "DataPixel.h"
#include "TargetValueOutput.h"
#include "CurrentValueOutput.h"

class VehicleState;

class Output
{
public:
    Output(VehicleState *vState);
    ~Output();

    void init();
    void cycle();

    DoorCtrlDimmingData doorCtrlDimmingDataFL[DoorCtrlDimmingData::doorCtrlPinNumber];
    DoorCtrlDimmingData doorCtrlDimmingDataFR[DoorCtrlDimmingData::doorCtrlPinNumber];
    DoorCtrlDimmingData doorCtrlDimmingDataBL[DoorCtrlDimmingData::doorCtrlPinNumber];
    DoorCtrlDimmingData doorCtrlDimmingDataBR[DoorCtrlDimmingData::doorCtrlPinNumber];

    VehicleState *vehicleState;
    ColorPixel colorPixel;
    DataPixel dataPixel;
    TargetValueOutput targetValueOutput;
	CurrentValueOutput currentValueOutput;
    SignalMappingDoorCtrl signalMappingDoorCtrl;
};
#else

#include "MockOutput.h"

#endif

#endif //!defined(OUTPUT_OUTPUT_INCLUDED_)
