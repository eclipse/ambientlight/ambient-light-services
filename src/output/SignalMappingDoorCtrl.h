/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file SignalMappingDoorCtrl.h
 * @brief Implementation of the class SignalMappingDoorCtrl
 * @date 2020-03-13
 */

#if !defined(OUTPUT_SIGNAL_MAPPING_DOORCTRL_INCLUDED_)
#define OUTPUT_SIGNAL_MAPPING_DOORCTRL_INCLUDED_

class Output;


/**
 * @brief The SignalMappingDoorCtrl class is intended to implement 'signal
 * mapping door control' fuctionality.
 */
class SignalMappingDoorCtrl
{

public:
    SignalMappingDoorCtrl(Output *out);
    virtual ~SignalMappingDoorCtrl();

    //    void input();

    /**
     * @brief Outputs target brightness and dimming times on RTE
     * 
     * @param none
     *
     * @return none
     */
    void output();

#if !defined(AMBLS_UNIT_TESTS)  // Tests want to have access to private members
private:
#endif    

    /**
     * @brief Bit masks of pins for each DoorCtrl in the parameters 
     *     p_DoorCtrl_Pinmapping  and 
     *     p_DoorCtrl_Pinmapping_Umfeldbeleuchtung. 
     */
    enum PinMask
    {
        pinDoorCtrlFL = 0x0001u,
        pinDoorCtrlFR = 0x0010u,
        pinDoorCtrlRL = 0x0100u,
        pinDoorCtrlRR = 0x1000u
    };

    // Helper structure with RTE with input signals for a pin of specific DoorCtrl
    struct InputSignals
    {
        // timeBase for a pin of specific DoorCtrl
        boolean timeBase;
        // timeBaseFactor for a pin of specific DoorCtrl
        uint8 timeBaseFactor;
        // TargetBrightness for a pin of specific DoorCtrl
        uint8 targetBrightness;
    };

    // Helper structure with pointers to RTE array of output signals for a DoorCtrl
    struct OutputSignalsRef
    {
        // pointer to RTE array of output timeBase values for a DoorCtrl
        boolean * timeBase;
        // pointer to RTE array of output timeBaseFactor values for a DoorCtrl
        uint8 * timeBaseFactor;
        // pointer to RTE array of output targetBrightness values for a DoorCtrl
        uint8 * targetBrightness;
    };

    // Pointer to global object of Output class
    Output *out;

    // Previous state of request SAL ambient lights input signal (bAnfUmfeldLuminaries)
    boolean prevReqAmbLighting;
    
    // MaxMultiplikator 2^4 - 1
    static const uint16 maxMultiplier = 15u;

    // Max HSV value (brightness)
    static const uint8 maxBrightnessValue = 100u;
    
    /**
     * @brief Outputs dimming time, timeBase and timeBaseFactor values on RTE for specific DoorCtrl
     * 
     * @param[in] pMask - pin mask for specific DoorCtrl
     * @param[out] doorCtrlDimmingData - pin mask for specific DoorCtrl
     *
     * @return none
     */
    void doorCtrlOutput( PinMask pMask, DoorCtrlDimmingData *doorCtrlDimmingData );
    

    /**
     * @brief Calculates values of TimeBase and TimeFactor for a dimming time.
     * 
     * @param[in] dTime - dimming time in msec,
     * @param[out] timeBase - reference to TimeBase
     * @param[out] timefactor - reference to TimeFactor
     *
     * @return none
     */
    static void calcTimeBaseFactor(uint16 dTime, boolean& timeBase, uint8& timeFactor);

    /**
     * @brief reads RTE input signals related to dimming data
     * 
     * @param[in] pMask - pin mask for specific DoorCtrl
     * @param[in] pin - pin id (0..3)
     * @param[out] inSig - the structure to save input signals
     *
     * @return none
     */
    void getInputSignals(PinMask pMask, uint8 pin, InputSignals& inSig);

    /**
     * @brief returns references to RTE output signals arrays
     * 
     * param[in] pMask - pin mask for specific DoorCtrl
     * param[out] outSigRef - the structure to save the references
     *
     * @return none
     */
    void getOutputSignalsRef(PinMask pMask, OutputSignalsRef& outSigRef);

};
#endif // !defined(OUTPUT_SIGNAL_MAPPING_DOORCTRL_INCLUDED_)
