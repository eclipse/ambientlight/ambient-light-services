/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file ColorPixel.h
 * @brief ColorPixel class declaration
 * @date 2020-05-28
 * 
 */

#if !defined(COLOR_PIXEL_INCLUDED)
#define COLOR_PIXEL_INCLUDED

#include "Hsv.h"
#include "Curve.h"
#include "PlatformTypes.h"
#include "Animation.h"

class Output;

/**
 * @brief This class forwards time and color values to an appropriate
 * output module
 * 
 */
class ColorPixel
{

public:
    /**
     * @brief Constructs a new Color Pixel object
     * 
     */
    ColorPixel(Output& outputModule);
    /**
     * @brief Destroys the Color Pixel object
     * 
     */
    virtual ~ColorPixel(){};

    /**
     * @brief Feeds necessary input data to underlying output modules
     * @param lightNumber Number of engaged light
     * @param lightGroup Group to which selected light belongs to
     * @param anim Animation related data
     */
    void input(uint8 lightNumber, uint8 lightGroup, Animation& anim);
private:
    Output& output;
    /**
     * @brief This group is called "Global".
     * All lights within this group are engaged into action.
     */
    const uint8 generalGroup = 0u;
};
#endif // !defined(COLOR_PIXEL_INCLUDED)
