/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file LHOState.h
 * @brief Implementation of the Class LHOState
 * @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_LHO_STATE_INCLUDED_)
#define VEHICLE_STATE_LHO_STATE_INCLUDED_

#include "AbstractState.h"
#include "LhoStateInput.h"


/**
 * @brief This class is intended to handle incoming input events in
 * "Leaving Home" state.
 *
 * @warning It's assumed that this state starts working only after one on the
 * following events was emitted before:
 * - Event::RadioUnlock;
 * - Event::RearDoorOpen.
 */

#if !defined(MOCK_LHOState)

class LHOState : public AbstractState
{
public:
    /**
     * @brief Construct a new LHOState::LHOState object
     * @param vState Global execution context
     */
    explicit LHOState( VehicleState* vState );

    /**
     * @brief Destroy the LHOState::LHOState object
     *
     * Default class destructor
     *
     */
    virtual ~LHOState();

    /**
     * @brief Does LHO startup actions:
     * @param None
     * @return None
     */
    virtual void entryAction();
    /**
     * @brief Handles input events while "LHO" state is active. 
     * @param Event event to be handled
     * @return None
     */
    virtual void handleEvent( InputType::InputType event );
    /**
     * @brief Cycle function of state. It does nothing.
     * @param None
     * @return None
     */
    virtual void cycle();
    /**
     * @brief Intended to Handle exit actions. It does nothing for now.
     * @param None
     * @return None
     */
    virtual void exitAction();

#if !defined(AMBLS_UNIT_TESTS)
private:
#else
    // public the members in order to test them
#endif     
    /**
     * @brief Outputs the event related to LHO.
     *        Usually this function can be called only if the blink timer expired.
     * @param inputEvent LHO input event
     * @return None
     */
    void outputEvent( LhoStateInput::LhoInputEvent inputEvent );
    
    // Allowed transitions
    Transition toIdleState;
    Transition toLhoRunState;
};

#else

#include "MockLHOState.h"

#endif // !defined(MOCK_LHOState)

#endif // !defined(VEHICLE_STATE_LHO_STATE_INCLUDED_)
