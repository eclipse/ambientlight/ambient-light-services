/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file BlinkingOverlay.cpp
 *  @brief Implementation of the Class BlinkingOverlay
 *  @date 2019-11-28
 */

#include "VehicleState.h"
#include "BlinkingStateInput.h"

#include "BlinkingOverlay.h"

extern VehicleState vehicleState;

BlinkingOverlay::BlinkingOverlay():
    active(false)
{
    /** @todo Auto-generated constructor stub */

}

BlinkingOverlay::~BlinkingOverlay()
{
    /** @todo Auto-generated destructor stub */
}

bool BlinkingOverlay::isWarningLight() const
{
    /* for now all required functionality related to warning lights has to be ignored
     * in future we have to check here if
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * vehicleState.BlinkDelay() differs from zero AND
     * vehicleState.blinkingInputs.WarningLightsActive() is true
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * until then this function returns false
     */
#if defined(OVERLAY_INCHARGE)
    return ( ( 0 != vehicleState.BlinkDelay() ) && vehicleState.blinkingInputs.WarningLightsActive() );
#else
    return false;
#endif
}

void BlinkingOverlay::cyclicRead()
{
#if defined(OVERLAY_INCHARGE)
    if(active)
    {
        if(!isWarningLight())
        {
            active = false;
        }
    }
    else
    {
        if(isWarningLight())
        {
            active = true;
        }
    }
#else
    active = false;
#endif
}

InputType::InputType BlinkingOverlay::filterEvent( InputType::InputType event )
{
#if defined(OVERLAY_INCHARGE)
    InputType::InputType ret = InputType::None;
    if ( !active ||
            ( InputType::ChoTimerExpired == event ) ||
            ( InputType::LhoTimerExpired == event ) ||
            ( InputType::Door == event ) || // open or close of doors may change the timer
            ( InputType::Terminal == event ) // maybe switch back to idle state
       )
    {
        ret = event;
    }
    return ret;
#else
    return event;
#endif
}
