/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file LhoTimer.h
 *  @brief Implementation of the class for LHO timer. 
 *  @date 2019-11-11
 */

#if !defined(VEHICLE_STATE_LHO_TIMER_INCLUDE_)
#define VEHICLE_STATE_LHO_TIMER_INCLUDE_

#include "AbstractTimer.h"
#include "InputType.h"

#if !defined(MOCK_Timers)

/**
 * @brief Class LhoTimer
 * This class implementes LHO timer.
 * LHO timer is derived from AbstractTimer class and 
 * intendend to be used in LHO states. 
 */
class LhoTimer: public AbstractTimer
{

public:
    /**
     * @brief Construct a new LhoTimer object
     * @param vState Global execution context
     */
    LhoTimer( VehicleState* vState );

    /**
     * @brief Default destructor for LhoTimer
     */
    virtual ~LhoTimer();

    /**
     * @brief Starts timer with value from RTE (LHActiveTime) and changes state of timer to 'Run'.
     * @param timerValue LHO timer is started with value from RTE, 
     *        so timerValue isn't used here.
     */
    virtual void start( uint32 timerVal = 0 );

private:
    /**
     * @brief Called if the timer expired 
     * It calls currentState->handleEvent with the event InputType::LhoTimerExpired
     */
    virtual void onChange();
};

#else

#include "MockLhoTimer.h"

#endif // !defined(MOCK_Timers)


#endif // !defined(VEHICLE_STATE_LHO_TIMER_INCLUDE_)
