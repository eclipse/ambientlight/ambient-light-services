/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file ChoTimers.cpp
 *
 *  @brief Implementation of the classes for both CHO timers. 
 *
 *  @date 2019-11-05
 */

#include "VehicleState.h"
#include "ChoTimers.h"



// CHO timer implementation

ChoTimer::ChoTimer(VehicleState* vState) :
    AbstractTimer(vState) 
{
}


ChoTimer::~ChoTimer()
{
}


void ChoTimer::start( uint32 timerVal )
{
    value = secs2ticks( timerVal );
    state = Run;
}

void ChoTimer::onChange(void)
{
    vehicleState->stateMachine.handleEvent( InputType::ChoTimerExpired );
}


// CHO exit timer implementation

ChoExitTimer::ChoExitTimer(VehicleState* vState) :
    AbstractTimer(vState) 
{
}

ChoExitTimer::~ChoExitTimer()
{
}

void ChoExitTimer::start( uint32 timerVal )
{
    value = secs2ticks( timerVal );
    state = Run;
}

void ChoExitTimer::onChange(void)
{
    vehicleState->stateMachine.handleEvent( InputType::ChoExitTimerExpired );
}
