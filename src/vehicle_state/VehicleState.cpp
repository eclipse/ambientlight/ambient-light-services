/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file VehicleState.cpp
 *  @brief Implementation of the Class VehicleState
 *  @date  2019-10-25
 */

#include "VehicleState.h"
#include "Dispatcher.h"

VehicleState::VehicleState():
    blinkingInputs(this),
    centralLockingInputs(this),
    choLhoInputs(),
    doorInputs(this),
    driverPresencInputs(),
    lightSensorInputs(),
    lightSwitchInputs(),
    terminalInputs(this),
    vehicleInputs(this),
    lhoInputs(this),
    stateMachine(this),
    blinkOverlay(),
    choTimer(this),
    choExitTimer(this),
    lhoTimer(this),
    surroundAmbLSPrio(0),
    ambLSAFSStagingState(0),
    ambLSModificationReason(0),
    ambLSModificationState(false),
    ambLSPerCntr(0),
    nLhoPlay(0),
    nLhoPlayCfg(0),
    initialized(false),
    prevStateId(StateIds::none)
{

}

VehicleState::~VehicleState()
{

}


void VehicleState::cycle()
{
    centralLockingInputs.cyclicRead();
    choLhoInputs.cyclicRead();
    doorInputs.cyclicRead();
    driverPresencInputs.cyclicRead();
    lightSensorInputs.cyclicRead();
    lightSwitchInputs.cyclicRead();
    terminalInputs.cyclicRead();
    vehicleInputs.cyclicRead();
    blinkingInputs.cyclicRead();  // this cyclicRead must be called last because it also runs/checks blinkTimer
    choTimer.run();
    choExitTimer.run();
    lhoTimer.run();
    blinkOverlay.cyclicRead();
    stateMachine.currentStateCycle();
    output();
}


void VehicleState::dispatch(Event::Event event)
{
    /** @todo dispatch event */ 
    //    runtime->setEvent(event);

    dispatcher.dispatch(event);
}

/**
 * @brief Checks whether or not events are allowed.
 *
 * @return true if events are allowed, otherwise returns false
 */
bool VehicleState::isEventEnabled()
{
    return blinkingInputs.isEventEnabled();
}


void VehicleState::output()
{
    runtime->setSurroundAmbLSPrio(surroundAmbLSPrio);
    runtime->setAmbLSAFSStagingState(ambLSAFSStagingState);
    runtime->setAmbLSModificationReason(ambLSModificationReason);
    runtime->setAmbLSModificationState(ambLSModificationState);
    runtime->setAmbLSPerCntr(ambLSPerCntr);
}


///
/// Initialization of VehicleState
///

void VehicleState::init()
{
    // Output event 31 after WakeUp
    dispatch( Event::Init );

    ambLSModificationReason = 0; /** @todo <Defaultwert>? */
    ambLSModificationState = 0; /** @todo <Defaultwert>? */
    ambLSPerCntr++;
    
    // Bahnkurven_aktivieren == "aktiv"
    ambLSAFSStagingState = ( true == ActivateCurves() );

    ResetLhoDynamicProtection();

    doorInputs.init(); // Remember initial doors status

    stateMachine.init();
    
    initialized = true;
}

bool VehicleState::isActiveState()
{
    return ( ( false == terminalInputs.Terminal15() ) &&
             ( true  == lightSensorInputs.LightSensorOnChoLho()) );
}

bool VehicleState::isChoReady() 
{
    return (
        ( choLhoInputs.CHActiveTime() > 0 ) &&  // CHO active time
        ( true == choLhoInputs.CHState(0) ) &&  // ON
        ( true == choLhoInputs.CHState(1) )     // Ambiance
    );
}

bool VehicleState::isLhoReady() 
{
    uint8 nPlay = LhoDynamicProtection();

    // Reset Lho protection if the configuration parameter changed
    if ( nPlay != nLhoPlayCfg ) 
    {
        ResetLhoDynamicProtection();
    }
    
    return (
        ( true == LhoRelease() )            &&  // LHO option coded
        ( choLhoInputs.LHActiveTime() > 0 )  &&  // LHO active time
        ( true == choLhoInputs.LHState(0) )  &&  // ON
        ( true == choLhoInputs.LHState(1) )  &&  // Ambiance
        ( nLhoPlay > 0 )                         // s_Zaehler_Spielschutz > 0
    );
}

