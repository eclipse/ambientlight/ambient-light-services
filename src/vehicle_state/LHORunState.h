/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file LHORunState.h
 * @brief This file contains data types needed to define "Leaving Home Run"
 * state operation
 * @version 0.1
 * @date 2019-12-02
 */

#if !defined(VEHICLE_STATE_LHO_RUN_STATE_INCLUDE_)
#define VEHICLE_STATE_LHO_RUN_STATE_INCLUDE_

#include "AbstractState.h"
#include "LhoStateInput.h"

/**
 * @brief This class is intended to handle incoming input events in
 * "Leaving Home Run" state.
 *
 * @warning It's assumed that this state starts working only after one on the
 * following events was emitted before:
 * - Event::RadioUnlock;
 * - Event::RearDoorOpen.
 */

#if !defined(MOCK_LHORunState)

class LHORunState : public AbstractState
{
public:
    explicit LHORunState( VehicleState* vState );
    virtual ~LHORunState();

    /**
     * @brief "LHO run" entry action.
     */
    virtual void entryAction();

    /**
     * @brief Handles input events while "LHO run" state is active
     * @param iEvent Input event to handle
     *
     * If "Warning blink" is active all incoming events will be ignored.
     * Otherwise the following events can be handled:
     * - Car is unlocked by radio key (FFB);
     * - Trunk is opened by driver request, back door button or virtual pedal;
     * - Car is locked by radio key (FFB);
     *
     * Event processing starts only after blinking timer is expired.
     */
    virtual void handleEvent( InputType::InputType event );

    /**
     * @brief "LHO run" virtual cycle function. It has empty implementation 
     *        in this class.
     */
    virtual void cycle();

    /**
     * @brief "LHO run" state termination point
     *
     * LHO run state terminates after LHO timer expires or terminal 15 signal rises
     * from 0 to 1
     */
    virtual void exitAction();
    

#if !defined(AMBLS_UNIT_TESTS) // Tests must have access to private members
private:
#endif    
    /**
     * @brief Outputs the event related to LHO.
     *        Usually this function can be called only if the blink timer expired.
     * @param inputEvent LHO input event
     * @return None
     */
    void outputEvent( LhoStateInput::LhoInputEvent inputEvent );

    /**
     * @brief Allows to transit to "Idle" state if animation is done
     */
    Transition toIdleState;
};
#else

#include "MockLHORunState.h"

#endif // !defined(MOCK_LHORunState)

#endif // !defined(VEHICLE_STATE_LHO_RUN_STATE_INCLUDE_)
