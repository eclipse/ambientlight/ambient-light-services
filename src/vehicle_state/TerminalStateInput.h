/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
*/

/**
 *  @file TerminalStateInput.h
 *  @brief Implementation of the Class TerminalStateInput
 *  @date  2019-10-25
 */
#if !defined(VEHICLE_STATE_TERMINAL_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_TERMINAL_STATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

class VehicleState;

#if !defined(MOCK_TerminalStateInput)
/**
 * @brief This class is intended to monitor all available terminals
 * 
 */
class TerminalStateInput : public StateInput
{

public:
    /**
     * @brief Constructs a new TerminalStateInput object
     * 
     * @param vState[in] Global VehicleState context
     */
    TerminalStateInput(VehicleState* vState);
    /**
     * @brief Destroys the TerminalStateInput object
     * 
     */
    virtual ~TerminalStateInput();
    /**
     * @brief Reads Terminal 15 current value
     * @return true if Terminal 15 current level is high, otherwise returns
     * false 
     */
    inline boolean Terminal15(){ return runtime->getTerminal15(); }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead();
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange();

private:
    VehicleState* vehicleState;
    boolean terminal15;

};

#else

#include "MockTerminalStateInput.h"

#endif // !defined(MOCK_TerminalStateInput)

#endif // !defined(VEHICLE_STATE_TERMINAL_STATE_INPUT_INCLUDED_)
