/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file LightSwitchStateInput.h
 *  @brief Implementation of the Class LightSwitchStateInput
 *  @date  2019-10-25
 */

#if !defined(VEHICLE_STATE_LIGHT_SWITCH_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_LIGHT_SWITCH_STATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

#if !defined(MOCK_LightSwitchStateInput)
/**
 * @brief This class allows to monitor Light switches
 * 
 */
class LightSwitchStateInput : public StateInput
{

public:
    /**
     * @brief Constructs a new LightSwitchStateInput object
     * 
     */
    LightSwitchStateInput(){}
    /**
     * @brief Destroy the Light Switch State Input object
     * 
     */
    virtual ~LightSwitchStateInput(){}
    /**
     * @brief Gets "Assistant Driving Light" status
     * @return true if "Assistant Driving Light" function is enabled, otherwise returns false
     */
    inline boolean StLdsAssistantDrivingLight() { return runtime->getStLdsAssistantDrivingLight(); }
    /**
     * @brief Gets "Dimmer Head Light" status
     * @return true if "Dimmer Head Light" function is activated
     */
    inline boolean StLdsDimmedHeadlight() { return runtime->getStLdsDimmedHeadlight(); }
    /**
     * @brief Gets status of a head light flasher (light flashes following car 
     * turns) switch
     * @return true if switch is on, otherwise returns false
     */
    inline boolean StLssRequestHeadlightFlasherPlOff() { return runtime->getStLssRequestHeadlightFlasherPlOff(); }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead(){}
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange(){}

};

#else

#include "MockLightSwitchStateInput.h"

#endif // !defined(MOCK_LightSwitchStateInput)

#endif // !defined(VEHICLE_STATE_LIGHT_SWITCH_STATE_INPUT_INCLUDED_)
