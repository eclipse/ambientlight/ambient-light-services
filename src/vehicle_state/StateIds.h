/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file StateIds.h
 *  @brief Implementation of the Enumeration StateIds
 *  @date  2019-10-25
 */
#if !defined(VEHICLE_STATE_STATE_IDS_INCLUDED_)
#define VEHICLE_STATE_STATE_IDS_INCLUDED_

namespace StateIds
{
    /**
     * @brief State numeric ID
     * 
     */
    enum StateIds
    {
        /**
         * @brief Unknown state
         * 
         */
        none,
        /**
         * @brief Idle state
         * 
         */
        idle=1,
        /**
         * @brief Coming home state
         * 
         */
        cho,
        /**
         * @brief Coming home run state
         * 
         */
        choRun,
        /**
         * @brief Leaving home state
         * 
         */
        lho,
        /**
         * @brief Leaving home run state
         * 
         */
        lhoRun
    };
}

#endif // !defined(VEHICLE_STATE_STATE_IDS_INCLUDED_)
