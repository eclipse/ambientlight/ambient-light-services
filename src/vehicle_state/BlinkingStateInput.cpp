/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file BlinkingStateInput.cpp
 * @brief Implementation of the Class BlinkingStateInput
 * @date 2019-10-25
 */

#include "VehicleState.h"
#include "BlinkingStateInput.h"

BlinkingStateInput::BlinkingStateInput(VehicleState *vState):
    vehicleState(vState),
    blinkTimer(vState),
    eventEnabled(false)
{
}

BlinkingStateInput::~BlinkingStateInput()
{
}

void BlinkingStateInput::cyclicRead()
{
    boolean enabled, expired;

    blinkTimer.run();
    expired = blinkTimer.expiredEvent;

    enabled =
        ( false != vehicleState->BlinkDelay() ) &&
        ( false == CentralLockingCloseBlinkingActive() ) &&
        ( false == CentralLockingOpenBlinkingActive() ) &&
        ( false == blinkTimer.isStarted() );

    if ( ( ( eventEnabled != enabled ) || (true == expired) )  &&
         ( true == enabled ) )
    {
        onChange();
    }

    eventEnabled = enabled;
    blinkTimer.expiredEvent = false;
}

void BlinkingStateInput::onChange()
{
    vehicleState->stateMachine.handleEvent( InputType::EventRelease );
}
