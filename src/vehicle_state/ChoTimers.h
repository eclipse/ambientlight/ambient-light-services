/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file ChoTimers.h
 *  @brief Implementation of the classes for both CHO timers. 
 *  @date 2019-11-05
 */

#if !defined(VEHICLE_STATE_CHO_TIMERS_INCLUDED_)
#define VEHICLE_STATE_CHO_TIMERS_INCLUDED_

#include "AbstractTimer.h"


#if !defined(MOCK_Timers)

/**
 * @brief Class ChoTimer implementes CHO timer.
 *        This class is derived from AbstractTimer class.
 */
class ChoTimer: public AbstractTimer
{

public:
    /**
     * @brief Construct a new ChoTimer object
     * @param vState Global execution context
     */
    ChoTimer( VehicleState* vState );
    /**
     * @brief Default destructor for ChoTimer
     */
    virtual ~ChoTimer();

    /**
     * @brief Starts timer with given value and changes state of timer to 'Run'.
     * @param timerValue - timer value in secs.
     */
    void start( uint32 timerVal );

private:    
    virtual void onChange();
};


/**
 * @brief Class ChoExitTimer implementes CHO exit timer.
 *        This is derived from AbstractTimer class.
 */
class ChoExitTimer: public AbstractTimer
{

public:
    /**
     * @brief Construct a new ChoExitTimer object
     * @param vState Global execution context
     */
    ChoExitTimer( VehicleState* vState );

    /**
     * @brief Default destructor for ChoExitTimer
     */
    virtual ~ChoExitTimer();

    /**
     * @brief Starts timer with given value and changes state of timer to 'Run'.
     * @param timerVal - timer value in secs
     */
    void start( uint32 timerVal = 0 );

private:
    virtual void onChange();
};

#else

#include "MockChoTimers.h"

#endif // !defined(MOCK_Timers)

#endif // !defined(VEHICLE_STATE_CHO_TIMERS_INCLUDED_)
