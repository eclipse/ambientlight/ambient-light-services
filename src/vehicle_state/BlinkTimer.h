/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file BlinkTimer.h
 *  @brief Implementation of the class of blink timer. 
 *  @date 2019-11-05
 */

#if !defined(VEHICLE_STATE_BLINK_TIMER_INCLUDED_)
#define VEHICLE_STATE_BLINK_TIMER_INCLUDED_

#include "AbstractTimer.h"
#include "InputType.h"

/**
 * @brief This class implements a Blink timer
 */

#if !defined(MOCK_Timers)
class BlinkTimer: public AbstractTimer
{

public:
    /**
     * @brief Construct a new BlinkTimer object
     * @param vState Global execution context
     */
    BlinkTimer( VehicleState* vState );

    /**
     * @brief Default destructor for BlinkTimer
     */
    virtual ~BlinkTimer();

    /**
     * @brief Starts the timer
     * @param timerValue Blink timer is started with default value always, so timerValue isn't used
     */
    virtual void start( uint32 timerValue = 0 );

    /**
     * @brief Handles input events
     * @param event Input event to be handled
     *
     * Following input events are handled:
     * - InputType::CentralLocking
     * - InputType::Vehicle
     *
     * If the condition is true and the timer is not started,
     * the timer is started by calling start()
     */
    void handleEvent( InputType::InputType event );

    bool expiredEvent;
    
private:    
    /**
     * @brief Called if the timer expired 
     * It calls currentState->handleEvent with the event InputType::BlinkTimerExpired
     */
    virtual void onChange();

};

#else

#include "MockBlinkTimer.h"

#endif // !defined(MOCK_Timers)

#endif // !defined(VEHICLE_STATE_BLINK_TIMER_INCLUDED_)
