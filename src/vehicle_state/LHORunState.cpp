/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file LHORunState.cpp
 * @brief "LHO run" state implementation
 * @version 0.1
 * @date 2019-12-02
 *
 * This file contains main LHORunState class definitions
 */

#include "LHORunState.h"

#include "VehicleState.h"

/**
 * @brief Construct a new LHORunState::LHORunState object
 * @param vState Global execution context
 */
LHORunState::LHORunState( VehicleState* vState )
    : AbstractState(vState, StateIds::lhoRun),
#if !defined(AMBLS_UNIT_TESTS)    
      toIdleState( &vehicleState->stateMachine.idleState )
#else
      // It simplifies mocking up StateMachine class
      toIdleState(0)
#endif    
{
}

/**
 * @brief Destroy the LHORunState::LHORunState object
 *
 * Default class destructor
 *
 */
LHORunState::~LHORunState()
{
}

/**
 * @brief Does LHO startup actions
 *
 */
void LHORunState::entryAction()
{
    if (true == vehicleState->blinkOverlay.isWarningLight())
    {
        vehicleState->dispatch(Event::LhoWarnBlink);
        vehicleState->stateMachine.transit(&toIdleState);
    }
}

void LHORunState::handleEvent(InputType::InputType iEvent)
{
    InputType::InputType filteredEvent = vehicleState->blinkOverlay.filterEvent(iEvent);

    /* Terminate LHO in case of rising edge */
    if (InputType::Terminal == filteredEvent && true == vehicleState->terminalInputs.Terminal15())
    {
        /* Reset LHO protection */
        vehicleState->ResetLhoDynamicProtection();

        // Reset last LHO input event
        vehicleState->lhoInputs.ResetLastInputEvent();
        
        // -> idle
        vehicleState->stateMachine.transit(&toIdleState);
    }
    
    /* Handle the last happened event after a blinking delay */
    else if (InputType::EventRelease == filteredEvent)
    {
        outputEvent(vehicleState->lhoInputs.LastInputEvent());
    }
    
    /* LHO timer expired */
    else if (InputType::LhoTimerExpired == filteredEvent)
    {
        vehicleState->dispatch(Event::LhoTimerExpired);
        /* Implicitly defined in */
        vehicleState->stateMachine.transit(&toIdleState);
    }
    
    /* Store the last event spawning LHO activity  */
    else
    {
        vehicleState->lhoInputs.InputEvent( filteredEvent, stateId );
    }
}

void LHORunState::outputEvent( LhoStateInput::LhoInputEvent inputEvent )
{
    Event::Event outEvent = Event::Init;
    
    switch ( inputEvent )
    {
    case LhoStateInput::RadioUnlock:
        if (vehicleState->nLhoPlay > 0)
        {
            outEvent = Event::SequenceRadioUnlock;
            vehicleState->nLhoPlay--;
            vehicleState->lhoTimer.start();
        }
        break;
    case LhoStateInput::HDOpen:
        if (vehicleState->nLhoPlay > 0)
        {
            outEvent = Event::SequenceRearDoorOpen;
            vehicleState->nLhoPlay--;
            vehicleState->lhoTimer.start();
        }
        break;
    case LhoStateInput::RadioLock:
        outEvent = Event::ChoLockRadio;
        break;
            
    default:
        break;
    }
            
    if ( outEvent != Event::Init )
    {
        vehicleState->dispatch(outEvent);
    }
    
    /* Prevent repeated events */
    vehicleState->lhoInputs.ResetLastInputEvent();
}

void LHORunState::cycle()
{

}

void LHORunState::exitAction()
{
}
