/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file IdleState.cpp
 * @brief Implementation of the Class IdleState
 * state operation
 * @date 2019-10-25
 */

#include "VehicleState.h"

#include "IdleState.h"

IdleState::IdleState( VehicleState * vState ) :
    AbstractState( vState, StateIds::idle ),
    defaultTransition( this ),
#if !defined(AMBLS_UNIT_TESTS)    
    toChoState( &vehicleState->stateMachine.choState ),
    toLhoState( &vehicleState->stateMachine.lhoState )
#else
    // It simplifies mocking up StateMachine class
    toChoState(0),
    toLhoState(0)
#endif    
    
{
}


IdleState::~IdleState()
{

}

void IdleState::entryAction()
{
    if ( StateIds::none != vehicleState->prevStateId )
    {
        vehicleState->dispatch( Event::SwitchOffStaging );
        vehicleState->surroundAmbLSPrio = 0u;
    }
    
    vehicleState->blinkingInputs.blinkTimer.stop();
    vehicleState->lhoTimer.stop();
}

void IdleState::handleEvent( InputType::InputType event )
{
    if (true == vehicleState->terminalInputs.Terminal15())
    {
        vehicleState->ResetLhoDynamicProtection();
    }
    if ( true == vehicleState->isActiveState() )
    {
        
        // KL15's gone to off and
        if ( ( InputType::Terminal == event ) && (true == vehicleState->isChoReady()) )
        {
            // to CHO state
            vehicleState->stateMachine.transit( &toChoState );
        }
        else if ( ( true == vehicleState->isLhoReady() )  &&
                  ( LhoStateInput::None != vehicleState->lhoInputs.InputEvent( event, stateId ) ) )
        {
            // to LHO state
            vehicleState->stateMachine.transit( &toLhoState );
        }
        else
        {
            // MISRA
        }
    }
}

void IdleState::cycle()
{

}

void IdleState::exitAction()
{
    // exit actions can be executed here
    // ...
}

