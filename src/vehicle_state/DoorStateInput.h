/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file DoorStateInput.h
 *  @brief DoorStateInput class declarations
 *  @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_DOOR_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_DOOR_STATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

class VehicleState;

#if !defined MOCK_DoorStateInput
/**
 * @brief This class is intended to monitor all vehicle's doors
 * 
 */
class DoorStateInput : public StateInput
{

public:
    /**
     * @brief Doors status flags enumeration
     */
    enum DoorsStatusFlags
    {
        nOpenPFD   = (uint8)(1u << 0u), // Passenger's door
        nOpenDFD   = (uint8)(1u << 1u), // Driver's door
        nOpenPRD = (uint8)(1u << 2u), // Passenger's rear door
        nOpenDRD  = (uint8)(1u << 3u), // Driver's rear door
        nOpenRTD  = (uint8)(1u << 4u), // Rear trunk door        
    };
    /**
     * @brief Construct a new DoorStateInput object
     * @param vState[in] Global VehicleState context
     */
    DoorStateInput(VehicleState* vState);
    /**
     * @brief Destroys the DoorStateInput object
     * 
     */
    virtual ~DoorStateInput();
    /**
     * @brief Gets passenger's door status
     * @return true if door is opened, otherwise returns false 
     */
    inline boolean PassengerSideFrontDoorOpen(){ return runtime->getPassengerSideFrontDoorOpen(); }
    /**
     * @brief Gets driver's door status
     * @return true if door is opened, otherwise returns false
     */
    inline boolean DriverSideFrontDoorOpen(){ return runtime->getDriverSideFrontDoorOpen(); }
    /**
     * @brief Gets status of a rear passenger's door
     * @return true if door is opened, otherwise returns false
     */
    inline boolean PassengerSideRearDoorOpen(){ return runtime->getPassengerSideRearDoorOpen(); }
    /**
     * @brief Gets status of a back door (trunk)
     * @return true if door is opened, otherwise returns false 
     */
    inline boolean RearSideTrunkDoorOpen(){ return runtime->getRearSideTrunkDoorOpen(); }
    /**
     * @brief Gets status of a door behind a driver
     * @return true if door is opened, otherwise returns false
     */
    inline boolean DriverSideRearDoorOpen(){ return runtime->getDriverSideRearDoorOpen(); }
    /**
     * @brief Checks is any door opened or not
     * @return true if there is opened door, otherwise return false
     */
    boolean isAnyDoorOpen() const { return (0 != inputMask); }
    /**
     * @brief Check whether FT door goes to closed state
     * @return true if FT door goes to "closed state", otherwise returns false
     */
    boolean isDriverDoorClosed() const { return (0 != (goesTo0 & nOpenDFD)); }
    /**
     * @brief Check whether Doors state goes from "all doors closed" to "any door opened"
     * @return true if Doors state goes from "all doors closed" to "any door opened", otherwise returns false
     */
    boolean isFirstDoorOpened() const { return firstDoorOpened; }

    /**
     * @brief Doors open status initialization
     */
    void init()
    {
        readDoorsStates();
        firstDoorOpened = false;
        goesTo0 = 0u;
    }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead();
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange();

#if !defined(AMBLS_UNIT_TESTS)
private:
#endif
    VehicleState* vehicleState;

    boolean firstDoorOpened; // Doors state goes from "all doors closed" to "any door opened"

    uint8 inputMask; // saves previous state of input boolean signals
    uint8 goesTo0;   // if a bit in this mask is 1, it means that corresponding signal triggers 1 -> 0

    /**
     * @brief Read door states
     * @return true if any door state changed, otherwise returns false
     */
    boolean readDoorsStates();

    /**
     * @brief Updates given bitmask by given signal mask
     * @param mask reference to bitmask
     * @param n bitmask of signal
     */ 
    static void updateMask( uint8 &mask, DoorsStatusFlags n )
    {
        mask |= n;
    }    
};
#else

#include "MockDoorStateInput.h"

#endif // !defined MOCK_DoorStateInput

#endif // !defined(VEHICLE_STATE_DOOR_STATE_INPUT_INCLUDED)
