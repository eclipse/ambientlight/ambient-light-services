/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file Transition.h
 *  @brief Implementation of the Class Transition
 *  @date  2019-11-26
 */
#ifndef VEHICLE_STATE_TRANSITION_INCLUDED_
#define VEHICLE_STATE_TRANSITION_INCLUDED_

class AbstractState;

#if !defined(MOCK_Transition)
/**
 * @brief This class allows to describe transitions between different FSM states.
 * 
 */
class Transition
{
public:
    /**
     * @brief Constructs a new Transition object
     * @param target[in] State where to transit
     */
    explicit Transition(AbstractState* target);
    /**
     * @brief Destroys the Transition object
     * 
     */
    virtual ~Transition();
    /**
     * @brief Action following this transition
     * 
     */
    virtual void triggerAction();
    /**
     * @brief Destination state
     * 
     */
    AbstractState* targetState;
private:
    Transition() {}
};

#else

#include "MockTransition.h"

#endif // !defined(MOCK_Transition)

#endif /* VEHICLE_STATE_TRANSITION_INCLUDED_ */
