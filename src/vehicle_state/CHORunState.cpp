/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

#include "CHORunState.h"
#include "VehicleState.h"

CHORunState::CHORunState( VehicleState* vState ):
    AbstractState( vState, StateIds::choRun ),
#if !defined(AMBLS_UNIT_TESTS)    
    toIdleState( &vehicleState->stateMachine.idleState ),
    toLhoState( &vehicleState->stateMachine.lhoState ),
    toChoState( &vehicleState->stateMachine.choState ),
#else    
    // It simplifies mocking up StateMachine class
    toIdleState(0),
    toLhoState(0),
    toChoState(0),
#endif    
    currentTimer( exitTimer ),
    lastEvent(None),
    warningLight(false)
{

}


CHORunState::~CHORunState()
{

}


void CHORunState::entryAction()
{
    lastEvent = None;
    warningLight = vehicleState->blinkOverlay.isWarningLight();

    if( vehicleState->doorInputs.isAnyDoorOpen() )
    {
        if( vehicleState->choTimer.isStarted() )
        {
            vehicleState->choTimer.pause( true );
        }
        currentTimer = exitTimer;
    }
    else
    {
        if( vehicleState->choTimer.isStarted() )
        {
            vehicleState->choTimer.pause( false );
        }
        currentTimer = wayTimer;
    }
}


void CHORunState::handleEvent(InputType::InputType iEvent)
{
    InputType::InputType event = vehicleState->blinkOverlay.filterEvent( iEvent );

    if ( LhoStateInput::None != vehicleState->lhoInputs.InputEvent( event, stateId ) )
    {
        // Idle state
        vehicleState->choTimer.stop();
        /** @todo check if LHO is ready */
        vehicleState->stateMachine.transit(&toLhoState);
    }
    else if ((InputType::Terminal == event) && (true == vehicleState->terminalInputs.Terminal15()))
    {
        vehicleState->choTimer.stop();
        vehicleState->stateMachine.transit(&toIdleState);
    }
    else if( InputType::Door == event )
    {
        if( vehicleState->doorInputs.isFirstDoorOpened() )
        {
            // the exit timer is our current timer
            currentTimer = exitTimer;
            // pause way timer
            vehicleState->choTimer.pause( true );

            // extend coming home light and exit timer
            if( vehicleState->choExitTimer.isStarted() && vehicleState->choTimer.isStarted() )
            {
                vehicleState->dispatch( Event::ExtendChoLight );
                vehicleState->choExitTimer.start( vehicleState->ChoExitTime() );
            }
        }
        if( !vehicleState->doorInputs.isAnyDoorOpen() )
        {
            // the way timer is our current timer
            currentTimer = wayTimer;
            // continue way timer - exit timer still counts down
            vehicleState->choTimer.pause( false );
            if( vehicleState->choExitTimer.isStarted() )
            {
                vehicleState->choTimer.start( vehicleState->choLhoInputs.CHActiveTime() );
            }
        }
        // check for event #B6 == all doors closed and no driver inside
        if( (DriverPresenceStateInput::StatusAbsent == vehicleState->driverPresencInputs.DriverPresenceStatus()) &&
            vehicleState->doorInputs.isDriverDoorClosed() )
        {
            if ( vehicleState->isEventEnabled() )
            {
                vehicleState->dispatch( Event::DoorClosedWithoutDriver );
                lastEvent = None;
            }
            else
            {
                lastEvent = EventDoorClosedWithoutDriver;
            }
        }
    }
    else if( InputType::ChoTimerExpired == event )
    {
        // current timer has been expired
        if ( wayTimer == currentTimer )
        {
            vehicleState->dispatch( Event::ChoTimerExpired );
            vehicleState->surroundAmbLSPrio = 0u;
        }
    }
    else if( InputType::ChoExitTimerExpired == event )
    {
        // current timer has been expired
        if ( exitTimer == currentTimer )
        {
            vehicleState->dispatch( Event::ChoTimerExpired );
            vehicleState->surroundAmbLSPrio = 0u;
        }
    }
    else if( InputType::Vehicle == event )
    {
        if( vehicleState->vehicleInputs.CustomerRequestFfbClose() )
        {
            if ( vehicleState->isEventEnabled() )
            {
                vehicleState->dispatch( Event::ChoLockRadio );
                lastEvent = None;
            }
            else
            {
                lastEvent = EventChoLockRadio;
            }
        }
    }
    else if( InputType::CentralLocking == event )
    {
        // a keyless entry system event would be handled here
    }
    else if ( InputType::EventRelease == event )
    {
        Event::Event ev;

        switch( lastEvent )
        {
        case EventChoLockRadio:
            ev = Event::ChoLockRadio;
            break;
        case EventDoorClosedWithoutDriver:
            ev = Event::DoorClosedWithoutDriver;
            break;
        case None:
            // FALTHRU
        default:
            ev = Event::Init;
            break;
        }

        if ( Event::Init != ev )
        {
            vehicleState->dispatch( ev );
            lastEvent = None;
        }
    }
    else
    {
        // MISRA
    }
}

void CHORunState::cycle()
{
    // if warning lights are activated just now
    if( !warningLight && vehicleState->blinkOverlay.isWarningLight() )
    {
        vehicleState->dispatch( Event::ChoWarnBlink );
        warningLight = true;
    }
    else
    {
        if( !vehicleState->blinkOverlay.isWarningLight() )
        {
            warningLight = false;
        }
    }
}

void CHORunState::exitAction()
{
    // exit actions can be executed here
    // ...
}

