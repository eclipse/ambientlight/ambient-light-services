/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
*/

/**
 * @file BlinkingStateInput.h
 * @brief Implementation of the Class BlinkingStateInput
 * @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_BLINKING_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_BLINKING_STATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

class VehicleState;

#if !defined(MOCK_BlinkingStateInput)
#include "BlinkTimer.h"

/**
 * @brief This class allows to organize a timing delay before an actual animation
 * 
 */
class BlinkingStateInput : public StateInput
{

public:
    /**
     * @brief Constructs a new BlinkingStateInput object
     * 
     * @param vState[in] Pointer to a global Vehicle State context 
     */
    BlinkingStateInput(VehicleState *vState);
    /**
     * @brief Destroys the BlinkingStateInput object
     * 
     */
    virtual ~BlinkingStateInput();
    /**
     * @brief Gets blink function activity status for CentralLockingClose event
     * @return true if blink function is active, otherwise returns false
     */
    inline boolean CentralLockingCloseBlinkingActive() { return runtime->getCentralLockingCloseBlinkingActive(); }
    /**
     * @brief Gets blink function activity status for CentralLockingOpen event
     * @return true if blink function is active, otherwise returns false
     */
    inline boolean CentralLockingOpenBlinkingActive() { return runtime->getCentralLockingOpenBlinkingActive(); }
    /**
     * @brief Gets "Warning lights" state
     * @return true if Warning lights are active, otherwise returns false
     */
    inline boolean WarningLightsActive() { return runtime->getWarningLightsActive(); }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead();
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange();

    /**
     * @brief Checks whether or not events are allowed.
     * @return true if events are allowed, otherwise returns false
     */
    bool isEventEnabled() { return eventEnabled;}

    /**
     * @brief Timer to control blink delays.
     * 
     */
    BlinkTimer blinkTimer;
    
private:
    VehicleState *vehicleState;
    boolean eventEnabled;
};

#else

#include "MockBlinkingStateInput.h"

#endif // !defined(MOCK_BlinkingStateInput)

#endif // !defined(VEHICLE_STATE_BLINKING_STATE_INPUT_INCLUDED_)
