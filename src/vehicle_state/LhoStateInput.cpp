/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file LhoStateInput.cpp
 *  @brief Implementation of the Class LhoStateInput
 *  @date  2019-11-11
 */

#include "VehicleState.h"
#include "LhoStateInput.h"

extern VehicleState vehicleState;

LhoStateInput::LhoStateInput( VehicleState *vState ):
    vehicleState(vState),
    lastInputEvent(None)
{
}

LhoStateInput::~LhoStateInput()
{

}

// The function returns current LHO event if any
LhoStateInput::LhoInputEvent LhoStateInput::InputEvent( InputType::InputType event, StateIds::StateIds stateId )
{
    LhoInputEvent lhoEvent = None;

    switch( event )
    {
    case InputType::Vehicle:
        if ( true == vehicleState->vehicleInputs.CustomerRequestFfbClose() )
        {
            if ( StateIds::lhoRun == stateId )
            {
                /* Car is locked by FFB */
                lhoEvent = RadioLock;
            }
        }
        else if ( ( false == vehicleState->doorInputs.RearSideTrunkDoorOpen() ) &&
                  ( true == vehicleState->vehicleInputs.CustomerRequestFfbHdfSingleUnlock()) )
        {
            /* Trunk is opened */
            lhoEvent = HDOpen;
        }
        else
        {
            // Nothing to do
        }
        break;


    case InputType::CentralLocking:
        if ( true == vehicleState->centralLockingInputs.OpenHdVirtualPedalGoesTo1() )
        {
            /* Trunk is opened */
            lhoEvent = HDOpen;
        }
        else if ( true == vehicleState->centralLockingInputs.OpenRadioGoesTo1() )
        {
            /* Car is unlocked by FFB */
            lhoEvent = RadioUnlock;
        }
        else if ( ( true == vehicleState->centralLockingInputs.OpenDFDGoesTo1() )    ||
                  ( true == vehicleState->centralLockingInputs.OpenPFDGoesTo1() ) )
        {
        }
        else
        {
            // Nothing to do
        }
        break;

    default:
        break;
    }
    
    if ( None != lhoEvent )
    {
        lastInputEvent = lhoEvent;
    }
    
    return lhoEvent;
}
