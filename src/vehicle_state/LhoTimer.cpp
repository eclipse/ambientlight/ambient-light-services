/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file LhoTimer.cpp
 *  @brief Implementation of the class for LHO timer. 
 *  @date 2019-11-05
 */

#include "VehicleState.h"
#include "LhoTimer.h"

LhoTimer::LhoTimer(VehicleState* vState) :
    AbstractTimer(vState) 
{
}


LhoTimer::~LhoTimer()
{
}

void LhoTimer::start( uint32 /*timerVal*/ )
{
    value = secs2ticks( vehicleState->choLhoInputs.LHActiveTime() );
    state = Run;
}

void LhoTimer::onChange(void)
{
    vehicleState->stateMachine.handleEvent( InputType::LhoTimerExpired );
}

