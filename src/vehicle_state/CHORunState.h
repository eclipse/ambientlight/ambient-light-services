/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file CHORunState
 * @brief Implementation of the Class CHORunState
 * @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_CHO_RUN_STATE_INCLUDED_)
#define VEHICLE_STATE_CHO_RUN_STATE_INCLUDED_

#include "AbstractState.h"

#if !defined(MOCK_CHORunState)

/**
 * @brief This class is intended to handle incoming input events in
 *        "Coming Home Run" state.
 */
class CHORunState : public AbstractState
{
public:
    /**
     * @brief Construct a new CHORunState::CHORunState object
     * @param vState pointer to global VehicleState execution context
     */
    explicit CHORunState( VehicleState * vState );
    /**
     * @brief Destroy the CHORunState::CHORunState object
     *
     * Default class destructor
     *
     */
    virtual ~CHORunState();
    
    /**
     * @brief Is called every time the global state machine enters this state.
     * @param None
     * @return None
     */
    virtual void entryAction();
    /**
     * @brief Handles input events while "CHORun" state is active.
     * @param event InputType event to be handled
     * @return None
     */
    virtual void handleEvent( InputType::InputType event );
    /**
     * @fn virtual void CHORunState::handleEvent( InputType::InputType event )
     * The function handles the following InputType events after filtering them with BlinkingOverlay::filterEvent():
     * - InputType::Terminal stops timer and transfers to IdleState
     * - InputType::Door depending on the door state it decides for "way home" or "exit" timer to be current timer
     * - InputType::ChoTimerExpired dispatches Event::ChoTimerExpired
     * - InputType::ChoExitTimerExpired dispatches Event::ChoTimerExpired
     * - InputType::Vehicle
     * - InputType::CentralLocking
     * - InputType::EventRelease dispatches postponed event
     * - LhoStateInput::InputEvent stops timer and transfers to LHOState
     */

    /**
     * @brief Cycle function of state.
     *
     * Checks for change of the warning lights status and dispatches Event::ChoWarnBlink when they are switched on.
     * @param None
     * @return None
     */
    virtual void cycle();
    /**
     * @brief Intended to Handle exit actions. It does nothing for now.
     * @param None
     * @return None
     */
    virtual void exitAction();

#if !defined(AMBLS_UNIT_TESTS)  // Tests want to have access to private members
private:
#endif    
    typedef enum TimerType
    {
        exitTimer,
        wayTimer
    } TimerType;

    typedef enum EventCondition
    {
        None,
        EventChoLight, // unconditioned
        EventExtendChoLight, // extend cho time
        EventChoLockRadio, // central lock radio
        EventDoorClosedWithoutDriver  // all doors closed, no driver

    } EventCondition;

    // Allowed transition
    Transition toIdleState;
    Transition toLhoState;
    Transition toChoState;

    TimerType currentTimer;
    EventCondition lastEvent;
    bool warningLight;
};

#else

#include "MockCHORunState.h"

#endif // !defined(MOCK_CHORunState)

#endif // !defined(VEHICLE_STATE_CHO_RUN_STATE_INCLUDED_)
