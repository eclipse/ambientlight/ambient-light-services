/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file EventIds.h
 * @brief Event identifiers
 * @date 2019-11-12
 */

#if !defined(VEHICLE_STATE_EVENT_IDS_INCLUDED_)
#define VEHICLE_STATE_EVENT_IDS_INCLUDED_

/**
 * @namespace Event
 * To access members of the enumeration by that name
 */
namespace Event
{
    /**
     * @enum Event
     * addressing events to be dispatched
     */
    enum Event
    {
        SwitchOffStaging = 0u, /*!< switch off staging */
        ChoLight = 1u, /*!< coming home Light activated */
        ExtendChoLight = 2u, /*!< extend coming home timer */
        ChoLockRadio = 4u, /*!< locking the car by radio during coming home */
        DoorClosedWithoutDriver = 6u, /*!< door is closed and driver is out */
        LhoTimerExpired = 7u, /*!< leaving home timer expired */
        RadioUnlock = 8u, /*!< unlocking the car by radio */
        RearDoorOpen = 11U, /*!< back door is open */
        ChoWarnBlink = 12u, /*!< warning lights switched on during coming home */
        LhoWarnBlink = 13u, /*!< warning lights switched on during leaving home */
        ChoTimerExpired = 14u, /*!< coming home timer expired */
        SequenceRadioUnlock = 17u, /*!< unlocking the car by radio sequence */
        SequenceRearDoorOpen = 20u, /*!< back door is open sequence */
        EyesFrontLeft = 21u, /*!< eye movement front left */
        EyesFrontRight = 22u, /*!< eye movement front right */
        /** @todo others */

        Init = 31u /*!< System started */
    };
}

#endif // VEHICLE_STATE_EVENT_IDS_INCLUDED_
