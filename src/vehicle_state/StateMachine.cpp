/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

#include "StateMachine.h"
#include "VehicleState.h"

StateMachine::StateMachine( VehicleState* vehicleState ):
    choRunState( vehicleState ),
    choState( vehicleState ),
    idleState( vehicleState ),
    lhoRunState( vehicleState ),
    lhoState( vehicleState ),
    currentState( &idleState ), 
    transitTarget( &idleState.defaultTransition ) // we need defaultTransition: transitTarget is not allowed to contain a null pointer
{

}


StateMachine::~StateMachine()
{

}


// workflow: EVENT -> currentState.exitAction -> transition.triggerAction -> newState.entryAction -> currentState := newState
/**
 * calls<i> currentState.handleEvent</i> and checks if the resulting state object
 * is another than <i>currentState</i>. If it is <i>currentstate</i> will be set
 * to the new state and its <i>entryAction</i> will be called.
 */
void StateMachine::handleEvent( InputType::InputType event )
{
    currentState->handleEvent(event);  // newState can be changed inside

    if ( !(currentState == transitTarget->targetState) )
    {
        currentState->exitAction();
        // call to transitTarget triggerAction ()
        currentState->vehicleState->prevStateId = currentState->stateId;
        currentState = transitTarget->targetState;
        currentState->entryAction();
    }
}


/**
 * Stores <i>transitTarget</i>.
 * <i>transit()</i> is called from <i>currentState.handleEvent</i>
 * in order to switch to new state.
 */
void StateMachine::transit( Transition * target )
{
    transitTarget = target;
}

/**
 * calls <i>idleState.entryAction</i>. 
 * Note <i>currentState</i> pointers to <i>idleState</i> at the
 * moment of initialization.
 */
void StateMachine::init()
{
    currentState->entryAction();
}

