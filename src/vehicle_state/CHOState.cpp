/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

#include "CHOState.h"

#include "VehicleState.h"

CHOState::CHOState( VehicleState * vState ) :
    AbstractState( vState, StateIds::cho ),
#if !defined(AMBLS_UNIT_TESTS)    
    toChoRunState( &vehicleState->stateMachine.choRunState ),
    toIdleState( &vehicleState->stateMachine.idleState ),
    toLhoState( &vehicleState->stateMachine.lhoState ),
#else
    // It simplifies mocking up StateMachine class
    // These pointers are not deferenced in the tests 
    toChoRunState(0),
    toIdleState(0),
    toLhoState(0),
#endif    
    tExit(0u),
    tWay(0u),
    currentTimer( exitTimer ),
    waitForEnableB1(false),
    manualOperation(false)
{

}


CHOState::~CHOState()
{

}

void CHOState::entryAction()
{
    waitForEnableB1 = false;
    // start exit/way timer
    tExit = vehicleState->ChoExitTime();
    tWay = vehicleState->choLhoInputs.CHActiveTime();
    if( tExit > tWay )
    {
        tWay = tExit;
    }

    vehicleState->choExitTimer.start( tExit );
    vehicleState->choExitTimer.pause( false );
    vehicleState->choTimer.start( tWay );

    if( vehicleState->doorInputs.isAnyDoorOpen() )
    {
        vehicleState->choTimer.pause( true );
        currentTimer = exitTimer;
    }
    else
    {
        vehicleState->choTimer.pause( false );
        currentTimer = wayTimer;
    }

    if( vehicleState->blinkOverlay.isActive() )
    {
        vehicleState->dispatch( Event::ChoWarnBlink );
    }
    else
    {
        if ( vehicleState->isEventEnabled() )
        {
            vehicleState->dispatch( Event::ChoLight );
            vehicleState->stateMachine.transit ( &toChoRunState );
        }
        else
        {
            waitForEnableB1 = true;
        }
    }
}

void CHOState::handleEvent(InputType::InputType iEvent)
{
    InputType::InputType event = vehicleState->blinkOverlay.filterEvent( iEvent );

    if( InputType::Door == event )
    {
        // One or several doors are open
        if( vehicleState->doorInputs.isAnyDoorOpen() )
        {
            // the exit timer is our current timer
            currentTimer = exitTimer;
            // pause way timer
            vehicleState->choTimer.pause( true );
        }
        else // One or several doors  have been closed and all the others already closed
        {
            // the way timer is our current timer
            currentTimer = wayTimer;
            // continue way timer - exit timer still counts down
            vehicleState->choTimer.pause( false );
            if( vehicleState->choExitTimer.isStarted() )
            {
                vehicleState->choTimer.start( vehicleState->choLhoInputs.CHActiveTime() );
            }
        }
    }
    else if( InputType::ChoTimerExpired == event )
    {
        // current timer has been expired
        if ( wayTimer == currentTimer )
        {
            vehicleState->dispatch( Event::ChoTimerExpired );
            vehicleState->surroundAmbLSPrio = 0u;
        }
    }
    else if( InputType::ChoExitTimerExpired == event )
    {
        // current timer has been expired
        if ( exitTimer == currentTimer )
        {
            vehicleState->dispatch( Event::ChoTimerExpired );
            vehicleState->surroundAmbLSPrio = 0u;
        }
    }
    else if ((InputType::Terminal == event) && (true == vehicleState->terminalInputs.Terminal15()))
    {
        // Reject: Terminal15
        vehicleState->choTimer.stop();
        vehicleState->stateMachine.transit ( &toIdleState );
    }
    else if ( InputType::EventRelease == event )
    {
        if ( waitForEnableB1 )
        {
            if ( vehicleState->choTimer.isStarted() )
            {
                waitForEnableB1 = false;
                vehicleState->dispatch( Event::ChoLight );
                vehicleState->stateMachine.transit ( &toChoRunState );
            }
        }
    }
    else if ( LhoStateInput::None != vehicleState->lhoInputs.InputEvent( event, stateId ) )
    {
        vehicleState->choTimer.stop();

        /** @todo check if LHO is ready */
        vehicleState->stateMachine.transit ( &toLhoState );
    }
    else
    {
        // for misra to know that we take care
    }
}

void CHOState::cycle()
{
    if( !vehicleState->blinkOverlay.isWarningLight() )
    {
        if ( waitForEnableB1 )
        {
            if ( vehicleState->choTimer.isStarted() && vehicleState->isEventEnabled() )
            {
                waitForEnableB1 = false;
                vehicleState->dispatch( Event::ChoLight );
                vehicleState->stateMachine.transit ( &toChoRunState );
            }
        }
    }
}

void CHOState::exitAction()
{
}

