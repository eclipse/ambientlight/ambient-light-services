/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file LightSensorStateInput.h
 *  @brief Implementation of the Class LightSensorStateInput
 *  @date  2019-10-25
 */


#if !defined(VEHICLE_STATE_LIGHT_SENSOR_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_LIGHT_SENSOR_STATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

#if !defined(MOCK_LightSensorStateInput)

/**
 * @brief This class is intended to monitor external illumination 
 * 
 */
class LightSensorStateInput : public StateInput
{

public:
    /**
     * @brief Constructs a new LightSensorStateInput object
     * 
     */
    LightSensorStateInput(){}
    /**
     * @brief Destroys the LightSensorStateInput object
     * 
     */
    virtual ~LightSensorStateInput(){}
    /**
     * @brief Gets information from the light sensor
     * @return true in case of light environment outside the car, otherwise returns false
     */
    inline boolean LightSensorOnChoLho() { return runtime->getLightSensorOnChoLho(); }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead(){}
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange(){}
};

#else

#include "MockLightSensorStateInput.h"

#endif // defined(MOCK_LightSensorStateInput)

#endif // !defined(VEHICLE_STATE_LIGHT_SENSOR_STATE_INPUT_INCLUDED_)
