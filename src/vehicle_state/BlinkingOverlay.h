/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file BlinkingOverlay.h
 * @brief Implementation of the Class BlinkingOverlay
 * @date 2019-11-28
 */

#ifndef VEHICLE_STATE_BLINKING_OVERLAY_INCLUDED_
#define VEHICLE_STATE_BLINKING_OVERLAY_INCLUDED_

#include "InputType.h"

#if !defined(MOCK_BlinkingOverlay)

/**
 * @class BlinkingOverlay
 * @brief This class watches the warning lights activities and implements an according event filter.
 */
class BlinkingOverlay
{
public:
    /**
     * @brief Constructs a new BlinkingOverlay object
     * 
     */
    BlinkingOverlay();
    /**
     * @brief Destroys the BlinkingOverlay object
     * 
     */
    virtual ~BlinkingOverlay();

    /**
     * @brief Checks whether Warning lights are on or off.
     * @param None
     * @return warning lights status
     */
    bool isWarningLight() const;
    /**
     * @brief Warning lights status.
     * @param None
     * @return warning lights activity
     */
    bool isActive() const { return active; }
    /**
     * @brief Checks cyclic if the warning lights are switched on.
     * @param None
     * @return None
     */
    void cyclicRead();
    /**
     * @brief Filters out all none essential events if the warning lights are switched on.
     * @param event InputType
     * @return filtered event InputType
     */
    InputType::InputType filterEvent( InputType::InputType event );
    /**
     * @fn InputType::InputType BlinkingOverlay::filterEvent( InputType::InputType event )
     * The essential events which will pass the filter in any condition are:
     * - InputType::ChoTimerExpired
     * - InputType::LhoTimerExpired
     * - InputType::Door open or close of doors may change the timer
     * - InputType::Terminal can't be lost, needs always to be recognized
     */

#if !defined(AMBLS_UNIT_TESTS)  // Tests want to have access to private members
private:
#endif
    bool active;
};

#else

#include "MockBlinkingOverlay.h"

#endif // !defined(MOCK_BlinkingOverlay)

#endif /* VEHICLE_STATE_BLINKING_OVERLAY_INCLUDED_ */
