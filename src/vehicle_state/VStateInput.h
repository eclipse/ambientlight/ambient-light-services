/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file VStateInput.h
 *  @brief Implementation of the Class VStateInput
 *  @date  2019-10-25
 */
#if !defined(VEHICLE_STATE_VSTATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_VSTATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

class VehicleState;

#if !defined(MOCK_VStateInput)
/**
 * @brief This class allows to monitor Vehicle State signals
 * 
 */
class VStateInput : public StateInput
{

public:
    /**
     * @brief Constructs a new VStateInput object
     * @param vState[in] Global VehicleState context
     */
    VStateInput(VehicleState* vState);
    /**
     * @brief Destroys the VStateInput object
     * 
     */
    virtual ~VStateInput();
    /**
     * @brief Gets status of FFB close signal
     * @return  true if customer requests for car locking, otherwise returns false
     */
    inline boolean CustomerRequestFfbClose(){ return runtime->getCustomerRequestFfbClose(); }
    /**
     * @brief Gets status of FFB single unlock request for back door (trunk)
     * @return true if customer requests for back door unlocking, otherwise returns false
     */
    inline boolean CustomerRequestFfbHdfSingleUnlock(){ return runtime->getCustomerRequestFfbHdfSingleUnlock(); }
    /**
     * @brief Gets AFS positions
     * @return One of the following possible values:
     * - 0 (default/initial)
     * - 1 (not available)
     * - 2 (reference run is active)
     * - 3 (reference run is finished)
     * - 4 (error)
     */
    inline uint8 AFSAmbLSRefStatus(){ return runtime->getAFSAmbLSRefStatus(); }
    /**
     * @brief Gets internal lights suppression
     * @return true if suppressed, otherwise returns false
     */
    inline boolean MFInteriorLightRejectionCan(){ return runtime->getMFInteriorLightRejectionCan(); }
    /**
     * @brief Gets the cyclecounter
     * @return Actuality counter
     */
    inline uint8 PerAmbLSCntr(){ return runtime->getPerAmbLSCntr(); }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead();
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange();

    
    #if defined(AMBLS_UNIT_TESTS)
    boolean getcustomerRequestFfbHdfSingleUnlock() { return customerRequestFfbHdfSingleUnlock; }
    boolean getcustomerRequestFfbClose() { return customerRequestFfbClose; }
    #endif
private:
    VehicleState* vehicleState;
    boolean customerRequestFfbHdfSingleUnlock;
    boolean customerRequestFfbClose;
};

#else

#include "MockVStateInput.h"

#endif // !defined(MOCK_VStateInput)

#endif // !defined(VEHICLE_STATE_VSTATE_INPUT_INCLUDED_)
