/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file TerminalStateInput.cpp
 * @brief TerminalStateInput class definitions
 * @date 2019-10-25
 * 
 */

#include "TerminalStateInput.h"


#include "InputType.h"
#include "VehicleState.h"
#include "StateMachine.h"

extern VehicleState vehicleState;

TerminalStateInput::TerminalStateInput(VehicleState *vState):
    vehicleState(vState),
    terminal15(false)
{

}



TerminalStateInput::~TerminalStateInput()
{

}



void TerminalStateInput::cyclicRead()
{
    if(this->Terminal15() != this->terminal15)
    {
        this->terminal15 = this->Terminal15();
        this->onChange();
    }

}


void TerminalStateInput::onChange()
{
    vehicleState->stateMachine.handleEvent( InputType::Terminal );
}
