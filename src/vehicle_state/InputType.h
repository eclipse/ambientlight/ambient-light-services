/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file InputType.h
 * @brief Implementation of the Enumeration InputType
 * @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_INPUT_TYPE_INCLUDED_)
#define VEHICLE_STATE_INPUT_TYPE_INCLUDED_

/**
 * @namespace InputType
 * To access members of the enumeration by that name
 */
namespace InputType
{
    /**
     * @enum InputType
     * addressing input change events
     */
    enum InputType
    {
        None,           /*!< no changes */
        Vehicle,        /*!< change in VStateInput */
        ChoLho,         /*!< change in ChoLhoStateInput */
        DriverPresence, /*!< change in DriverPresenceStateInput */
        Blinking,       /*!< change in BlinkingStateInput */
        Door,           /*!< change in DoorStateInput */
        CentralLocking, /*!< change in CentralLockingStateInput */
        Terminal,       /*!< change in TerminalStateInput */
        LightSensor,    /*!< change in LightSensorStateInput */
        LightSwitch,    /*!< change in LightSwitchStateInput */
        ChoTimerExpired,/*!< ChoTimer expired */
        ChoExitTimerExpired,/*!< ChoExitTimer expired */
        LhoTimerExpired,/*!< LhoTimer expired */
        EventRelease,   /*!< change in BlinkingStateInput from event disabled to event enabled*/
        LhoState        /*!< change in LhoStateInput */
    };
}

#endif // !defined(VEHICLE_STATE_INPUT_TYPE_INCLUDED_)
