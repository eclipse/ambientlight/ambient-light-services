/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file ChoLhoStateInput.h
 *  @brief Implementation of the Class ChoLhoStateInput 
 *  @date 2019-10-25
 */

#if !defined(CHOLHOSTATEINPUT_INCLUDED_)
#define CHOLHOSTATEINPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

#if !defined(MOCK_ChoLhoStateInput)
/**
 * @brief This class is intended to monitor "Coming Home/Leaving home" settings
 * 
 */
class ChoLhoStateInput : public StateInput
{

public:
    /**
     * @brief Constructs a new ChoLhoStateInput object
     * 
     */
    ChoLhoStateInput();
    /**
     * @brief Destroys the ChoLhoStateInput object
     * 
     */
    virtual ~ChoLhoStateInput();
    /**
     * @brief Gets "Coming home" active time
     * @return "Coming home" duration in seconds
     */
    inline uint8 CHActiveTime(){ return runtime->getCHActiveTime(); }
    /**
     * @brief Gets "Choming home" state from a bitmask
     * 
     * Works with the bits of the 8-bit bitfield (it's uint8) like from an array-access-function
     * @param id[in] Bit number
     * @return false if bit is unset, otherwise returns true 
     */
    inline boolean CHState(uint8 id) { return (inputArrayCount > id)?(0 != (runtime->getCHState() & inputArrayBits[id])):false; }
    /**
     * @brief Gets "Leaving home" active time
     * @return "Leaving home" duration in seconds
     */
    inline uint8 LHActiveTime(){ return runtime->getLHActiveTime(); }
    /**
     * @brief Gets "Leaving home" state from a bitmask
     * 
     * Works with the bits of the 8-bit bitfield like from an array-access-function
     * @param id[in] Bit number 
     * @return false if bit is unset, otherwise returns true
     */
    inline boolean LHState(uint8 id) { return (inputArrayCount > id)?(0 != (runtime->getLHState() & inputArrayBits[id])):false; }
    /**
     * @brief Gets "Manual Activation" setup
     * @return true if "Manual Activation" is enabled, otherwise returns false
     */
    inline boolean ManualActivationCH(){ return runtime->getManualActivationCH(); }
    /**
     * @brief Gets "Signature Light" settings from a bitmask
     * @param id[in] Bit number
     * @return 1 if "Signature Light" is set, otherwise returns 0 
     */
    inline uint8 SignatureLightSetup(uint8 id)
    {
        return (inputArrayCount > id) ?
            ( (0 != ( runtime->getSignatureLightSetup() & inputArrayBits[id] )) ? 1 : 0 ) : runtime->getSignatureLightSetup();
    }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead();
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange();
    /**
     * @brief Equals to 0b11111111
     * 
     */
    static const uint8 bitFieldAll = 255;
private:
    static const uint8 inputArrayCount = 8;
    static const uint8 inputArrayBits[8];
};

#else

#include "MockChoLhoStateInput.h"

#endif

#endif // !defined(CHOLHOSTATEINPUT_INCLUDED_)
