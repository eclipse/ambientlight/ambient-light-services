/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file VStateInput.cpp
 * @brief VStateInput class definitions
 * @date 2019-11-26
 * 
 */

#include "VStateInput.h"


#include "VStateInput.h"
#include "VehicleState.h"
#include "StateMachine.h"

VStateInput::VStateInput(VehicleState *vState) :
    vehicleState(vState),
    customerRequestFfbHdfSingleUnlock(false),
    customerRequestFfbClose(false)
{

}

VStateInput::~VStateInput()
{

}

void VStateInput::cyclicRead()
{
    if ( CustomerRequestFfbHdfSingleUnlock() != customerRequestFfbHdfSingleUnlock )
    {
        customerRequestFfbHdfSingleUnlock = CustomerRequestFfbHdfSingleUnlock();
        onChange();
    }
    else if ( CustomerRequestFfbClose() != customerRequestFfbClose )
    {
        customerRequestFfbClose = CustomerRequestFfbClose();
        onChange();
    }
}

void VStateInput::onChange()
{
    vehicleState->blinkingInputs.blinkTimer.handleEvent( InputType::Vehicle );
    vehicleState->stateMachine.handleEvent( InputType::Vehicle );
}
