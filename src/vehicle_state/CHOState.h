/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file CHOState.h
 * @brief Implementation of the Class CHOState
 * @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_CHO_STATE_INCLUDED_)
#define VEHICLE_STATE_CHO_STATE_INCLUDED_

#include "AbstractState.h"

#if !defined(MOCK_CHOState)

/**
 * @class CHOState
 * @brief This class is intended to handle incoming input events in
 *        "Coming Home" state.
 *
 * The central state machine will invoke this state class in case of Terminal-15 goes from 1 -> 0 and the driver leaves the car.
 * It manages two timers: A "way home" timer and an "exit" timer.
 */
class CHOState : public AbstractState
{
public:
    /**
     * @brief Construct a new CHOState::CHOState object
     * @param vState pointer to global VehicleState execution context
     */
    explicit CHOState( VehicleState * vState );
    /**
     * @brief Destroy the CHOState::CHOState object
     * Default class destructor
     */
    virtual ~CHOState();

    /**
     * @brief Is called every time the global state machine enters this state.
     * @param None
     * @return None
     */
    virtual void entryAction();
    /**
     * @fn virtual void CHOState::entryAction()
     * Setting up the two timers and deciding which one will be
     * in charge depending on the door state.
     *
     * If warning lights are on, the Event::ChoWarnBlink will be dispatched.
     *
     * Otherwise it checks for global event enabling and transfers to
     * CHORunState with dispatching of Event::ChoLight or the attempt
     * will be stored if events are not enabled.
     */

    /**
     * @brief Handles input events while "CHO" state is active.
     * @param event InputType event to be handled
     * @return None
     */
    virtual void handleEvent( InputType::InputType event );
    /**
     * @fn virtual void CHOState::handleEvent( InputType::InputType event )
     * The function handles the following InputType events after filtering them with BlinkingOverlay::filterEvent():
     * - InputType::Door depending on the door state it decides for "way home" or "exit" timer to be current timer
     * - InputType::ChoTimerExpired dispatches Event::ChoTimerExpired
     * - InputType::ChoExitTimerExpired dispatches Event::ChoTimerExpired
     * - InputType::Terminal stops timer and transfers to IdleState
     * - InputType::EventRelease dispatches Event::ChoLight and transfers to CHORunState if the B1 waiting condition was set before
     * - LhoStateInput::InputEvent stops timer and transfers to LHOState
     */

    /**
     * @brief Checks cyclic for the condition to dispatch "B1" event.
     *
     * It checks for global event enabling and transfers to CHORunState with dispatching of Event::ChoLight
     * @param None
     * @return None
     */
    virtual void cycle();

    /**
     * @brief Intended to Handle exit actions. It does nothing for now.
     * @param None
     * @return None
     */
    virtual void exitAction();

#if !defined(AMBLS_UNIT_TESTS)  // Tests must have access to private members
private:
#endif    
    typedef enum TimerType
    {
        exitTimer,
        wayTimer
    } TimerType;

    // Allowed transitions
    Transition toChoRunState;
    Transition toIdleState;
    Transition toLhoState;
    
    uint32 tExit;
    uint32 tWay;

    TimerType currentTimer;
    bool waitForEnableB1;

    boolean manualOperation;
};

#else

#include "MockCHOState.h"

#endif // !defined(MOCK_CHOState)

#endif // !defined(VEHICLE_STATE_CHO_STATE_INCLUDED_)
