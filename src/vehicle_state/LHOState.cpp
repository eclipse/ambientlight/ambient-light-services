/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file LHOState.cpp
 * @brief Implementation of the Class LHOState
 * @date 2019-10-25
 */

#include "LHOState.h"
#include "VehicleState.h"


LHOState::LHOState( VehicleState * vState ) :
    AbstractState( vState, StateIds::lho ),
#if !defined(AMBLS_UNIT_TESTS)    
    // It simplifies mocking up StateMachine class
    toIdleState( &vehicleState->stateMachine.idleState ),
    toLhoRunState( &vehicleState->stateMachine.lhoRunState )
#else
    toIdleState(0),
    toLhoRunState(0)
#endif    
{

}


LHOState::~LHOState()
{
}


void LHOState::entryAction()
{
}

void LHOState::handleEvent( InputType::InputType iEvent )
{
    InputType::InputType event = vehicleState->blinkOverlay.filterEvent( iEvent );

    if ( ( InputType::Terminal == event )  &&
         ( true == vehicleState->terminalInputs.Terminal15() ) )
    {
        // Reset last LHO input event
        vehicleState->lhoInputs.ResetLastInputEvent();

        // Reset LHO dynamic protection
        vehicleState->nLhoPlay = vehicleState->LhoDynamicProtection();

        // -> idle
        vehicleState->stateMachine.transit( &toIdleState );
    }
    
    else if ( InputType::EventRelease == event )
    {
        // output (dispatch) event which is corresponding to LHO input event 
        outputEvent( vehicleState->lhoInputs.LastInputEvent() );
    }
    
    else
    {
        // keep last event
        vehicleState->lhoInputs.InputEvent( event, stateId );
    }
}

void LHOState::outputEvent( LhoStateInput::LhoInputEvent inputEvent )
{
    Event::Event ev;

    // check latest LHO input event
    switch ( inputEvent )
    {
    case LhoStateInput::HDOpen:
        ev = Event::RearDoorOpen;
        break;
    case LhoStateInput::RadioUnlock:
        ev = Event::RadioUnlock;
        break;
    default:
        ev = Event::Init;
        break;
    }

    if ( Event::Init != ev )
    {
        vehicleState->lhoTimer.start();
        /* nLhoPlay > 0 always here otherwise LHO state could not be entered */
        vehicleState->nLhoPlay--;
        vehicleState->dispatch( ev );
        
        // st_AmbLS_iLHO == 0 -> 1 
        vehicleState->surroundAmbLSPrio = 1;  
        vehicleState->stateMachine.transit( &toLhoRunState );
    }

    // Reset last LHO input event 
    vehicleState->lhoInputs.ResetLastInputEvent();
}

void LHOState::cycle()
{

}

void LHOState::exitAction()
{
    // exit actions can be executed here
    // ...
}
