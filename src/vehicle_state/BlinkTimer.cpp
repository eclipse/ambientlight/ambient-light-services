/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file BlinkTimer.cpp
 *  @brief Implementation of the class of blink timer.
 *  @date 2019-11-05
 */

#include "VehicleState.h"
#include "BlinkTimer.h"

BlinkTimer::BlinkTimer(VehicleState* vState) :
    AbstractTimer(vState),
    expiredEvent(false)
{
}

BlinkTimer::~BlinkTimer()
{
}

void BlinkTimer::start(uint32 /* timerValue */)
{
    // timer is started with  the value  p_Delay
    value = secs2ticks( vehicleState->SwcSignalDelayTime() );
    state = Run;
}

void BlinkTimer::onChange(void)
{
    expiredEvent = true;
    // blinking state input class does all remaining
}


void BlinkTimer::handleEvent( InputType::InputType event )
{
    if ( !isStarted() )
    {
        switch ( event )
        {
        case InputType::Vehicle:
            if ( ( true == vehicleState->vehicleInputs.CustomerRequestFfbClose() ) ||
                 ( ( false == vehicleState->doorInputs.RearSideTrunkDoorOpen() ) &&
                   ( true == vehicleState->vehicleInputs.CustomerRequestFfbHdfSingleUnlock() ) ) )
            {
                start();
            }
            break;

        case InputType::CentralLocking:
            // All signals in CentralLocking are related to BlinkTimer.
            // If at least one of them goes to 1, the timer will be start.
            if ( true == vehicleState->centralLockingInputs.anySignalGoesTo1() )
            {
                start();
            }
            break;

        default:
            // nothing to do
            break;
        }
    }
}

