/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file AbstractState.h 
 *  @brief This file contains definitions for AbstractState class
 *  @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_ABSTRACTSTATE_INCLUDED_)
#define VEHICLE_STATE_ABSTRACTSTATE_INCLUDED_

#include "PlatformTypes.h"
#include "InputType.h"
#include "StateIds.h"
#include "Transition.h"

class VehicleState;

/**
 * @brief Base interface for vehicle's states
 * 
 * All concrete states should be derived from this interface
 * The following methods should be redefined in each derived state:
 * - entryAction;
 * - handleEvent;
 * - cycle;
 * - exitAction. 
 * 
 */
class AbstractState
{

public:
    /**
     * @brief Constructs a new AbstractState object
     * 
     */
    AbstractState():
        stateId(StateIds::none),
        vehicleState()
    {
    }
    /**
     * @brief Constructs a new AbstractState object
     * 
     * @param vState[in] Global VehicleState context
     * @param stId [in] Unique state identifier
     */
    explicit AbstractState( VehicleState* vState,
                            StateIds::StateIds stId = StateIds::none):
        stateId(stId),
        vehicleState(vState)
    {
    }
    /**
     * @brief Destroys the AbstractState object
     * 
     */
    virtual ~AbstractState() 
    {
    }
    /**
     * @brief State numeric identifier
     * 
     */
    StateIds::StateIds stateId;
    /**
     * @brief VehicleState context
     * 
     */
    VehicleState* vehicleState;
    /**
     * @brief Actions should be done right after state entering
     * 
     */
    virtual void entryAction() = 0;
    /**
     * @brief Does main state actions
     * @param event[in] Happened event ID.
     */
    virtual void handleEvent( InputType::InputType event ) = 0;
    /**
     * @brief Periodic state actions, independent from incoming events
     * 
     */
    virtual void cycle() = 0;
    /**
     * @brief Does main state actions before exit
     * 
     */
    virtual void exitAction() = 0;
    
};
#endif // !defined(VEHICLE_STATE_ABSTRACTSTATE_INCLUDED_)
