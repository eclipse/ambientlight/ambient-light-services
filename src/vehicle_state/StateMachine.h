/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file StateMachine.h
 *  @brief Implementation of the Class StateMachine
 *  @date  2019-10-25
 */
#if !defined(VEHICLE_STATE_STATE_MACHINE_INCLUDED_)
#define VEHICLE_STATE_STATE_MACHINE_INCLUDED_

#include "CHORunState.h"
#include "CHOState.h"
#include "IdleState.h"
#include "LHORunState.h"
#include "LHOState.h"
#include "InputType.h"

#if !defined(MOCK_StateMachine)
/**
 * @brief This class represents state machine
 * 
 */
class StateMachine
{

public:
    /**
     * @brief Constructs a new StateMachine object
     * @param vehicleState[in] Global VehicleState context
     */
    explicit StateMachine( VehicleState* vehicleState );
    /**
     * @brief Destroys the StateMachine object
     * 
     */
    virtual ~StateMachine();
    /**
     * @brief Initializes state machine before use
     * 
     */
    void init();
    /**
     * @brief Invokes logic, corresponding to a current active state
     * @param event[in] Incoming event 
     */
    void handleEvent( InputType::InputType event );
    /**
     * @brief Swithces current state
     * 
     * @param target[in] Transition description
     */
    void transit( Transition * target );
    /**
     * @brief Invokes state repeated actions
     * 
     */
    void currentStateCycle()
    {
        currentState->cycle();
    }

    /**
     * @brief "Choming home run" state
     * 
     */
    CHORunState choRunState;
    /**
     * @brief "Choming home" state
     * 
     */
    CHOState choState;
    /**
     * @brief "Idle" state
     * 
     */
    IdleState idleState;
    /**
     * @brief "Leaving home run" state
     * 
     */
    LHORunState lhoRunState;
    /**
     * @brief "Leaving home" state
     * 
     */
    LHOState lhoState;

private: 
    AbstractState *currentState;
    Transition *transitTarget;
};

#else

#include "MockStateMachine.h"

#endif // !defined(MOCK_StateMachine)

#endif // !defined(VEHICLE_STATE_STATE_MACHINE_INCLUDED_)
