/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file DoorStateInput.cpp
 *  @brief DoorStateInput class definitions
 *  @date 2019-10-25
 */

#include "VehicleState.h"
#include "DoorStateInput.h"
#include "InputType.h"
#include "StateMachine.h"

DoorStateInput::DoorStateInput(VehicleState *vState):
    vehicleState(vState),
    firstDoorOpened(false),
    inputMask(0u),
    goesTo0(0u)
{

}



DoorStateInput::~DoorStateInput()
{

}



void DoorStateInput::cyclicRead()
{
    if( readDoorsStates() )
    {
        onChange();
    }
}


void DoorStateInput::onChange()
{
    vehicleState->stateMachine.handleEvent( InputType::Door );
}

boolean DoorStateInput::readDoorsStates()
{
    uint8 newMask = 0u;
    boolean statesChanged = false;

    firstDoorOpened = false;

    if ( PassengerSideFrontDoorOpen() )
    {
        updateMask( newMask, nOpenPFD );
    }
    
    if ( DriverSideFrontDoorOpen() )
    {
        updateMask( newMask, nOpenDFD );
    }
    
    if ( PassengerSideRearDoorOpen() )
    {
        updateMask( newMask, nOpenPRD );
    }
    
    if ( DriverSideRearDoorOpen() )
    {
        updateMask( newMask, nOpenDRD );
    }
    
    if ( RearSideTrunkDoorOpen() )
    {
        updateMask( newMask, nOpenRTD );
    }

    statesChanged = newMask != inputMask;

    if ( statesChanged )
    {
        uint8 edgeMask = inputMask ^ newMask;
        goesTo0 = edgeMask & (~newMask);

        firstDoorOpened = (0u == inputMask) && (0u != newMask);

        inputMask = newMask;
    }

    return statesChanged;
}
