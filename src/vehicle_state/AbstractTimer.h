/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file AbstractTimer.h
 *
 *  @brief Implementation of the class SW timer. 
 *  This timer is intended to be called cyclically with a constant 
 *  time period, for example, every 10 ms
 *
 *  @date 2019-11-05
 */

#if !defined(VEHICLE_STATE_ABSTRACTTIMER_INCLUDED_)
#define VEHICLE_STATE_ABSTRACTTIMER_INCLUDED_

/**
 *  @brief Class <i>AbstractTimer</i> provides following interface to work with timer:
 *  - start()
 *  - run()
 *  - stop()
 *  - pause(bool)
 *  - isStarted() -  run or paused
 *  - isRun()
 *  - isPaused()
 *  - isExpired()
 *  - isStopped()
 */

#if !defined(MOCK_Timers)

class AbstractTimer
{
public:
    
    /**
     *  @brief Constructs new object of <i>AbstractTimer</i>
     *  @param vState Global execution context
     */
    AbstractTimer(VehicleState *vState) :
        vehicleState(vState), value(0), state(Stopped)
    {}

    /**
     *  @brief Destructor of <i>AbstractTimer</i>
     */
    virtual ~AbstractTimer()
    {}

    /**
     * @brief Checks if the timer started
     * @return true if the timer is run or paused, otherwise - false
     */
    bool isStarted()
    {
        return ( state > Expired );
    }

    /**
     *  @brief Checks if the timer expired
     *  @return true if the timer is expired, otherwise - false
     */
    bool isExpired()
    {
        return ( Expired == state );
    }

    /**
     *  @brief Checks if the timer run
     *  @return true if the timer is run, otherwise - false
     */
    bool isRun()
    {
        return ( Run  == state );
    }

    /**
     *  @brief Checks if the timer stopped
     *  @return true if the timer is stopped, otherwise - false
     */
    bool isStopped()
    {
        return ( Stopped  == state );
    }

    /**
     *  @brief Checks if the timer paused
     *  @return true if the timer is paused, otherwise - false
     */
    bool isPaused()
    {
        return ( Paused  == state );
    }

    /**
     *  @brief Starts timer. It must be called from state machine logic.
     *  @param: timerVal in secs. It must also set state to Started
     */
    virtual void start( uint32 timerVal ) = 0;

    /**
     *  @brief Stops the timer
     *  @Warning  It must be able to stop timer regardless of the timer state
     */
    void stop()
    {
        value = 0u;
        state = Stopped;
    }
    
    /**
     *  @brief Runs timer. It must be called on every runnable call.
     *         It decrements timer value on every call and calls onChange() if the value == 0.
     *  @warning If run() is called on next cycle after start(), start(0) and start(1) have 
     *           the same behavour. 
     */
    void run()
    {
        if ( Run == state )
        {
            if (  value > 0 )
            {
                value --;
            }
            if ( value == 0 )
            {
                state = Expired;
                onChange();
            }
        }
    }

    /**
     *  @brief Pauses/unpauses timer.
     *  @Warning The function does nothing if the timer is not started.
     *  @param p If p is true, the timer goes to Puased state, otherwise it goes to Run state. 
     */
    void pause(bool p)
    {
        if ( isStarted() ) // only if timer is started
        {
            if ( true == p )
            {
                state = Paused;
            }
            else
            {
                state = Run;
            }
        }
    }

    /**
     *  @brief Converts seconds to ticks
     *  @param secs the value in seconds   
     *  @return the value in ticks
     */
    static uint32 secs2ticks( uint32 secs )
    {
        return secs * (1000u / resolution);
    }

protected:

    /**
     *  @brief Enumeration with timer states
     */
    enum TimerState
    {
        Stopped,
        Expired,
        Paused,
        Run
    };

    /**
     *  @brief It is called if the timer expires
     */
    virtual void onChange() = 0;

    /**
     *  @brief Pointer to VehicleState
     */
    VehicleState *vehicleState;

    /**
     * @brief Timer value
     */
    uint32 value;

    /**
     * @brief Current timer state
     */
    TimerState state;

    /**
     * One timer tick in ms (it must be equal of the runnable cycle time)
     */
    static const uint32 resolution = AMBLS_RUNNABLE_CYCLE;
};

#else

#include "MockAbstractTimer.h"

#endif // !defined(MOCK_Timers)

#endif // !defined(VEHICLE_STATE_ABSTRACTTIMER_INCLUDED_)
