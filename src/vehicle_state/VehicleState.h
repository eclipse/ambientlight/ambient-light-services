/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file VehicleState.h
 *  @brief Implementation of the Class VehicleState
 *  @date  2019-10-25
 */

#if !defined(VEHICLE_STATE_VEHICLE_STATE_INCLUDED_)
#define VEHICLE_STATE_VEHICLE_STATE_INCLUDED_

#include "BlinkingStateInput.h"
#include "CentralLockingStateInput.h"
#include "ChoLhoStateInput.h"
#include "DoorStateInput.h"
#include "DriverPresenceStateInput.h"
#include "LightSensorStateInput.h"
#include "LightSwitchStateInput.h"
#include "StateMachine.h"
#include "TerminalStateInput.h"
#include "VStateInput.h"
#include "LhoStateInput.h"
#include "BlinkingOverlay.h"
#include "ChoTimers.h"
#include "BlinkTimer.h"
#include "LhoTimer.h"
#include "EventIds.h"

#include "IRuntime.h"

#if !defined(MOCK_VehicleState)
/**
 * @brief This class represents VehicleState module 
 * 
 */
class VehicleState
{

public:
   
    /**
    * @brief Construct a new VehicleState object
    * 
    */
    VehicleState();
    /**
     * @brief Destroys the VehicleState object
     * 
     */
    virtual ~VehicleState();
    /**
     * @brief Blinking monitor
     * 
     */
    BlinkingStateInput blinkingInputs;
    /**
     * @brief Central locking monitor
     * 
     */
    CentralLockingStateInput centralLockingInputs;
    /**
     * @brief "Coming home/Leaving home" related inputs monitor
     * 
     */
    ChoLhoStateInput choLhoInputs;
    /**
     * @brief Doors monitor
     * 
     */
    DoorStateInput doorInputs;
    /**
     * @brief Driver presence monitor
     * 
     */
    DriverPresenceStateInput driverPresencInputs;
    /**
     * @brief Light sensor monitor
     * 
     */
    LightSensorStateInput lightSensorInputs;
    /**
     * @brief Light switch monitor
     * 
     */
    LightSwitchStateInput lightSwitchInputs;
    /**
     * @brief Terminal monitor
     * 
     */
    TerminalStateInput terminalInputs;
    /**
     * @brief Vehicle inputs monitor
     * 
     */
    VStateInput vehicleInputs;
    /**
     * @brief "Leaving home" related monitor
     * 
     */
    LhoStateInput lhoInputs;
    /**
     * @brief Internal FSM
     * 
     */
    StateMachine stateMachine;
    /**
     * @brief Events filter
     * 
     */
    BlinkingOverlay blinkOverlay;
    /**
     * @brief "Choming home" timer
     * 
     */
    ChoTimer choTimer;
    /**
     * @brief "Choming home" exit timer
     * 
     */
    ChoExitTimer choExitTimer;
    /**
     * @brief "Leaving home" timer
     * 
     */
    LhoTimer lhoTimer;
    /**
     * @brief VehicleState working cycle
     * 
     */
    void cycle();
    /**
     * @brief Emits event for a Dispatcher
     * @param event[in] Event to emit
     */
    void dispatch(Event::Event ev);
    /**
     * @brief Checks whether event emission is allowed or not
     * @return true 
     * @return false 
     */
    bool isEventEnabled();
    /**
     * @brief Outputs all necessary signals
     * 
     */
    void output();
    /**
     * @brief Initializes VehicleState object before use
     * 
     */
    void init();
    /**
     * @brief Checks activation conditions
     * @return true if module should be active, otherwise returns false
     */
    bool isActiveState();
    
    /**
     * @brief Checks if CHO is ready
     * @param None
     * @return <i>true</i> if ready otherwise - <i>false</i>
     */
    bool isChoReady();

    /**
     * @brief Checks if LHO is ready
     * @param None
     * @return <i>true</i> if ready otherwise - <i>false</i>
     */
    bool isLhoReady();
    /**
     * @brief Sets number of maximum allowed "Leaving home" animations to it's initial value
     * 
     */
    void ResetLhoDynamicProtection() { nLhoPlayCfg = nLhoPlay = LhoDynamicProtection(); }

    /**
     * @brief Reads "Activate Curves" parameter
     * @return Parameter value
     */
    inline uint8 ActivateCurves(){ return runtime->getActivateCurves(); }
    /**
     * @brief Reads "Blink delay" parameter
     * 
     * Provides an access to "Blink delay" function activity
     * @return Parameter value
     */
    inline uint8 BlinkDelay(){ return runtime->getBlinkDelay(); }
    /**
     * @brief Reads "Choming home Exit time" parameter
     * 
     * Provides an access to "Choming home" exit timer period
     * @return Parameter value
     */
    inline uint8 ChoExitTime(){ return runtime->getChoExitTime(); }
    /**
     * @brief Reads "Leaving Home Dynamic Protection" parameter
     * 
     * Provides an access to a maximum number of allowed "Leaving home" animations
     * @return Parameter value
     */
    inline uint8 LhoDynamicProtection(){ return runtime->getLhoDynamicProtection(); }
    /**
     * @brief Reads "Leaving Home release" parameter
     * @return Parameter value
     */
    inline uint8 LhoRelease(){ return runtime->getLhoRelease(); }
    /**
     * @brief Reads "SWC Signal Delay Time" parameter
     * 
     * Provides an access to a blinking delay value
     * @return Parameter value
     */
    inline uint8 SwcSignalDelayTime(){ return runtime->getSwcSignalDelayTime(); }
    /**
     * @brief Reads "Events Scene" parameter
     * 
     * Provides an access to car zone descriptors
     * @return Parameter value
     */
    inline uint8 EventsScene(uint8 num) { return (4 > num) ? runtime->getEventsScene()[num] : 255; };

    /**
     * @brief "surroundAmbLSPrio" output value
     * 
     */
    uint8 surroundAmbLSPrio;
     /**
     * @brief "ambLSAFSStagingState" output value
     * 
     */
    uint8 ambLSAFSStagingState;
     /**
     * @brief "ambLSModificationReason" output value
     * 
     */
    uint8 ambLSModificationReason;
     /**
     * @brief "ambLSModificationState" output value
     * 
     */
    boolean ambLSModificationState;
     /**
     * @brief "ambLSPerCntr" output value
     * 
     */
    uint8 ambLSPerCntr;
     /**
     * @brief "nLhoPlay" output value
     * 
     */
    uint8 nLhoPlay;
     /**
     * @brief "nLhoPlayCfg" output value
     * 
     */
    uint8 nLhoPlayCfg;
     /**
     * @brief Initialization status
     * 
     */
    boolean initialized;
    /**
     * @brief ID of a previous FSM state
     * 
     */
    StateIds::StateIds prevStateId;
};

#else 

#include "MockVehicleState.h"

#endif // !defined(MOCK_VehicleState)

#endif // !defined(VEHICLE_STATE_VEHICLE_STATE_INCLUDED_)
