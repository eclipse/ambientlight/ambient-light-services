/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file CentralLockingStateInput.cpp
 *  @brief Implementation of the Class CentralLockingStateInput
 *  @date 2019-12-05
 */

#include "CentralLockingStateInput.h"


#include "InputType.h"
#include "VehicleState.h"
#include "StateMachine.h"

extern VehicleState vehicleState;


CentralLockingStateInput::CentralLockingStateInput(VehicleState* vState):
    vehicleState(vState),
    inputMask(0u),
    edgeMask(0u),
    goesTo0(0u),
    goesTo1(0u)
{
    
}

CentralLockingStateInput::~CentralLockingStateInput()
{

}

void CentralLockingStateInput::cyclicRead()
{
    uint8 newMask = 0u;
    
    if ( CentralLockingOpenHdVirtualPedal() )
    {
        updateMask( newMask, nOpenHdVirtualPedal );
    }
    
    if ( CentralLockingOpenRadio() )
    {
        updateMask( newMask, nOpenRadio );
    }
    
    if ( CentralLockingOpenDFD() )
    {
        updateMask( newMask, nOpenDFD );
    }
    
    if ( CentralLockingOpenPFD() )
    {
        updateMask( newMask, nOpenPFD );
    }

    if ( newMask != inputMask )
    {
        edgeMask = inputMask ^ newMask;
        goesTo1 = edgeMask & newMask;
        goesTo0 = edgeMask & (~newMask);

        inputMask = newMask;

        onChange();
    }
}

void CentralLockingStateInput::onChange()
{
    vehicleState->blinkingInputs.blinkTimer.handleEvent( InputType::CentralLocking );
    vehicleState->stateMachine.handleEvent( InputType::CentralLocking );
}
