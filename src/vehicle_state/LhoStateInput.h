/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file LhoStateInput.h
 *  @brief Implementation of the Class LhoStateInput
 *  @date  2019-11-11
 */

#if !defined(VEHICLE_STATE_LHO_STATE_INPUT_INCLUDE_)
#define VEHICLE_STATE_LHO_STATE_INPUT_INCLUDE_

#include "StateInput.h"

#include "InputType.h"
#include "EventIds.h"
#include "StateIds.h"

/**
 * @brief the class LhoStateInput
 * It proccesses input events related to LHO
 */

#if !defined(MOCK_LhoStateInput)

class LhoStateInput
{
public:

    /**
     * @brief the enumeration LhoInputEvent
     * Every constant of the enumeration corresponds to one of LHO output event directly
     */
    enum LhoInputEvent
    {
        None,
        HDOpen, 
        RadioUnlock, 
        RadioLock,
    };

    /**
     * @brief Construct a new LhoStateInput object
     */
    LhoStateInput(VehicleState *vState);
    virtual ~LhoStateInput();
    
    /**
     * @brief  handles an input event, and if it's related to LHO, calculates
     *         which output LHO event must be dispatched and saves it in
     *         the variable <i>lastInputEvent</i> using the constants from 
     *         the enumeration <i>LhoInputEvent</i> 
     *         (not from <i>EventIds</i>). 
     *
     * This function can be called from all states.
     * - If it called from idle and returns not None, transit to LHO state
     * - If it called from cho and returns not None, transit to LHO state
     * - If it called from choRun and returns not None, transit to LHO state.
     * - Both LHO states handles all returned values != None.
     *
     * The blink timer must be started already for all events returned.
     *
     * @param  event Input event to be handled 
     *
     * @param  stateId It is Id of current state.>
     *
     * @return saved <i>lastInputEvent</i> or <i>None</i> if the input event is
     *         not applicable to current state.
     */
    LhoInputEvent InputEvent( InputType::InputType event, StateIds::StateIds stateId );

    /**
     * @brief  Returns last LHO event which was saved in InputEvent()
     */
    LhoInputEvent LastInputEvent() const { return lastInputEvent; }

    /**
     * @brief Resets saved last LHO event
     */
    void ResetLastInputEvent() { lastInputEvent = None; }

    /**
     * @brief Pointer to global enviroment
     */
    VehicleState *vehicleState;

private:
    LhoInputEvent lastInputEvent;
};

#else

#include "MockLhoStateInput.h"


#endif // !defined(MOCK_LhoStateInput)

#endif // !defined(VEHICLE_STATE_LHO_STATE_INPUT_INCLUDE_)
