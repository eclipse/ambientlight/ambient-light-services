/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file StateInput.h
 *  @brief Implementation of the Interface StateInput
 *  @date  2019-10-25
 */
#if !defined(VEHICLE_STATE_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_STATE_INPUT_INCLUDED_

/**
 * @brief Base class for all signal inputs
 * 
 * All concrete inputs should be derived from this class
 */
class StateInput
{

public:
	/**
	 * @brief Constructs a new StateInput object
	 * 
	 */
	StateInput() {

	}
	/**
	 * @brief Destroys the StateInput object
	 * 
	 */
	virtual ~StateInput() {

	}	
	/**
	 * @brief Signal monitoring should be done here
	 * 
	 */
	virtual void cyclicRead() =0;
	/**
	 * @brief Actions caused by signal changes should be done here 
	 * 
	 */
	virtual void onChange() =0;

};
#endif // !defined(VEHICLE_STATE_STATE_INPUT_INCLUDED_)
