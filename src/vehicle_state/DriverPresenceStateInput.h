/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file DriverPresenceStateInput.h
 *  @brief Implementation of the Class DriverPresenceStateInput
 *  @date 2019-10-25
 */
#if !defined(VEHICLE_STATE_DRIVER_PRESENCE_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_DRIVER_PRESENCE_STATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

#if !defined(MOCK_DriverPresenceStateInput)

/**
 * @brief This class is intended to monitor driver presence
 * 
 */
class DriverPresenceStateInput : public StateInput
{

public:
    /**
     * @brief Driver Presence status enumeration
     * 
     */
    enum DriverPresenceStatuses
    {
        StatusInit = enDriverPresenceStatus_Init,
        StatusAbsent = enDriverPresenceStatus_Absent,
        StatusEntry = enDriverPresenceStatus_Entry,
        StatusPresent = enDriverPresenceStatus_Present,
        StatusPresentAndVerified = enDriverPresenceStatus_PresentVerified,
        StatusExitIntent = enDriverPresenceStatus_ExitIntent,
        StatusExit = enDriverPresenceStatus_Exit,
        StatusError = enDriverPresenceStatus_Error
    };
    /**
     * @brief Constructs a new DriverPresenceStateInput object
     * 
     */
    DriverPresenceStateInput(){}
    /**
     * @brief Destroys the DriverPresenceStateInput object
     * 
     */
    virtual ~DriverPresenceStateInput(){}
    /**
     * @brief Gets seat belt status
     * @return returns true if seat belt is locked, otherwise returns false
     * @todo Belt status is not a bool value in fact
     */
    inline boolean DriverBeltLock() { return runtime->getDriverBeltLock(); }
    /**
     * @brief Gets driver presence status
     * @return Of of the following possible values:
     * - 0 (init)
     * - 1 (reserved)
     * - 2 (absent)
     * - 3 (reserved)
     * - 4 (entry)
     * - 5 (reserved)
     * - 6 (present)
     * - 7 (reserved)
     * - 8 (present and verified)
     * - 9 (reserved)
     * - 10 (reserved)
     * - 11 (exit intent)
     * - 12 (reserved)
     * - 13 (exit)
     * - 14 (reserved)
     * - 15 (error)
     */
    inline DriverPresenceStatuses DriverPresenceStatus() 
    { 
        return static_cast<DriverPresenceStatuses>( runtime->getDriverPresenceStatus() );
    }
    /**
     * @brief Gets seat occupancy status
     * @return returns true if seat is occupied, otherwise returns false
     * @todo Seat occupancy is not a bool value in fact
     */
    inline boolean DriverSeatOccupancy() { return runtime->getDriverSeatOccupancy(); }
    /**
     * @brief Polls input signals and reacts on their changes
     * 
     */
    virtual void cyclicRead(){}
    /**
     * @brief Does all needed actions as a reaction on signal changes
     * 
     */
    virtual void onChange(){}
};

#else

#include "MockDriverPresenceStateInput.h"

#endif // !defined(MOCK_DriverPresenceStateInput)

#endif // !defined(VEHICLE_STATE_DRIVER_PRESENCE_STATE_INPUT_INCLUDED_)
