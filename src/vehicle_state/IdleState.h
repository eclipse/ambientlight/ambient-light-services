/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file IdleState.h
 * @brief Implementation of the Class IdleState
 * state operation
 * @date 2019-10-25
 */


#if !defined(VEHICLE_STATE_IDLE_STATE_INCLUDED_)
#define VEHICLE_STATE_IDLE_STATE_INCLUDED_

#include "AbstractState.h"

/**
 * @brief The class is intended to handle incoming input events in Idle state.
 *        It is derived from AbstractState class. 
 */

#if !defined(MOCK_IdleState)

class IdleState : public AbstractState
{
public:
    /**
     * @brief Construct a new <i>IdleState</i> object
     * @param vState Global execution context
     */
    explicit IdleState( VehicleState * vState);

    /**
     * @brief Default destructor for IdleState
     */
    virtual ~IdleState();

    /**
     * @brief Does <i>Idle</i> state  startup actions:
     *        - dispatches event 0
     *        - stops LHO amd CHO timers
     * @param None
     * @return None
     */
    virtual void entryAction();

    /**
     * @brief Handles input events while <i>Idle</i> state is active. 
     * @param Event event to be handled
     * @return None
     */
    virtual void handleEvent( InputType::InputType event );

    /**
     * @brief Cycle function of state. It does nothing.
     * @param None
     * @return None
     */
    virtual void cycle();

    /**
     * @brief Exit action of state. It does nothing.
     * @param None
     * @return None
     */
    virtual void exitAction();

    /**
     * @brief Checks if CHO is ready
     * @param None
     * @return <i>true</i> if ready otherwise - <i>false</i>
     */
    bool isChoReady();

    /**
     * @brief Checks if LHO is ready
     * @param None
     * @return <i>true</i> if ready otherwise - <i>false</i>
     */
    bool isLhoReady();

    /**
     * @brief This object is to initialize <i>transitTarget</i> pointer 
     *        in <i>StateMachine</i> class
     */
    Transition defaultTransition;

#if !defined(AMBLS_UNIT_TESTS)
private:
#endif    
    // Allowed transitions
    Transition toChoState;
    Transition toLhoState;

};

#else

#include "MockIdleState.h"

#endif // !defined(MOCK_IdleState)

#endif // !defined(VEHICLE_STATE_IDLE_STATE_INCLUDED_)
