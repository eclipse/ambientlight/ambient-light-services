/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file CentralLockingStateInput.h
 *  @brief Implementation of the Class CentralLockingStateInput
 *  @date 2019-10-25
 */

#if !defined(VEHICLE_STATE_CENTRAL_LOCKING_STATE_INPUT_INCLUDED_)
#define VEHICLE_STATE_CENTRAL_LOCKING_STATE_INPUT_INCLUDED_

#include "IRuntime.h"
#include "StateInput.h"

class VehicleState;

/**
 *  @brief This class is intended to monitor vehicle central locking
 * subsystem
 *
 *  The class collects the input signals releated to CentralLocking.
 *  It provides interface to 
 *  - read state of each input signal
 *  - output InputEvent::CentralLocking if at least one of input signals
 *    was triggered.
 *  - check if input signal is triggered to 1 or to zero 
 */
#if !defined(MOCK_CentralLockingStateInput)
class CentralLockingStateInput : public StateInput
{
    /**
     * @brief Bit mask for each input signal
     */
    enum InputBit
    {
        nOpenPFD     = (uint8)(1u << 2u),
        nOpenDFD     = (uint8)(1u << 3u),
        nOpenHdVirtualPedal = (uint8)(1u << 4u),
        nOpenRadio  = (uint8)(1u << 5u)
    };
    
public:
    /**
     * @brief Construct a new CentralLockingStateInput object
     */
    CentralLockingStateInput(VehicleState*);

    /**
     * @brief Default destructor for the class
     */
    virtual ~CentralLockingStateInput();
    
    /**
     * @brief Reads input signal <i>OpenPFD</i> from RTE
     * @return true if the signal is equal to 1 otherwise - false
     */
    inline boolean CentralLockingOpenPFD(){ return runtime->getCentralLockingOpenPFD(); }

    /**
     * @brief Reads input signal <i>OpenDFD</i> from RTE
     * @return true if the signal is equal to 1 otherwise - false
     */
    inline boolean CentralLockingOpenDFD(){ return runtime->getCentralLockingOpenDFD(); }

    /**
     * @brief Reads input signal <i>OpenHdVirtualPedal</i> from RTE
     * @return true if the signal is equal to 1 otherwise - false
     */
    inline boolean CentralLockingOpenHdVirtualPedal(){ return runtime->getCentralLockingOpenHdVirtualPedal(); }

    /**
     * @brief Reads input signal <i>OpenRadio</i> from RTE
     * @return true if the signal is equal to 1 otherwise - false
     */
    inline boolean CentralLockingOpenRadio(){ return runtime->getCentralLockingOpenRadio(); }


    /**
     * @brief Checks if input signal <i>OpenPFD</i> is triggered 0 -> 1
     * @return true if the signal is triggered to 1 otherwise - false
     */
    boolean OpenPFDGoesTo1(){ return GoesTo1( nOpenPFD ); }

    /**
     * @brief Checks if input signal <i>OpenDFD</i> is triggered 0 -> 1
     * @return true if the signal is triggered to 1 otherwise - false
     */
    boolean OpenDFDGoesTo1(){ return GoesTo1( nOpenDFD ); }

    /**
     * @brief Checks if input signal <i>OpenHdVirtualPedal</i> is triggered 0 -> 1
     * @return true if the signal is triggered to 1 otherwise - false
     */
    boolean OpenHdVirtualPedalGoesTo1(){ return GoesTo1( nOpenHdVirtualPedal ); }

    /**
     * @brief Checks if input signal <i>OpenRadio</i> is triggered 0 -> 1
     * @return true if the signal is triggered to 1 otherwise - false
     */
    boolean OpenRadioGoesTo1(){ return GoesTo1( nOpenRadio ); }


    /**
     * @brief Checks if input signal <i>OpenPFD</i> is triggered 1 -> 0
     * @return true if the signal is triggered to 0 otherwise - false
     */
    boolean OpenPFDGoesTo0(){ return GoesTo0( nOpenPFD ); }

    /**
     * @brief Checks if input signal <i>OpenDFD</i> is triggered 1 -> 0
     * @return true if the signal is triggered to 0 otherwise - false
     */
    boolean OpenDFDGoesTo0(){ return GoesTo0( nOpenDFD ); }

    /**
     * @brief Checks if input signal <i>OpenHdVirtualPedal</i> is triggered 1 -> 0
     * @return true if the signal is triggered to 0 otherwise - false
     */
    boolean OpenHdVirtualPedalGoesTo0(){ return GoesTo0( nOpenHdVirtualPedal ); }

    /**
     * @brief Checks if input signal <i>OpenRadio</i> is triggered 1 -> 0
     * @return true if the signal is triggered to 0 otherwise - false
     */
    boolean OpenRadioGoesTo0(){ return GoesTo0( nOpenRadio ); }

    /**
     * @brief Checks if at least one of input signals is triggered 0 -> 1
     * @return true if the signal is triggered to 0 otherwise - false
     */
    boolean anySignalGoesTo1() { return goesTo1 != 0u; }

    /**
     * @brief Checks if at least one of input signals is triggered 1 -> 0
     * @return true if the signal is triggered to 0 otherwise - false
     */
    boolean anySignalGoesTo0() { return goesTo0 != 0u; }

    /**
     * @brief Reads all input signals 
     * It reads all input signals and checks if a signal or signals changed 
     * after previous reading. 
     * If some signals are changed, it:
     * - calls <i>onChanged()</i>
     * - calculates 3 masks: <i>edgeMask</i>, <i>goesTo0</i>, <i>goesTo1</i>
     */
    virtual void cyclicRead();

    /**
     * If some signals are changed, it 
     * - calls <i>handleEvent()</i> of <i>BlinkTimer</i> 
     * - calls <i>handleEvent()</i> of  <i>currentState</i>
     */
    virtual void onChange();

    /* Assigns a new set of rised edges */
    void setGoesTo1(uint8 val) { goesTo1 = val;}
    /* Assigns a new set of fallen signals */
    void setGoesTo0(uint8 val) { goesTo0 = val;}


private:
    VehicleState* vehicleState;
    /**
     * @brief Checks if given  input signal is triggered 0 -> 1
     * @param n bitmask of signal
     * @return true if given input signal triggers 0 -> 1
     */
    boolean GoesTo1( InputBit n ) { return ( goesTo1 & n ) != 0 ; }

    /**
     * @brief Checks if given  input signal is triggered 0 -> 1 otherwise - false
     * @param n bitmask of signal
     * @return true if given input signal triggers 1 -> 0 otherwise - false
     */
    boolean GoesTo0( InputBit n ) { return ( goesTo0 & n ) != 0 ; }

    /**
     * @brief Updates given bitmask by given signal mask
     * @param mask reference to bitmask
     * @param n bitmask of signal
     */
    static void updateMask( uint8 &mask, InputBit n )
    {
        mask |= n;
    }
    
    uint8 inputMask; // saves previous state of input boolean signals
    uint8 edgeMask;  // if a bit in this mask is 1, it means that corresponding signal goes
    uint8 goesTo0;   // if a bit in this mask is 1, it means that corresponding signal triggers 1 -> 0 
    uint8 goesTo1;   // if a bit in this mask is 1, it means that corresponding signal triggers 0 -> 1 
};

#else

#include "MockCentralLockingStateInput.h"

#endif // !defined(MOCK_CentralLockingStateInput)

#endif // !defined(VEHICLE_STATE_CENTRAL_LOCKING_STATE_INPUT_INCLUDED_)

