/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Scene.cpp
 * @brief Implementation of the Class Scene
 * @date 2020-04-13
 */

#include "Scene.h"
#include "Animation.h"
#include "Output.h"


extern Output output;

uint16 Scene::iDelayMax;

const Global Scene::gGlobal;
const CircleRing Scene::gCircleRing;
const CircleSegment Scene::gCircleSegment;
const CircleDynamicRing Scene::gCircleDynamicRing;
const CircleDynamicSegment Scene::gCircleDynamicSegment;
const Point Scene::gPoint;

Geometry const * const Scene::geometry[Scene::numOfGeometries] =
{
    &gGlobal,
    &gCircleRing,
    &gCircleSegment,
    &gCircleDynamicRing,
    &gCircleDynamicSegment,
    &gPoint
};

Output * const Scene::output = &::output;


inline uint64 bitMask(uint8 bit)
{
    return 1llu << static_cast<uint64>(bit);
}


Scene::Scene():
    timerValue(0),
    timerState(Stopped),
    lightMask(0),
    index(emptySceneIndex),
    group(0),
    nSteps(0),
    iStep(0),
    state(Init),
    tAni(0)
{
}

Scene::~Scene()
{
}


void Scene::start( uint8 sceneInd )
{
    index = sceneInd;
    group = (runtime->getSceneGroups())[sceneInd];
    nSteps = (runtime->getStepAmounts())[sceneInd];
    iStep = 0;

    startAnimation();
}


void Scene::startAnimation()
{
    uint8 const *p_Szn_Animation;
    uint8 const *p_Szn_t;
    uint8 light;


    // the list of animation indexes  related to the scene
    p_Szn_Animation = runtime->getAnimationIndexes() + index * maxNumOfAnimations;
    
    // the list of animation start delays related to the scene
    p_Szn_t = runtime->getAnimationDelays() + index * maxNumOfAnimations;
    
    // The animation index of the current substep
    ani.index = p_Szn_Animation[iStep];

    // The animation start delay of the current substep
    ani.startDelay = p_Szn_t[iStep];

    // mask with active lights
    lightMask = 0;
    for( light = 0; light < runtime->getTotalActuatorsAmount(); light++ )
    {
        if ( true == geometry[ani.type()]->isLightAvailable(ani, light) )
        {
            lightMask |= bitMask(light);
        }
    }
    
    // Start scene timer with the animation delay 
    state = StartAnimation;
    timerStart(ani.startDelay / timeDivider);
}

uint16 Scene::getLightDelay( uint8 light ) const
{
    // Calculate tDelay in 10ms ticks
    uint16 tDelay =  runtime->getActuatorDelays()[light];

    
    if ( iDelayMax > tDelay )
    {
        tDelay = iDelayMax - tDelay;
    }
    else
    {
        tDelay = 0u;
    }

    tDelay += geometry[ani.type()]->getTurnOnTime(ani, light);
    
    return tDelay;
}


void Scene::outputLight( uint8 light )
{
    uint8 actuator_type = runtime->getActuatorTypes()[light];

    switch( actuator_type )
    {
    case DataDirectType:
    case DataReferenceType:
    case DataType:
        output->dataPixel.input( light, group, ani );
        break;
        
    case ActualValueType:
    case TargetValueType:
        output->colorPixel.input( light, group, ani );
        break;
        
    default:
        break;
    }
}

void Scene::controlLights()
{
    uint8 light;
    uint16 tDelay, tDelay_min = 0xFFFFu;

    for( light = 0u; light < runtime->getTotalActuatorsAmount(); light++ )
    {
        if ( bitMask(light) == ( lightMask & bitMask(light) ) )
        {
            tDelay = getLightDelay( light );

            if ( tDelay == tAni  )
            {
                // output light
                outputLight( light );

                // Delete light from actual light list
                lightMask &= ~bitMask(light);
            }
            else if ( tDelay < tDelay_min )
            {
                tDelay_min  = tDelay;
            }
            else
            {
                // MISRA
            }
        }
    }

    if( state != FinishAnimation )
    {
        // are there the delayed lights, 
        if( lightMask != 0 )
        {
            if( tDelay_min > ani.duration() )
            {
                // bad dataSet. A delay for light more than animation duration.
                tDelay_min = ani.duration();
            }
        
            tDelay = tDelay_min - tAni;
        }
        else
        {
            tDelay = ani.duration() - tAni;
        }

		tAni += tDelay;
		if (tAni >= ani.duration())
		{
			state = FinishAnimation;
		}
		else
		{
			state = RunAnimation;
		}
        timerStart( tDelay / timeDivider );
    }
}

void Scene::onTimerExpired()
{
    switch (state)
    {
    case StartAnimation:
        tAni = 0;
        controlLights();
        break;
        
    case RunAnimation:
        controlLights();
        break;

    case FinishAnimation:
        controlLights();
        iStep++;
        if ( iStep < nSteps )
        {
            startAnimation();
        }

    default:
        break;
    }
}

void Scene::calculateDelayMax()
{
    uint16 i;
    uint8 pDelayMax, pActuatorDelay;

    // Calculate iDelayMax is in 10ms ticks

    iDelayMax = 0;  // i_Delay_Max

    // get maximum from iDelayMax and all p_Light_Delay
    for ( i = 0u; i < runtime->getTotalActuatorsAmount(); i++ )  //  (0 <= i < p_Num_Luminaries)
    {
        pActuatorDelay = runtime->getActuatorDelays()[i];  // p_Light_Delay
        if ( pActuatorDelay > iDelayMax )
        {
            iDelayMax = pActuatorDelay;
        }
    }

    // Limit i_Delay_Max to p_Delay_Max
    pDelayMax = runtime->getMaxTransportDelay() / 10u;    //  p_Delay_Max (1ms -> 10ms)
    if ( iDelayMax > pDelayMax )
    {
        iDelayMax = pDelayMax;
    }
}

