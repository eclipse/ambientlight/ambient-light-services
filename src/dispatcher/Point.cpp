/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Point.cpp
 * @brief Definitions for the class Point
 * @date 2020-04-17
 * 
 */

#include "IRuntime.h"
#include "Geometry.h"

bool Point::isLightWithin(const Animation& anim, uint8 lightNum) const
{
    PolarCoordinates start;
    bool aniGo = false;
	uint8 lightPos = runtime->getActuatorPositions()[lightNum];
    
    anim.startPosition( start );

    if (runtime->getPosAngles()[lightPos] == start.angle &&
        runtime->getPosRadiusLengths()[lightPos] == start.radius)
    {
        aniGo = true;
    }
    
    return aniGo;
}
