/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file AnimationType.h
 * @brief Implementation of the Enumeration AnimationType
 * @date 2020-04-13
 */

#if !defined(DISPATCHER_ANIMATION_TYPE_INCLUDED_)
#define DISPATCHER_ANIMATION_TYPE_INCLUDED_

#include "IRuntime.h"

/**
 * @namespace AnimationType
 * To access members of the enumeration by that name
 */
namespace AnimationType
{
    /**
     * @enum AnimationType
     * addressing input animation types by RTE constants
     */
    enum AnimationType
    {
        Global = ANIMATIONSTYP_GLOBAL,
        CircleRing = ANIMATIONSTYP_KREISRING,
        CircleSegment = ANIMATIONSTYP_KREISSEGMENT,
        CircleRingDynamic = ANIMATIONSTYP_KREISRING_DYNAMISCH,
        CircleSegmentDynamic = ANIMATIONSTYP_KREISSEGMENT_DYNAMISCH,
        Point = ANIMATIONSTYP_PUNKT
    };
}

#endif // !defined(DISPATCHER_ANIMATION_TYPE_INCLUDED_)
