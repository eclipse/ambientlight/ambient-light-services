/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file CircleDynamicSegment.cpp
 * @brief Definitions for the class CircleDynamicSegment
 * @date 2020-04-17
 * 
 */
#include "IRuntime.h"
#include "Geometry.h"

void CircleDynamicSegment::scaleAngles(uint16& startAngle, uint16& endAngle, uint16& lightAngle, uint16 rotationDir) const
{
    if (rotationDir == negativeRotation && startAngle < endAngle)
    {
        if (lightAngle <= startAngle)
        {
            lightAngle += fullCircle;
        }
        startAngle += fullCircle;
    }
    if (rotationDir == positiveRotation && startAngle > endAngle)
    {
        if (lightAngle <= endAngle)
        {
            lightAngle += fullCircle;
        }
        endAngle += fullCircle;
    }
}

bool CircleDynamicSegment::isLightWithin(const Animation& anim, uint8 lightNum) const
{
    bool aniGo = false;
    PolarCoordinates start, end;

    anim.startPosition( start );
    anim.endPosition( end );
	uint8 lightPos = runtime->getActuatorPositions()[lightNum];
    uint16 lightAngle = runtime->getPosAngles()[lightPos];

    scaleAngles(start.angle, end.angle, lightAngle, anim.rotationDirection());
    if (end.angle >= start.angle)
    {
        if (lightAngle >= start.angle && lightAngle <= end.angle)
        {
            aniGo = true;
        }
    }
    else
    {
        if (lightAngle <= start.angle && lightAngle >= end.angle)
        {
            aniGo = true;
        }
    }

    return aniGo;
}

uint16 CircleDynamicSegment::getTurnOnTime(const Animation& anim, uint8 lightNum) const
{
    uint32 numerator = 0;
    uint16 denominator = 0;
    uint16 result = 0;
    PolarCoordinates start, end;
	uint8 lightPos = runtime->getActuatorPositions()[lightNum];
    uint16 lightAngle = runtime->getPosAngles()[lightPos];

    anim.startPosition( start );
    anim.endPosition( end );

    scaleAngles(start.angle, end.angle, lightAngle, anim.rotationDirection());

    // calculate time offset of lamp
    if (true == isLightWithin(anim, lightNum) && start.angle != end.angle)
    {
        if (end.angle > start.angle)
        {
            denominator = end.angle - start.angle;
            numerator = static_cast<uint32>(anim.duration()) * static_cast<uint32>(lightAngle - start.angle);
        }
        else
        {
            denominator = start.angle - end.angle;
            numerator = static_cast<uint32>(anim.duration()) * static_cast<uint32>(start.angle - lightAngle);
        }

        result = static_cast<uint16>(numerator / denominator);
    }

    return result;
}
