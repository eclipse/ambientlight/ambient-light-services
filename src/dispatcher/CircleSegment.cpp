/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file CircleSegment.cpp
 * @brief Definitions for the class CircleSegment
 * @date 2020-04-17
 *
 */
#include "IRuntime.h"
#include "Geometry.h"

void CircleSegment::scaleAngles(uint16& startAngle, uint16& endAngle, uint16& lightAngle, uint16 rotationDir) const
{
    if (rotationDir == negativeRotation && startAngle < endAngle)
    {
        if (lightAngle <= startAngle)
        {
            lightAngle += fullCircle;
        }
        startAngle += fullCircle;
    }
    if (rotationDir == positiveRotation && startAngle > endAngle)
    {
        if (lightAngle <= endAngle)
        {
            lightAngle += fullCircle;
        }
        endAngle += fullCircle;
    }
}

bool CircleSegment::isLightWithin(const Animation& anim, uint8 lightNum) const
{
    bool aniGo = false;
    PolarCoordinates start, end;
	uint8 lightPos = runtime->getActuatorPositions()[lightNum];
    uint16 lightAngle = runtime->getPosAngles()[lightPos];

    anim.startPosition( start );
    anim.endPosition( end );

    scaleAngles(start.angle, end.angle, lightAngle, anim.rotationDirection());
    if (end.angle >= start.angle)
    {
        if (lightAngle >= start.angle && lightAngle <= end.angle)
        {
            aniGo = true;
        }
    }
    else
    {
        if (lightAngle <= start.angle && lightAngle >= end.angle)
        {
            aniGo = true;
        }
    }

    return aniGo;
}
