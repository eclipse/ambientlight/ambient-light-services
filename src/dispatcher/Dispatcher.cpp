/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Dispatcher.cpp
 * @brief Implementation of the Class Dispatcher
 * @date 2020-04-13
 */

#include "Dispatcher.h"
#include "IRuntime.h"

Dispatcher::Dispatcher():
    nScenes(0),
    prevEvent(Event::Init),
    activeEvent(Event::Init)
{
}

Dispatcher::~Dispatcher()
{
    
}

void Dispatcher::dispatch( Event::Event ev )
{
    if ( ev != prevEvent )
    {
        prevEvent = activeEvent;
        
        // A new event has to be dispatched
        activeEvent = ev;

        if ( activeEvent != Event::Init )
        {
            // stop processing all scenes of previous event if any
            stopScenes();

            // Start processing the active event
            startScenes();
        }
    }
    return;
}

void Dispatcher::stopScenes()
{
   int i;
   for ( i = 0; i < Scene::maxNumOfScenes; i++ )
   {
       sceneList[i].timerStop();
   }
}


void Dispatcher::cycle()
{
    int i;
    for ( i = 0; i < Scene::maxNumOfScenes; i++ )
    {
        sceneList[i].timerRun();
    }
}

void Dispatcher::startScenes()
{
    uint8 i;

    // Pointer to array with scene indexes for specific event
    // scenes[0]...scenes[3] - scene indexes for even events
    // scenes[4]...scenes[7] - scene indexes for odd events
    uint8 const *  scenes;
    uint8 start;  // start index in scenes[] for specific event
    
    start = (activeEvent % 2u) << 2u;
    
    switch ( static_cast<uint8>(activeEvent) )
    {
    case 0u:
    case 1u:
        scenes = runtime->getSceneListEv0Ev1();
        break;
    case 2u:
    case 3u:
        scenes = runtime->getSceneListEv2Ev3();
        break;
    case 4u:
    case 5u:
        scenes = runtime->getSceneListEv4Ev5();
        break;
    case 6u:
    case 7u:
        scenes = runtime->getSceneListEv6Ev7();
        break;
    case 8u:
    case 9u:
        scenes = runtime->getSceneListEv8Ev9();
        break;
    case 10u:
    case 11u:
        scenes = runtime->getSceneListEv10Ev11();
        break;
    case 12u:
    case 13u:
        scenes = runtime->getSceneListEv12Ev13();
        break;
    case 14u:
    case 15u:
        scenes = runtime->getSceneListEv14Ev15();
        break;
    case 16u:
    case 17u:
        scenes = runtime->getSceneListEv16Ev17();
        break;
    case 18u:
    case 19u:
        scenes = runtime->getSceneListEv18Ev19();
        break;
    case 20u:
    case 21u:
        scenes = runtime->getSceneListEv20Ev21();
        break;
    case 22u:
    case 23u:
        scenes = runtime->getSceneListEv22Ev23();
        break;
    case 24u:
    case 25u:
        scenes = runtime->getSceneListEv24Ev25();
        break;
    case 26u:
    case 27u:
        scenes = runtime->getSceneListEv26Ev27();
        break;
    case 31u:
    default:
        // exception to the rule: the event 31 handled as even
        start = 0;
        scenes = runtime->getSceneListEv31();
        break;
    }

    // Calculate i_Delay_Max
#if !defined(AMBLS_UNIT_TEST)    
    Scene::calculateDelayMax();
#else
    // it is not possible to mock up static methods 
    sceneList[0].calculateDelayMax();
#endif
    
    nScenes = 0;

    for( i=0; i < Scene::maxNumOfScenes; i++ )
    {
        uint8 sceneInd = scenes[start + i];

        if ( (sceneInd != Scene::emptySceneIndex) && (sceneInd <= runtime->getTotalScenesAmount()) )
        {
            sceneList[nScenes].start( sceneInd );
            nScenes++;
        }
    }
}

