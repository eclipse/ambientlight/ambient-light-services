/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Scene.h
 * @brief Implementation of the Class Scene
 * @date 2020-04-13
 */

#if !defined(DISPATCHER_SCENE_INCLUDED_)
#define DISPATCHER_SCENE_INCLUDED_

#include "PlatformTypes.h"
#include "EventIds.h"
#include "Geometry.h"
#include "Animation.h"

#if !defined(MOCK_Scene)

class Output;

/**
 * @brief The Scene class is intended to implement scene processing
 */
class Scene
{

public:

    
    /**
     * @brief Max number of scenes assigned to one event
     */
    static const uint8 maxNumOfScenes = 4;
    
    /**
     * @brief Index value for empty scene
     */
    static const uint8 emptySceneIndex = 255u;

    /**
     * @brief Max number of animations per scene
     */
    static const uint8 maxNumOfAnimations = 32u;
    
    /**
     * @brief Number of animation geometries' types
     */
    static const uint8 numOfGeometries = 6;

    /**
     * @brief Constructs a new Scene object
     * 
     */
    Scene();

    /**
     * @brief Destroys the Scene object
     * 
     */
    ~Scene();

    /**
     * @brief scene start
     * @param[in] sceneInd  Scene index
     * @return none
     */
    void start( uint8 sceneInd );

    /**
     * @brief start Animation for current step: 0 <= iStep < nSteps 
     *        Note step range is: 0 <= iStep < nSteps
     * @return none
     */
    void startAnimation();
    
    /**
     * @brief  get delay for an actuator
     * @param[in] light  actuator ID
     * @return delay for given actuator
     */
    uint16 getLightDelay( uint8 light ) const;

    /**
     * @brief  output light action
     * @param[in] light  actuator ID
     * @return none
     */
    void outputLight( uint8 light );

    /**
     * @brief Control lights for current animation animation
     * @param none
     * @return none
     */
    void controlLights();
   
    /**
     *  @brief Runs scene timer. It must be called on every runnable
     *         call.  It decrements timer value on every call and
     *         calls onTimerExpired() if the timer value == 0.
     *  @warning If timerRun() is called on next cycle after timerStart(),
     *           start(0) and start(1) have the same behavour.
     */
    void timerRun()
    {
        if ( Run == timerState )
        {
            if (  timerValue > 0 )
            {
                timerValue --;
            }

            if ( timerValue == 0 )
            {
                timerState = Expired;
                onTimerExpired();
            }
        }
    }

    /**
     *  @brief Starts scene timer 
     *  @param: timerVal  the timer value  
     *  @return none
     */
    void timerStart( uint32 timerVal )
    {
        timerState = Run;
        timerValue = timerVal;
        if (timerValue == 0)
        {
            timerState = Expired;
            onTimerExpired();
        }
        else
        {
            timerState = Run;
        }
    }

    /**
     *  @brief Stops the timer
     *  @return none
     */
    void timerStop()
    {
        timerValue = 0u;
        timerState = Stopped;
    }
    
    /**
     *  @brief It is called if the timer expires
     *  @return none
     */
    void onTimerExpired();
    

    /**
     *  @brief Calculates i_Delay_Max
     *  @return none
     */
    static void calculateDelayMax();

#if !defined(AMBLS_UNIT_TEST)    
private:
#endif
    
    /**
     *  @enum scene states
     */
    enum SceneState
    {
        Init,
        StartAnimation,
        RunAnimation,
        FinishAnimation
    };

    /**
     *  @enum Light output types
     */
    enum LightOutType   /** @todo use RTE constants here when RTE is ready */
    {
        ActualValueType = 0,
        TargetValueType = 1,
        DataType = 2 ,
		DataDirectType,
		DataReferenceType,
	};

    
    /**
     *  @brief Enumeration with scene timer states
     */
    enum TimerState
    {
        Stopped,
        Expired,
        Run
    };
    
    /**
     * @brief Scene timer value
     */
    uint32 timerValue;

    /**
     * @brief Scene timer state
     */
    TimerState timerState;

    /**
     *  @brief mask of light actuators
     */
    uint64 lightMask;
    
    uint8 index;   // scene index
    uint8 group;
    uint8 nSteps;   // n of steps (n of animations in the scene)
    uint8 iStep;    // current step in the scene
    SceneState state;    
    uint16 tAni;    // current animation time
    Animation ani;  // current animation


    /**
     * @brief Pointer to output class 
     */
    static Output * const output;

    /**
     * @brief The maximum of all output delays of all actuators i_Delay_Max
     */
    static uint16 iDelayMax;

    /**
     * @brief interface objects to specific geometry
     */
    static const Global gGlobal;
    static const Point gPoint;
    static const CircleRing gCircleRing;
    static const CircleDynamicRing gCircleDynamicRing;
    static const CircleSegment gCircleSegment;
    static const CircleDynamicSegment gCircleDynamicSegment;

    /**
     * @brief General interface to specific geometry objects
     */
    static Geometry const * const geometry[numOfGeometries];
    

    // Scene timer resolution (number of ms in one tick)
    static const uint16 timerResolution = AMBLS_RUNNABLE_CYCLE;
    // RTE time parameters' resolution (number of ms in one tick)
    static const uint16 runtimeTimeResolution = AMBLS_DATASET_TIME_RESOLUTION;
    static const uint16 timeDivider = timerResolution / runtimeTimeResolution;
};
#else
#include "MockScene.h"
#endif // !defined(MOCK_CHORunState)

#endif // !defined(DISPATCHER_SCENE_INCLUDED_)
