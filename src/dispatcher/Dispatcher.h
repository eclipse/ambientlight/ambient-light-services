/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Dispatcher.h
 * @brief Implementation of the Class Dispatcher
 * @date 2020-04-13
 */

#if !defined(DISPATCHER_DISPATCHER_INCLUDED_)
#define DISPATCHER_DISPATCHER_INCLUDED_

#include "EventIds.h"
#include "Scene.h"
#include "Animation.h"

#if !defined(MOCK_Dispatcher)

/**
 * @brief The Dispatcher class is intended to implement ExAmb
 * dispatcher functionality
 */
class Dispatcher
{

public:

    /**
     * @brief Constructs a new Dispatcher object
     * 
     */
    Dispatcher();

    /**
     * @brief Destroys the Dispatcher object
     * 
     */
    ~Dispatcher();

    /**
     * @brief Dispatches event calculated by VehicleState. Called from
     *        VehicleState if there is a new event calculated.
     * @param[in] ev An event to be dispatched
     * @return none
     */
    void dispatch(Event::Event ev);

    /**
     * @brief It playes the scenes assigned to active event.  Called
     *        on every cycle of AmbLS runnable.
     * @param none
     * @return none
     */
    void cycle();

   /**
     * @brief Prepares scenes for active event and start playing for them
     * @param none
     * @return none
     */
    void startScenes();

   /**
     * @brief stop processing all scenes of previous event if any
     * @param none
     * @return none
     */ 
    void stopScenes();


   /**
     * @brief Calculate i_Delay_Max
     * @param none
     * @return none
     */ 
    void calculateDelayMax();
    

    /**
     * @brief Scene list, assigned to active event
     */
    Scene sceneList[Scene::maxNumOfScenes];


    /**
     * @brief the number of active scenes in scene list
     */
    uint8 nScenes;

#if !defined(AMBLS_UNIT_TEST)    
private:
#endif    

   /**
     * @brief Previous value of event
     */
    Event::Event prevEvent;
    Event::Event activeEvent;
};

#else

#include "MockDispatcher.h"

#endif // !defined(MOCK_Dispatcher)

extern Dispatcher dispatcher;

#endif // !defined(DISPATCHER_DISPATCHER_INCLUDED_)
