/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file CircleDynamicRing.cpp
 * @brief Definitions for the class CircleDynamicRing
 * @date 2020-04-17
 *
 */
#include "IRuntime.h"
#include "Geometry.h"

bool CircleDynamicRing::isLightWithin(const Animation& anim, uint8 lightNum) const
{
    bool aniGo = false;
    PolarCoordinates start, end;
	uint8 lightPos = runtime->getActuatorPositions()[lightNum];
    uint16 lightRadius = runtime->getPosRadiusLengths()[lightPos];

    anim.startPosition( start );
    anim.endPosition( end );

    if (start.radius <= end.radius)
    {
        if (start.radius <= lightRadius &&
            end.radius >= lightRadius)
        {
            aniGo = true;
        }
    }
    else
    {
        if (start.radius >= lightRadius &&
            end.radius <= lightRadius)
        {
            aniGo = true;
        }
    }

    return aniGo;
}

uint16 CircleDynamicRing::getTurnOnTime(const Animation& anim, uint8 lightNum) const
{
    uint32 numerator = 0;
    uint16 denominator = 0;
    uint16 result = 0;
	uint8 lightPos = runtime->getActuatorPositions()[lightNum];
    uint16 lightRadius = runtime->getPosRadiusLengths()[lightPos];
    PolarCoordinates start, end;

    anim.startPosition( start );
    anim.endPosition( end );

    if (true == isLightWithin(anim, lightNum) && start.radius != end.radius)
    {
        if (end.radius > start.radius)
        {
            denominator = end.radius - start.radius;
            numerator = static_cast<uint32>(anim.duration()) *
                        static_cast<uint32>(lightRadius - start.radius);
        }
        else
        {
            denominator = start.radius - end.radius;
            numerator = static_cast<uint32>(anim.duration()) *
                        static_cast<uint32>(start.radius - lightRadius);
        }
        result = static_cast<uint16>(numerator / denominator);
    }

    return result;
}
