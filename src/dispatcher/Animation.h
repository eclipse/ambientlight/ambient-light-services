/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Animation.h
 * @brief Implementation of the Class Animation
 * @date 2020-04-13
 */

#if !defined(DISPATCHER_ANIMATION_INCLUDED_)
#define DISPATCHER_ANIMATION_INCLUDED_

#include "PlatformTypes.h"
#include "Hsv.h"
#include "AnimationType.h"


/**
 * @brief Structure of polar coordinates
 */
struct PolarCoordinates
{
    uint16 radius;
    uint16 angle;
};

#if !defined(MOCK_Animation)


/**
 * @brief The Animation class is intended to get all necessary information about 
 * current animation. 
 * The information is following:
 * - type
 * - duration
 * - dimming time
 * - start position
 * - end position
 * - rotation direction
 * - HSV
 */

class Animation
{

public:
    /**
     * @brief Constructs a new Animation object
     * 
     */
    Animation();

    /**
     * @brief Destroys the Animation object
     * 
     */
    ~Animation();

    /**
     * @brief index of the animation for specific step (filled in scene.getAnimation()) 
     */
    uint8 index;

    /**
     * @brief start delay of the animation for specific step (filled in scene.getAnimation())
     */
    uint8 startDelay;    

    /**
     * @brief type of the animation (p_Any_Type)
     * @param none
     * @return type of the animation
     */
    AnimationType::AnimationType type() const
    {
        return static_cast<AnimationType::AnimationType>
            ( ( runtime->getAnimationTypes() )[index] );
    }
    
    /**
     * @brief Duration of the animation (p_Any_t)
     * @param none
     * @return Duration of the animation
     */
    uint16 duration() const
    {
        return ( runtime->getAnimationDurations() )[index];
    }

    /**
     * @brief dimming time of the animation
     * @param none
     * @return dimming time of the animation
     */
    uint16 dimmingTime() const
    {
        return ( runtime->getAnimationDimmingTimes() )[index];
    }

    /**
     * @brief start position of the animation (p_Pos_Angle and p_Pos_Radius)
     * @param[out] pc Polar cordinates
     * @return start position of the animation 
     */
    void startPosition( PolarCoordinates &pc ) const;

    /**
     * @brief end position of the animation (p_Pos_Angle and p_Pos_Radius)
     * @param[out] pc Polar cordinates
     * @return end position of the animation 
     */
    void endPosition( PolarCoordinates &pc ) const;

    /**
     * @brief rotation direction of the animation (p_Any_RotDir)
     * @param  none
     * @return rotation direction of the animation 
     */
    uint8 rotationDirection() const
    {
        return ( runtime->getRotationDirections() )[index];
    }
    
    /**
     * @brief fills HSV by parameters  
     * @param[out]  hsv Reference to HSV object
     * @return none
     */
    void getHsv( Hsv &hsv ) const;
    
};

#else

#include "MockAnimation.h"

#endif // !defined(MOCK_Animation)

#endif // !defined(DISPATCHER_ANIMATION_INCLUDED_)
