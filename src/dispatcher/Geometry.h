/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Geometry.h
 * @brief Implementation of the Interface Geometry
 * @date 2020-04-13
 */

#if !defined(DISPATCHER_GEOMETRY_INCLUDED_)
#define DISPATCHER_GEOMETRY_INCLUDED_

#include "PlatformTypes.h"
#include "Animation.h"

/**
 * @brief This class is intended to determine lights' locations
 * within predefined areas (geometric figures) around a vehicle
 * 
 * All concrete geometry classes describing geometric figures and corresponding
 * rules to infer light positions should be derived from this class
 * 
 * Every area can be precisely described by animation related info.
 * Every light can be considered as a part of a selected area according to rules
 * specific to that area ("Ani Go" condition)
 * 
 * 
 */
class Geometry
{
protected:
    /**
     * @brief Counterclockrwise direction along a Circle
     * 
     */
    const uint8 positiveRotation = 0u;
    /**
     * @brief Clockwise direction along a Circle
     * 
     */
    const uint8 negativeRotation = 1u;
    /**
     * @brief 360 degrees value
     * 
     */
    const uint16 fullCircle = 8000u;
    /**
     * @brief The topmost position index
     * 
     */
    const uint8 maxPosNumber = 31;
public:
    /**
     * @brief Constructs a new Geometry object
     * 
     */
    Geometry()
    {
    }
    /**
     * @brief Destroys the Geometry object
     * 
     */
    virtual ~Geometry()
    {
    }
    /**
     * @brief Checks whether light belongs to geometric figure or not
     * 
     * "Ani Go" condition rules take place here
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return true if light resides inside this area, otherwise returns
     * false
     * 
     */
    virtual bool isLightWithin(const Animation& anim, uint8 lightNum) const = 0;
    /**
     * @brief Computes time at which selected light should be turned on
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Time delay relative to start of animation 
     */
    virtual uint16 getTurnOnTime(const Animation& anim, uint8 lightNum) const = 0;

    bool isLightAvailable(const Animation& anim, uint8 lightNum) const
    {
        bool ret = false;

        if (runtime->getActuatorPositions()[lightNum] <= maxPosNumber)
        {
            ret = isLightWithin(anim, lightNum);
        }
        return ret;
    }
};

#if !defined(MOCK_Geometry)

/**
 * @brief Global geometry area around a vehicle
 * 
 * Within this area every single light is engaged for any action
 * 
 * 
 */
class Global : public Geometry
{

public:
    /**
     * @brief Constructs a new Global object
     * 
     */
    Global() {}
    /**
     * @brief Destroys the Global object
     * 
     */
    ~Global() {}
    /**
     * @brief Checks whether selected light belongs to this area or not
     * 
     * All lights are within this area
     * 
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Always returns true
     * 
     */
    bool isLightWithin(const Animation& anim, uint8 lightNum) const { return true; }
    /**
     * @brief Computes time at which selected light should be turned on
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Time delay relative to start of animation 
     */
    uint16 getTurnOnTime(const Animation& anim, uint8 lightNum) const { return 0; }

};

/**
 * @brief Defines geometric object of a type Point
 * 
 */
class Point : public Geometry
{

public:
    /**
     * @brief Constructs a new Point object
     * 
     */
    Point(){};
    /**
     * @brief Destroys the Point object
     * 
     */
    ~Point(){};
    /**
     * @brief Checks whether selected light belongs to the this area or not
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return true if light belongs to point area, otherwise returns false
     */
    bool isLightWithin(const Animation& anim, uint8 lightNum) const;
    /**
     * @brief Computes time at which selected light should be turned on
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Time delay relative to start of animation 
     */
    uint16 getTurnOnTime(const Animation& anim, uint8 lightNum) const { return 0; }
};

/**
 * @brief Defines geometric object of a type Circle Ring
 * 
 */
class CircleRing : public Geometry
{

public:
    /**
     * @brief Constructs a new Circle Ring object
     * 
     */
    CircleRing(){};
    /**
     * @brief Destroys the Circle Ring object
     * 
     */
    virtual ~CircleRing(){};
    /**
     * @brief Checks whether selected light belongs to the this area or not
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return true if light belongs to point area, otherwise returns false
     * 
     */
    bool isLightWithin(const Animation& anim, uint8 lightNum) const;
    /**
     * @brief Computes time at which selected light should be turned on
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Time delay relative to start of animation 
     */
    uint16 getTurnOnTime(const Animation& anim, uint8 lightNum) const { return 0; }
};

/**
 * @brief Defines geometric object of a type Circle Dynamic Ring
 */
class CircleDynamicRing : public Geometry
{

public:
    /**
     * @brief Constructs a new Circle Dynamic Ring object
     * 
     */
    CircleDynamicRing(){};
    /**
     * @brief Destroys the Circle Dynamic Ring object
     * 
     */
    virtual ~CircleDynamicRing(){};
    /**
     * @brief Checks whether selected light belongs to the this area or not
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return true if light belongs to point area, otherwise returns false
     * 
     */
    bool isLightWithin(const Animation& anim, uint8 lightNum) const;
    /**
     * @brief Computes time at which selected light should be turned on
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Time delay relative to start of animation 
     */
    uint16 getTurnOnTime(const Animation& anim, uint8 lightNum) const;

};

/**
 * @brief Defines geometric object of a type Circle Segment
 */
class CircleSegment : public Geometry
{

public:
    /**
     * @brief Constructs a new Circle Segment object
     * 
     */
    CircleSegment(){};
    /**
     * @brief Destroys the Circle Segment object
     * 
     */
    virtual ~CircleSegment(){};
    /**
     * @brief Checks whether selected light belongs to the this area or not
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return true if light belongs to point area, otherwise returns false
     */
    bool isLightWithin(const Animation& anim, uint8 lightNum) const;
    /**
     * @brief Computes time at which selected light should be turned on
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Time delay relative to start of animation 
     */
    uint16 getTurnOnTime(const Animation& anim, uint8 lightNum) const { return 0; }

private:
    /**
     * @brief Scales angles up or down if animation goes beyond the full circle
     * in a positive or negative directions respectively
     */
    void scaleAngles(uint16& startAngle, uint16& endAngle, uint16& lightAngle, uint16 rotationDir) const;
};

/**
 * @brief Defines geometric object of a type Circle Dynamic Ring
 */
class CircleDynamicSegment : public Geometry
{

public:
    /**
     * @brief Construct a new Radius Line object
     * 
     */
    CircleDynamicSegment(){};
    /**
     * @brief Destroy the Radius Line object    
     * 
     */
    virtual ~CircleDynamicSegment(){};
    /**
     * @brief Checks whether selected light belongs to the this area or not
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return true if light belongs to point area, otherwise returns false
     */
    bool isLightWithin(const Animation& anim, uint8 lightNum) const;
    /**
     * @brief Computes time at which selected light should be turned on
     * @param[in] anim Animation description
     * @param[in] light Light description
     * @return Time delay relative to start of animation 
     */
    uint16 getTurnOnTime(const Animation& anim, uint8 lightNum) const;

private:
    /**
     * @brief Scales angles up or down if animation goes beyond the full circle
     * in a positive or negative directions respectively
     */
    void scaleAngles(uint16& startAngle, uint16& endAngle, uint16& lightAngle, uint16 rotationDir) const;
};
#else   
    #include "MockGeometry.h"
#endif // !defined(MOCK_Geometry)
#endif // !defined(DISPATCHER_GEOMETRY_INCLUDED_)
