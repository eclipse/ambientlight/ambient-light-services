/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 * @file Animation.cpp
 * @brief Implementation of the Class Animation
 * @date 2020-04-13
 */

#include "Animation.h"


Animation::Animation():
    index(0),
    startDelay(0)
{
}

Animation::~Animation()
{
}

void Animation::startPosition( PolarCoordinates &pc ) const
{
    uint8 posIndex = ( runtime->getStartPositions() )[index];

    pc.angle = ( runtime->getPosAngles() )[posIndex];
    pc.radius = ( runtime->getPosRadiusLengths() )[posIndex];
}

void Animation::endPosition( PolarCoordinates &pc ) const
{
    uint8 posIndex = ( runtime->getEndPositions() )[index];

    pc.angle = ( runtime->getPosAngles() )[posIndex];
    pc.radius = ( runtime->getPosRadiusLengths() )[posIndex];
}

void Animation::getHsv( Hsv &hsv ) const
{
        // p_Ani_Target_Color[iAni]
    uint8 colorIdx = ( runtime->getAnimationTargetColors() )[index];

    // p_Color_Hue[colorIdx]
    hsv.numHue = ( runtime->getColorHues() )[colorIdx];
    // p_Color_Sat[colorIdx]
    hsv.numSat = ( runtime->getColorSaturations() )[colorIdx];
    // p_Color_Val[colorIdx]
    hsv.numVal = ( runtime->getColorValues() )[colorIdx];
}


