/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
*/

#include <iostream>
#include <unistd.h>
#include <VersionInfo.h>
#include "IRuntime.h"

#define Runtime_obj runtime

struct Hello_AmbLS_Scenario {
    int time;
    boolean *condition;
    boolean cond_value;
    const char *cond_name;
} scenario[] =
{
    { 5, &Runtime_obj->Terminal15, false, "Terminal15" },
    { 10, &Runtime_obj->CentralLockingOpenRadio, true, "OpenRadio"},
    { 11, &Runtime_obj->CentralLockingOpenRadio, false, "OpenRadio"},
    { 25, &Runtime_obj->Terminal15, true, "Terminal15" },
    { 26, &Runtime_obj->Terminal15, false, "Terminal15" },
    { 40, &Runtime_obj->DriverSideFrontDoorOpen, true, "DriverDoor"},
    { 50, &Runtime_obj->DriverSideFrontDoorOpen, false, "DriverDoor"},
    { 60, &Runtime_obj->CentralLockingOpenRadio, true, "OpenRadio"},
    { 70, &Runtime_obj->CentralLockingOpenRadio, false, "OpenRadio"},
};


extern "C" void cyclic_AmbLS_EventGenerator( void ); // AmbLS 10 ms cyclic runnable

int main(int argc, char **argv) 
{
    const uint8_DIM4 majorVersion = VERSION_MAJOR;
    const uint8_DIM4 minorVersion = VERSION_MINOR;
    const uint8_DIM4 patchVersion = VERSION_PATCH;
    (void)argc;
    (void)argv;

    std::cout << "AmbLS Library Version: " << 
    majorVersion[2] << majorVersion[3] << "." << 
    minorVersion[2] << minorVersion[3] << "." << 
    patchVersion[2] << patchVersion[3] << std::endl;

    std::cout << "=== Hello AmbLS: start scenario ===" << std::endl;
    /* Prepare RTE data */
    runtime->LhoRelease = true;
    runtime->LHActiveTime = 5;
    runtime->LHState = 0x3;
    runtime->CHActiveTime = 5;
    runtime->CHState = 0x3;
    runtime->LhoDynamicProtection = 8;
    runtime->LightSensorOnChoLho = true;
    runtime->StLdsAssistantDrivingLight = true;
    runtime->Terminal15 = false;
    runtime->BlinkDelay = 10;

    /* Main loop */
    for( int i=0 ;i<130000; i++ )
    {
        for(int s=0; s < (sizeof(scenario)/sizeof(scenario[0])) ;s++)
        {
            if (scenario[s].time == i)
            {
                *scenario[s].condition = scenario[s].cond_value;
                std::cout << "time: " << i << " " << scenario[s].cond_name  << " -> " << (scenario[s].cond_value?"true":"false") << std::endl;
            }
        }
        cyclic_AmbLS_EventGenerator();
    }

    std::cout << "=== Hello AmbLS: stop scenario ===" << std::endl;

    return 0;
}
