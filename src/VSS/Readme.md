# VSS integration
In this directory we provide an example how to integrate [VSS](https://github.com/COVESA/vehicle_signal_specification) with Ambient Light Services.

The according headers [vss_spec.h](/inc/VSS/vss_spec.h) and [vss_macro.h](/inc/VSS/vss_macro.h) are generated from VSS specification.

The Class SignalConnector shows, as an example, how to connect VSS signals to the BaseRuntime struct in the library.