/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file SignalConnector.cpp
 *  @brief Implementation of the class SignalConnector
 *  @date 2022-11-25
 */

#include "vss_spec.h"
#include "vss_macro.h"
#include "SignalConnector.h"


SignalConnector::SignalConnector(BaseRuntime* runtime):
    pRuntime(runtime),
    connected(false),
    driverPosition(driverPosLeft),
    doorCount(4)
{
}

SignalConnector::~SignalConnector()
{
}

boolean SignalConnector::connectSignals()
{
    if(nullptr != pRuntime)
    {
        if(!connected)
        {
            //evaluate driver position
            driverPosition = *(static_cast<uint8*>(VSS_Cabin_DriverPosition()->user_data));
            doorCount = *(static_cast<uint8*>(VSS_Cabin_DoorCount()->user_data));
            if(driverPosLeft == driverPosition)
            {
                pRuntime->DriverSideFrontDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row1_Left_IsOpen()->user_data));
                pRuntime->PassengerSideFrontDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row1_Right_IsOpen()->user_data));
                if(4 <= doorCount)
                {
                    pRuntime->DriverSideRearDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row2_Left_IsOpen()->user_data));
                    pRuntime->PassengerSideRearDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row2_Right_IsOpen()->user_data));
                }
                
            }
            else
            {
                pRuntime->DriverSideFrontDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row1_Right_IsOpen()->user_data));
                pRuntime->PassengerSideFrontDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row1_Left_IsOpen()->user_data));
                if(4 <= doorCount)
                {
                    pRuntime->DriverSideRearDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row2_Right_IsOpen()->user_data));
                    pRuntime->PassengerSideRearDoorOpen = *(static_cast<boolean*>(VSS_Cabin_Door_Row2_Left_IsOpen()->user_data));
                }
            }
            switch(driverPosition)
            {
                case 3:
                {
                    pRuntime->DriverSeatOccupancy = *(static_cast<boolean*>(VSS_Cabin_Seat_Row1_Pos3_IsOccupied()->user_data));
                    pRuntime->DriverBeltLock = *(static_cast<boolean*>(VSS_Cabin_Seat_Row1_Pos3_IsBelted()->user_data));
                }
                break;
                case 2:
                {
                    pRuntime->DriverSeatOccupancy = *(static_cast<boolean*>(VSS_Cabin_Seat_Row1_Pos2_IsOccupied()->user_data));
                    pRuntime->DriverBeltLock = *(static_cast<boolean*>(VSS_Cabin_Seat_Row1_Pos2_IsBelted()->user_data));
                }
                break;
                case 1: // fallthrough
                default:
                {
                    pRuntime->DriverSeatOccupancy = *(static_cast<boolean*>(VSS_Cabin_Seat_Row1_Pos1_IsOccupied()->user_data));
                    pRuntime->DriverBeltLock = *(static_cast<boolean*>(VSS_Cabin_Seat_Row1_Pos1_IsBelted()->user_data));
                }
                break;
            }
            connected = true;
        }
    }
    return connected;
}