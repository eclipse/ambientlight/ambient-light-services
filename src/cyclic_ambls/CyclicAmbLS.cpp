/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
 */

/**
 *  @file CyclicAmbLS.cpp
 *  @brief Implementation of 10 ms cyclic runnable of the SWC AmbLS 
 *  @date 2019-11-05
 */


#include "EventGenerator.h"
#include "VersionInfo.h"

#include "VehicleState.h"
#include "Dispatcher.h"
#include "Output.h"


// ------------------------------ Global variables ------------------------------
// Runtime object
BaseRuntime _runtime;
BaseRuntime *runtime = &_runtime;
// Vehicle state object
VehicleState vehicleState;
// Dispatcher object
Dispatcher dispatcher;
// Output  object
Output output(&vehicleState);
// ------------------------------------------------------------------------------


/**
 *  @brief runnable cyclic_AmbLS_EventGenerator
 *  It is called every 10 ms.
 */
extern "C" void cyclic_AmbLS_EventGenerator(void)
{
    if ( !vehicleState.initialized )
    {
        vehicleState.init();
        output.init();
    }
    vehicleState.cycle();
    output.cycle();
}

