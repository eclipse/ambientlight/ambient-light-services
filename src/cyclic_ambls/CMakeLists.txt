#  ========================================================================
#  Ambient Light Services
# 
#  Copyright (C) 2019 - 2022 T-Systems International GmbH
# 
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#  
#       http://www.apache.org/licenses/LICENSE-2.0
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# 
#  SPDX-License-Identifier: Apache-2.0
# 
#  ========================================================================

cmake_minimum_required(VERSION 3.10)
project(${SWC})

set(SOURCES
    CyclicAmbLS.cpp
)

# Create includes library
set(INCLUDES_LIB inc_${SWC})
add_library(${INCLUDES_LIB} INTERFACE)
target_include_directories(${INCLUDES_LIB} INTERFACE
    ${PROJECT_SOURCE_DIR}
)

# Create an object library from existing sources
add_library(${PROJECT_NAME} OBJECT ${SOURCES})
target_link_libraries(${PROJECT_NAME} PUBLIC
    inc_common
    inc_vss
    inc_vehicle_state
    inc_dispatcher
    inc_output
)
target_link_libraries(${PROJECT_NAME} PRIVATE ${INCLUDES_LIB})