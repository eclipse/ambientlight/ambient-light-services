/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
*/

/**
 *  @file BaseRuntime.h
 *  @brief Contains runtime interface definitions
 *  @date 2019-11-05
 */

#ifndef BASE_RUNTIME_INCLUDED
#define BASE_RUNTIME_INCLUDED

#include "ParamOffsets.h"
#include "EventGenerator.h"

/**
 * @brief This class is used as an intermediate layer to access runtime data
 * @todo Should provide access to parameters via const pointers to const data
 * 
 */
struct BaseRuntime
{
    BaseRuntime()
        : CentralLockingCloseBlinkingActive(false), CentralLockingOpenBlinkingActive(false),
          WarningLightsActive(false), CentralLockingOpenPFD(false),
          CentralLockingOpenDFD(false), CentralLockingOpenHdVirtualPedal(0), 
          CentralLockingOpenRadio(false), CHActiveTime(0), CHState(0), LHActiveTime(0), LHState(0),
          ManualActivationCH(0), SignatureLightSetup(0), PassengerSideFrontDoorOpen(false), DriverSideFrontDoorOpen(false), PassengerSideRearDoorOpen(false),
          RearSideTrunkDoorOpen(false), DriverSideRearDoorOpen(false), DriverBeltLock(false), DriverPresenceStatus(0),
          DriverSeatOccupancy(false), EventsScene{45, 60, 135, 60}, LightSensorOnChoLho(false), StLdsAssistantDrivingLight(false),
          StLdsDimmedHeadlight(false), StLssRequestHeadlightFlasherPlOff(false), BEMSwitchOffLevel(0),
          SleepRequest(false), Terminal15(false), CustomerRequestFfbClose(0), CustomerRequestFfbHdfSingleUnlock(0),
          AFSAmbLSRefStatus(0), MFInteriorLightRejectionCan(0), PerAmbLSCntr(0),
          surroundAmbLSPrio(0), ambLSAFSStagingState(0), ambLSModificationReason(0),
          ambLSModificationState(false), ambLSPerCntr(0), afsPosHorizontalR(0), afsPosHorizontalL(0), afsPosVerticalR(0),
          afsPosVerticalL(0), animationProfile(0), emblemBrightness(0), slatBrightness(0), tailLightBrightness(0), tailLightState(0),
          markerLightState(false), headlightsState(false), licensePlateLightState(false), signature1State(false), signature2State(false),
          ActivateCurves(0), BlinkDelay(0), ChoExitTime(0), LhoDynamicProtection(0), LhoRelease(0), SwcSignalDelayTime(0)
    {
    }
    // Input values
    boolean CentralLockingCloseBlinkingActive;
    boolean CentralLockingOpenBlinkingActive;
    boolean WarningLightsActive;
    boolean CentralLockingOpenPFD;
    boolean CentralLockingOpenDFD;
    boolean CentralLockingOpenHdVirtualPedal;
    boolean CentralLockingOpenRadio;
    uint8 CHActiveTime;
    uint8 CHState;
    uint8 LHActiveTime;
    uint8 LHState;
    boolean ManualActivationCH;
    uint8 SignatureLightSetup;
    boolean PassengerSideFrontDoorOpen;
    boolean DriverSideFrontDoorOpen;
    boolean PassengerSideRearDoorOpen;
    boolean RearSideTrunkDoorOpen;
    boolean DriverSideRearDoorOpen;
    boolean DriverBeltLock;
    uint8 DriverPresenceStatus;
    boolean DriverSeatOccupancy;
    boolean LightSensorOnChoLho;
    boolean StLdsAssistantDrivingLight;
    boolean StLdsDimmedHeadlight;
    boolean StLssRequestHeadlightFlasherPlOff;
    uint8 BEMSwitchOffLevel;
    boolean SleepRequest;
    boolean Terminal15;
    boolean CustomerRequestFfbClose;
    boolean CustomerRequestFfbHdfSingleUnlock;
    uint8 AFSAmbLSRefStatus;
    boolean MFInteriorLightRejectionCan;
    uint8 PerAmbLSCntr;
    /* DoorCtrl data */
    boolean timeBaseVL[4];
    boolean timeBaseVR[4];
    boolean timeBaseHL[4];
    boolean timeBaseHR[4];
    uint8 timeBaseFactorVL[4];
    uint8 timeBaseFactorVR[4];
    uint8 timeBaseFactorHL[4];
    uint8 timeBaseFactorHR[4];
    uint8 targetBrightnessVL[4];
    uint8 targetBrightnessVR[4];
    uint8 targetBrightnessHL[4];
    uint8 targetBrightnessHR[4];
    boolean reqAmbLighting;
    // Output values
    uint8 surroundAmbLSPrio;
    uint8 ambLSAFSStagingState;
    uint8 ambLSModificationReason;
    boolean ambLSModificationState;
    uint8 ambLSPerCntr;
    uint16 afsPosHorizontalR;
    uint16 afsPosHorizontalL;
    uint16 afsPosVerticalR;
    uint16 afsPosVerticalL;
    uint8 animationProfile;
    uint8 emblemBrightness;
    uint8 slatBrightness;
    uint8 tailLightBrightness;
    uint8 tailLightState;
    boolean markerLightState;
    boolean headlightsState;
    boolean licensePlateLightState;
    boolean signature1State;
    boolean signature2State;
    // Parameter values
    uint8 ActivateCurves;
    uint8 BlinkDelay;
    uint8 ChoExitTime;
    uint8 LhoDynamicProtection;
    uint8 LhoRelease;
    uint8 SwcSignalDelayTime;
    uint8 EventsScene[4];
    uint16 dimmerTimeSALAmb;
    uint16 doorCtrlPinMapping;
    uint16 doorCtrlPinMappingAmbLighting;
    uint8 doorCtrlTimeBase0;
    uint8 doorCtrlTimeBase1;

    /**
     * @brief The start address of the data set memory area
     * 
     */
    uint8* dataSet;

    /**
     * @brief Sets a new address to the data set memory area
     * 
     * @param address 
     */
    void setDataSetAddress(uint8* startAddress) { dataSet = startAddress;}
       /**
     * @brief Reads an upper bound for the light compensation delay
     * @return Maximum delay value (p_Delay_Max)
     * 
     */
    inline uint8 getMaxTransportDelay() {return *(dataSet + ParamOffset::DelayMax); }
    /**
     * @brief Reads total amount of scenes available in the system 
     * @return Amount of available scenes (p_Num_Scenes)
     */
    inline uint8 getTotalScenesAmount() { return *(dataSet + ParamOffset::NumScenes); }
    /**
     * @brief Reads total amount of animations available in the system
     * @return Amount of available animations (p_Num_Animations)
     * 
     */
    inline uint8 getTotalAnimationsAmount() { return *(dataSet + ParamOffset::NumAnimations); }
    /**
     * @brief Reads total amount of luminaries available in the system
     * @return Amount of available luminaries (p_Num_Luminaries)
     * 
     */
    inline uint8 getTotalActuatorsAmount() { return *(dataSet + ParamOffset::NumLuminaries); }
    /**
     * @brief Reads total amount of colors available in the system
     * @return Amount of available colors (p_Num_Colors)
     * 
     */
    inline uint8 getTotalColorsAmount() { return *(dataSet + ParamOffset::NumColors); }
    /**
     * @brief Reads total amount of positions available in the system
     * @return Amount of available positions (p_Num_Positions)
     */
    inline uint8 getTotalPositionsAmount(){ return *(dataSet + ParamOffset::NumPositions); }
    /**
     * @brief Reads a brightness turn-on threshold. If a current brightness 
     * value is greater or equal to the read value the light is considered 
     * as turned on.
     * @return A brightness threshold(p_Dimm2Bin)
     * 
     */
    inline uint8 getLightTurnOnThreshold() {return *(dataSet + ParamOffset::Dimm2Bin);}
    /**
     * @brief Reads lists of available scenes for events 0 and 1
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv0Ev1(){ return dataSet + ParamOffset::EventsScene; }
    /**
     * @brief Reads lists of available scenes for events 2 and 3
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv2Ev3(){  return dataSet + ParamOffset::EventsScene + 8; }    
    /**
     * @brief Reads lists of available scenes for events 4 and 5
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv4Ev5(){  return dataSet + ParamOffset::EventsScene + 8 * 2; }    
    /**
     * @brief Reads lists of available scenes for events 6 and 7
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv6Ev7(){ return dataSet + ParamOffset::EventsScene + 8 * 3;}    
    /**
     * @brief Reads lists of available scenes for events 8 and 9
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv8Ev9(){ return dataSet + ParamOffset::EventsScene + 8 * 4; }    
    /**
     * @brief Reads lists of available scenes for events 10 and 11
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv10Ev11(){ return dataSet + ParamOffset::EventsScene + 8 * 5; }    
    /**
     * @brief Reads lists of available scenes for events 12 and 13
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv12Ev13(){ return dataSet + ParamOffset::EventsScene + 8 * 6; }    
    /**
     * @brief Reads lists of available scenes for events 14 and 15
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv14Ev15(){ return dataSet + ParamOffset::EventsScene + 8 * 7; }
    /**
     * @brief Reads lists of available scenes for events 16 and 17
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv16Ev17(){ return dataSet + ParamOffset::EventsScene + 8 * 8; }
    /**
     * @brief Reads lists of available scenes for events 18 and 19
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv18Ev19(){ return dataSet + ParamOffset::EventsScene + 8 * 9; }
    /**
     * @brief Reads lists of available scenes for events 20 and 21
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv20Ev21(){ return dataSet + ParamOffset::EventsScene + 8 * 10; }
    /**
     * @brief Reads lists of available scenes for events 22 and 23
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv22Ev23(){ return dataSet + ParamOffset::EventsScene + 8 * 11; }
    /**
     * @brief Reads lists of available scenes for events 24 and 25
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv24Ev25(){ return dataSet + ParamOffset::EventsScene + 8 * 12; }
    /**
     * @brief Reads lists of available scenes for events 26 and 27
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv26Ev27(){ return dataSet + ParamOffset::EventsScene + 8 * 13; }
    /**
     * @brief Reads lists of available scenes for events 31
     * @return Pointer to the list of scenes (p_Events_Scene)
     * 
     */
    inline const uint8* getSceneListEv31(){ return dataSet + ParamOffset::EventsScene + 8 * 15; }
    /**
     * @brief Reads the list of available groups of scenes
     * @return Pointer to the list of groups (p_Szn_Group)
     * @todo Brief group set layout
     */
    inline const uint8* getSceneGroups(){ return dataSet + ParamOffset::SznGroup; }
   /**
     * @brief Reads amounts of steps (couples of animation and delay) for each scene
     * @return Pointer to the list of amounts of steps (p_Szn_Steps)
     */
    inline const uint8* getStepAmounts(){ return dataSet + ParamOffset::SznSteps; }
    /**
     * @brief Reads indexes of animations for each scene
     * @return Pointer to the list of indexes (p_Szn_Animation)
     */
	inline const uint8* getAnimationIndexes() { return dataSet + ParamOffset::SznAnimation; }
    /**
     * @brief Reads delays for each animation
     * @return Pointer to the list of animation delays (p_Szn_t)
     */
    inline const uint8* getAnimationDelays(){ return dataSet + ParamOffset::SznT; }
    /**
     * @brief Reads durations of each animation
     * @return Pointer to the list of durations of animations (p_Ani_t)
     */
    inline const uint16* getAnimationDurations(){ return reinterpret_cast <uint16*> (dataSet + ParamOffset::AniT); }
    /**
     * @brief Reads indexes of animation target colors
     * @return Pointer to the list of target colors (p_Ani_Target_Color)
     */
    inline const uint8* getAnimationTargetColors(){ return dataSet + ParamOffset::AniTargetColor; }
    /**
     * @brief Reads a set of dimming time values 
     * @return Pointer to the list of time values (p_Ani_Target_t)
     */
	inline const uint16* getAnimationDimmingTimes() { return reinterpret_cast <uint16*>(dataSet + ParamOffset::AniTargetT); }
    /**
     * @brief Reads indexes of start positions for each animation
     * @return Pointer to the list of start positions (p_Ani_PosStart)
     */
    inline const uint8* getStartPositions(){ return dataSet + ParamOffset::AniPosStart; }
    /**
     * @brief Reads indexes of end positions for each animation
     * @return Pointer to the list of end positions (p_Ani_PosEnd)
     */
    inline const uint8* getEndPositions(){ return dataSet + ParamOffset::AniPosEnd; }
    /**
     * @brief Reads rotation directions for each animation
     * 
     * Positive direction equals to 0, negative to 1
     * @return Pointer to the list of rotation directions (p_Ani_RotDir)
     */
    inline const uint8* getRotationDirections(){ return dataSet + ParamOffset::AniRotDir; }
    /**
     * @brief Reads animation types
     * 
     * Available types:
     * - Global (0)
     * - Circle Ring (1)
     * - Circle Segment (2)
     * - Dynamic Circle Ring (3)
     * - Dynamic Circle Segment (4)
     * - Point (5)
     * @return Pointer to the list of animation types (p_Ani_Typ) 
     */
    inline const uint8* getAnimationTypes(){ return dataSet + ParamOffset::AniType; }
    /**
     * @brief Reads position angles
     * @return Pointer to the list of angles (p_Pos_Angle)
     */
    inline const uint16 *getPosAngles(){ return reinterpret_cast <uint16*> (dataSet + ParamOffset::PosAngle); }
    /**
     * @brief Reads radius vector lengths
     * @return Pointer to the list of lengths (p_Pos_Radius)
     */
    inline const uint16* getPosRadiusLengths(){ return reinterpret_cast <uint16*> (dataSet + ParamOffset::PosRadius); }
    /**
     * @brief Reads indexes of actuators' positions
     * @return Pointer to the list of indexes (p_Light_Pos)
     */
    inline const uint8* getActuatorPositions(){ return dataSet + ParamOffset::LightPos; }
    /**
     * @brief Reads delays for each actuator
     * @return Pointer to the list of delays (p_Light_Delay)
     */
    inline const uint8* getActuatorDelays(){ return dataSet + ParamOffset::LightDelay; }
    /**
     * @brief Reads actuators' types
     * 
     * The following types exist:
     * - Data out
     * - Data reference
     * - Actual value
     * - Target value
     * @return Pointer to the list of types (p_Light_OffType)
     */
    inline const uint8* getActuatorTypes(){ return dataSet + ParamOffset::LightOffType; }
    /**
     * @brief Reads types of an actuator endpoint
     * @return Pointer to the list of drivers (p_Light_Driver)
     * 
     */
    inline const uint8* getActuatorDrivers(){ return dataSet + ParamOffset::LightDriver;}
    /**
     * @brief Reads dedicated color spaces for all available lights
     * @return Pointer to the list of color spaces (p_Light_ColorType)
     */
    inline const uint8* getActuatorColorTypes() { return dataSet + ParamOffset::LightColorType;}
    /**
     * @brief Reads an acuator addresses
     * @return Pointer to the list of addresses (p_Light_Address)
     */
    inline const uint8* getActuatorAddresses(){ return dataSet + ParamOffset::LightAddress;}
    /** 
     * @brief Reads lights' group inclusion statuses
     * @return Pointer to the list of inclusion statuses (p_Light_Max)
     */
    inline const uint8* getActuatorGroupInclusion(){ return dataSet + ParamOffset::LightMax;}
   /**
     * @brief Reads color hues 
     * @return Pointer to the list of color hues in HSV color space (p_Color_Hue)
     */
    inline const uint16* getColorHues(){ return reinterpret_cast <uint16*> (dataSet + ParamOffset::ColorHue); }
    /**
     * @brief Reads color saturations 
     * @return Pointer to the list of color saturations in HSV color space (p_Color_Sat)
     */
    inline const uint8* getColorSaturations(){ return dataSet + ParamOffset::ColorSat; }
    /**
     * @brief Reads color values (brightnesses)
     * @return Pointer to the list of color values in HSV color space (p_Color_Val)
     */
    inline const uint8* getColorValues(){ return dataSet + ParamOffset::ColorVal; }

    /* Public read interfaces */
    boolean getCentralLockingCloseBlinkingActive() { return CentralLockingCloseBlinkingActive; }
    boolean getCentralLockingOpenBlinkingActive() { return CentralLockingOpenBlinkingActive; }
    boolean getWarningLightsActive() { return WarningLightsActive; }
    boolean getCentralLockingOpenPFD() { return CentralLockingOpenPFD; }
    boolean getCentralLockingOpenDFD() { return CentralLockingOpenDFD; }
    boolean getCentralLockingOpenHdVirtualPedal() { return CentralLockingOpenHdVirtualPedal; }
    boolean getCentralLockingOpenRadio() { return CentralLockingOpenRadio; }
    uint8 getCHActiveTime() { return CHActiveTime; }
    uint8 getCHState() { return CHState; }
    uint8 getLHActiveTime() { return LHActiveTime; }
    uint8 getLHState() { return LHState; }
    boolean getManualActivationCH() { return ManualActivationCH; }
    uint8 getSignatureLightSetup() { return SignatureLightSetup; }
    boolean getPassengerSideFrontDoorOpen() { return PassengerSideFrontDoorOpen; }
    boolean getDriverSideFrontDoorOpen() { return DriverSideFrontDoorOpen; }
    boolean getPassengerSideRearDoorOpen() { return PassengerSideRearDoorOpen; }
    boolean getRearSideTrunkDoorOpen() { return RearSideTrunkDoorOpen; }
    boolean getDriverSideRearDoorOpen() { return DriverSideRearDoorOpen; }
    boolean getDriverBeltLock() { return DriverBeltLock; }
    uint8 getDriverPresenceStatus() { return DriverPresenceStatus; }
    boolean getDriverSeatOccupancy() { return DriverSeatOccupancy; }
    /**
     * @brief Reads sectors' descriptors
     * @return pointer to an array:
     * - zero element - front sector upper bound
     * - 1nd element - front sector radius
     * - 2d element - rear sector bottom bound
     * - 3th element - rear sector radius
     */
    const uint8* getEventsScene() { return EventsScene; }
    boolean getLightSensorOnChoLho() { return LightSensorOnChoLho; }
    boolean getStLdsAssistantDrivingLight() { return StLdsAssistantDrivingLight; }
    boolean getStLdsDimmedHeadlight() { return StLdsDimmedHeadlight; }
    boolean getStLssRequestHeadlightFlasherPlOff() { return StLssRequestHeadlightFlasherPlOff; }
    uint8 getBEMSwitchOffLevel() { return BEMSwitchOffLevel; }
    boolean getSleepRequest() { return SleepRequest; }
    boolean getTerminal15() { return Terminal15; }
    boolean getCustomerRequestFfbClose() { return CustomerRequestFfbClose; }
    boolean getCustomerRequestFfbHdfSingleUnlock() { return CustomerRequestFfbHdfSingleUnlock; }
    uint8 getAFSAmbLSRefStatus() { return AFSAmbLSRefStatus; }
    boolean getMFInteriorLightRejectionCan() { return MFInteriorLightRejectionCan; }
    uint8 getPerAmbLSCntr() { return PerAmbLSCntr; }
    inline const boolean* getTimeBaseVL() { return timeBaseVL; }
    inline const boolean* getTimeBaseVR() { return timeBaseVR; }
    inline const boolean* getTimeBaseHL() { return timeBaseHL; }
    inline const boolean* getTimeBaseHR() { return timeBaseHR; }
    inline const uint8* getTimeBaseFactorVL () { return timeBaseFactorVL; }
    inline const uint8* getTimeBaseFactorVR () { return timeBaseFactorVR; }
    inline const uint8* getTimeBaseFactorHL () { return timeBaseFactorHL; }
    inline const uint8* getTimeBaseFactorHR () { return timeBaseFactorHR; }
    inline const uint8* getTargetBrightnessVL () { return targetBrightnessVL; }   
    inline const uint8* getTargetBrightnessVR () { return targetBrightnessVR; }  
    inline const uint8* getTargetBrightnessHL () { return targetBrightnessHL; }
    inline const uint8* getTargetBrightnessHR () { return targetBrightnessHR; }
    inline boolean getReqAmbLighting() { return reqAmbLighting; }
    
    /* Access parameters */
    uint8 getActivateCurves(){return ActivateCurves;}
    uint8 getBlinkDelay() {return BlinkDelay;}
    uint8 getChoExitTime() {return ChoExitTime;}
    uint8 getLhoDynamicProtection() {return LhoDynamicProtection;}
    uint8 getLhoRelease() { return LhoRelease;}
    uint8 getSwcSignalDelayTime(){return SwcSignalDelayTime;}
    inline uint16 getDimmerTimeSALAmb() { return dimmerTimeSALAmb; }
    inline uint16 getDoorCtrlPinMapping() { return doorCtrlPinMapping; }
    inline uint16 getDoorCtrlPinMappingAmbLighting() { return doorCtrlPinMappingAmbLighting; }
    inline uint8 getDoorCtrlTimeBase0() { return doorCtrlTimeBase0; }
    inline uint8 getDoorCtrlTimeBase1() { return doorCtrlTimeBase1; }

    /* Public write interfaces */
    void setSurroundAmbLSPrio(uint8 val) { surroundAmbLSPrio = val; }
    void setAmbLSAFSStagingState(uint8 val) { ambLSAFSStagingState = val; }
    void setAmbLSModificationReason(uint8 val) { ambLSModificationReason = val; }
    void setAmbLSModificationState(boolean val) { ambLSModificationState = val; }
    void setAmbLSPerCntr(uint8 val) { ambLSPerCntr = val; }
    inline void setTimeBaseVL(boolean* timeBase) 
    {
        timeBaseVL[0] = timeBase[0];
        timeBaseVL[1] = timeBase[1];
        timeBaseVL[2] = timeBase[2];
        timeBaseVL[3] = timeBase[3];
    }
    inline void setTimeBaseVR(boolean_DIM4 timeBase)
    {
        timeBaseVR[0] = timeBase[0];
        timeBaseVR[1] = timeBase[1];
        timeBaseVR[2] = timeBase[2];
        timeBaseVR[3] = timeBase[3];
    }
    inline void setTimeBaseHL(boolean_DIM4 timeBase)
    {
        timeBaseHL[0] = timeBase[0];
        timeBaseHL[1] = timeBase[1];
        timeBaseHL[2] = timeBase[2];
        timeBaseHL[3] = timeBase[3];
    }
    inline void setTimeBaseHR(boolean_DIM4 timeBase)
    {
        timeBaseHR[0] = timeBase[0];
        timeBaseHR[1] = timeBase[1];
        timeBaseHR[2] = timeBase[2];
        timeBaseHR[3] = timeBase[3];
    }
    inline void setTimeBaseFactorVL(uint8_DIM4 timeBaseFactor)
    {
        timeBaseFactorVL[0] = timeBaseFactor[0];
        timeBaseFactorVL[1] = timeBaseFactor[1];
        timeBaseFactorVL[2] = timeBaseFactor[2];
        timeBaseFactorVL[3] = timeBaseFactor[3];
    }
    inline void setTimeBaseFactorVR(uint8_DIM4 timeBaseFactor)
    {
        timeBaseFactorVR[0] = timeBaseFactor[0];
        timeBaseFactorVR[1] = timeBaseFactor[1];
        timeBaseFactorVR[2] = timeBaseFactor[2];
        timeBaseFactorVR[3] = timeBaseFactor[3];
    }
    inline void setTimeBaseFactorHL(uint8_DIM4 timeBaseFactor)
    {
        timeBaseFactorHL[0] = timeBaseFactor[0];
        timeBaseFactorHL[1] = timeBaseFactor[1];
        timeBaseFactorHL[2] = timeBaseFactor[2];
        timeBaseFactorHL[3] = timeBaseFactor[3];
    }
    inline void setTimeBaseFactorHR(uint8_DIM4 timeBaseFactor)
    {
        timeBaseFactorHR[0] = timeBaseFactor[0];
        timeBaseFactorHR[1] = timeBaseFactor[1];
        timeBaseFactorHR[2] = timeBaseFactor[2];
        timeBaseFactorHR[3] = timeBaseFactor[3];
    }
    inline void setTargetBrightnessVL(uint8_DIM4 targetBrightness)
    {
        targetBrightnessVL[0] = targetBrightness[0];
        targetBrightnessVL[1] = targetBrightness[1];
        targetBrightnessVL[2] = targetBrightness[2];
        targetBrightnessVL[3] = targetBrightness[3];
    }
    inline void setTargetBrightnessVR(uint8_DIM4 targetBrightness)
    {
        targetBrightnessVR[0] = targetBrightness[0];
        targetBrightnessVR[1] = targetBrightness[1];
        targetBrightnessVR[2] = targetBrightness[2];
        targetBrightnessVR[3] = targetBrightness[3];
    }
    inline void setTargetBrightnessHL(uint8_DIM4 targetBrightness)
    {
        targetBrightnessHL[0] = targetBrightness[0];
        targetBrightnessHL[1] = targetBrightness[1];
        targetBrightnessHL[2] = targetBrightness[2];
        targetBrightnessHL[3] = targetBrightness[3];
    }
    void setTargetBrightnessHR(uint8_DIM4 targetBrightness) 
    { 
        targetBrightnessHR[0] = targetBrightness[0];
        targetBrightnessHR[1] = targetBrightness[1];
        targetBrightnessHR[2] = targetBrightness[2];
        targetBrightnessHR[3] = targetBrightness[3];
    }
    inline boolean* getTimeBaseVLWriteRef() { return timeBaseVL; }    
    inline boolean* getTimeBaseVRWriteRef() { return timeBaseVR; }
    inline boolean* getTimeBaseHLWriteRef() { return timeBaseHL; }
    inline boolean* getTimeBaseHRWriteRef() { return timeBaseHR; }
    inline uint8* getTimeBaseFactorVLWriteRef() { return timeBaseFactorVL; }
    inline uint8* getTimeBaseFactorVRWriteRef() { return timeBaseFactorVR; }
    inline uint8* getTimeBaseFactorHLWriteRef() { return timeBaseFactorHL; }
    inline uint8* getTimeBaseFactorHRWriteRef() { return timeBaseFactorHR; }
    inline uint8* getTargetBrightnessVLWriteRef() { return targetBrightnessVL; }
    inline uint8* getTargetBrightnessVRWriteRef() { return targetBrightnessVR; }
    inline uint8* getTargetBrightnessHLWriteRef() { return targetBrightnessHL; }
    inline uint8* getTargetBrightnessHRWriteRef() { return targetBrightnessHR; }
    inline void setAfsPosHorizontalR(uint16 val) {afsPosHorizontalR = val;}
    inline uint16* getAfsPosHorizontalRWriteRef() {return &afsPosHorizontalR;}
    inline void setAfsPosHorizontalL(uint16 val) {afsPosHorizontalL = val;}
    inline uint16* getAfsPosHorizontalLWriteRef() {return &afsPosHorizontalL;}
    inline void setAfsPosVerticalR(uint16 val) {afsPosVerticalR = val;}
    inline uint16* getAfsPosVerticalRWriteRef() {return &afsPosVerticalR;}
    inline void setAfsPosVerticalL(uint16 val) {afsPosHorizontalL = val;}
    inline uint16* getAfsPosVerticalLWriteRef() {return &afsPosHorizontalL;}
    inline void setAnimationProfile(uint8 val) {animationProfile = val; }
    inline uint8* getAnimationProfileWriteRef() {return &animationProfile;}
    inline void setEmblemBrightness(uint8 val) {emblemBrightness = val;}
    inline void setSlatBrightness(uint8 val) {slatBrightness = val;}
    inline void setTailLightBrightness(uint8 val) {tailLightBrightness = val;}
    inline void setTailLightState(boolean val) {tailLightState = val;}
    inline void setMarkerLightState(boolean val) {markerLightState = val;}
    inline void setHeadlightsState(boolean val) {headlightsState = val;}
    inline void setLicensePlateLightState(boolean val) {licensePlateLightState = val;}
    inline void setSignature1State(boolean val) {signature1State = val;}
    inline void setSignature2State(boolean val) {signature2State = val;}

};
#endif // BASE_RUNTIME_INCLUDED
