/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
*/

/**
 *  @file PlatformTypes.h
 *  @brief Contains definitions of platform specific standard types
 *  @date 2022-10-25
 */

#if (!defined PLATFORM_TYPES_INCLUDED)
#define PLATFORM_TYPES_INCLUDED

#if (!defined TRUE)
/** \brief true value for boolean type */
#define TRUE 1U
#endif

#if (!defined FALSE)
/** \brief false value for boolean type */
#define FALSE 0U
#endif

/* redefinition check may lead to an incorrect condition that
 * TRUE and FALSE are equal */
#if (TRUE == FALSE)
#error TRUE == FALSE is incorrect
#endif

/** \brief Type definition of standard type boolean
 */
typedef unsigned char boolean;

/** \brief Type definition of standard type sint8
 */
typedef signed char sint8;

/** \brief Type definition of standard type sint16
 */
typedef signed short sint16;

/** \brief Type definition of standard type sint32
 */
typedef signed long sint32;

/** \brief Type definition of standard type uint8
 */
typedef unsigned char uint8;

/** \brief Type definition of standard type uint16
 */
typedef unsigned short uint16;

/** \brief Type definition of standard type uint32
 */
typedef unsigned long uint32;

/** \brief Type definition of standard type sint8_least
 */
typedef signed long sint8_least;

/** \brief Type definition of standard type sint16_least
 */
typedef signed long sint16_least;

/** \brief Type definition of standard type sint32_least
 */
typedef signed long sint32_least;

/** \brief Type definition of standard type uint8_least
 */
typedef unsigned long uint8_least;

/** \brief Type definition of standard type uint16_least
 */
typedef unsigned long uint16_least;

/** \brief Type definition of standard type uint32_least
 */
typedef unsigned long uint32_least;

/** \brief Type definition of standard type float32
 */
typedef float float32;

/** \brief Type definition of standard type float64
 */
typedef double float64;

/** \brief Type definition of standard type sint64
 */
typedef signed long long sint64;

/** \brief Type definition of standard type uint64
 */
typedef unsigned long long uint64;

#endif // PLATFORM_TYPES_INCLUDED