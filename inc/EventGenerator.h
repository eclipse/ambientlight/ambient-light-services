/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
*/

/**
 *  @file EventGenerator.h
 *  @brief This file contains AmbLS specific definitions of types 
 *  and constants.
 *  @date 2022-10-25
 */

#if (!defined EVENTGENERATOR_INCLUDED)
#define EVENTGENERATOR_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "PlatformTypes.h"


/* ==================[Types]================================================== */

#ifndef AMBLS_TYPE_boolean_DIM4
#define AMBLS_TYPE_boolean_DIM4
typedef boolean boolean_DIM4[4];
#endif

#ifndef AMBLS_TYPE_uint8_DIM4
#define AMBLS_TYPE_uint8_DIM4
typedef uint8 uint8_DIM4[4];
#endif

/* ==================[Constants]============================================== */

#ifndef ANIMATIONSTYP_GLOBAL
#define ANIMATIONSTYP_GLOBAL 0U
#endif

#ifndef ANIMATIONSTYP_KREISRING
#define ANIMATIONSTYP_KREISRING 1U
#endif

#ifndef ANIMATIONSTYP_KREISSEGMENT
#define ANIMATIONSTYP_KREISSEGMENT 2U
#endif

#ifndef ANIMATIONSTYP_KREISRING_DYNAMISCH
#define ANIMATIONSTYP_KREISRING_DYNAMISCH 3U
#endif

#ifndef ANIMATIONSTYP_KREISSEGMENT_DYNAMISCH
#define ANIMATIONSTYP_KREISSEGMENT_DYNAMISCH 4U
#endif

#ifndef ANIMATIONSTYP_PUNKT
#define ANIMATIONSTYP_PUNKT 5U
#endif

#ifndef enDriverPresenceStatus_Init
#define enDriverPresenceStatus_Init 0U
#endif

#ifndef enDriverPresenceStatus_Absent
#define enDriverPresenceStatus_Absent 2U
#endif

#ifndef enDriverPresenceStatus_Entry
#define enDriverPresenceStatus_Entry 4U
#endif

#ifndef enDriverPresenceStatus_Present
#define enDriverPresenceStatus_Present 6U
#endif

#ifndef enDriverPresenceStatus_PresentVerified
#define enDriverPresenceStatus_PresentVerified 8U
#endif

#ifndef enDriverPresenceStatus_ExitIntent
#define enDriverPresenceStatus_ExitIntent 11U
#endif

#ifndef enDriverPresenceStatus_Exit
#define enDriverPresenceStatus_Exit 13U
#endif

#ifndef enDriverPresenceStatus_Error
#define enDriverPresenceStatus_Error 15U
#endif

#define AMBLS_RUNNABLE_CYCLE 10u  
#define AMBLS_DATASET_TIME_RESOLUTION 10u  

void cyclic_AmbLS_EventGenerator(void);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif // EVENTGENERATOR_INCLUDED