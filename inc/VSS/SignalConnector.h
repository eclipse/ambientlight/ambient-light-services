/* ========================================================================
 * Ambient Light Services
 *
 * Copyright (C) 2019 - 2022 T-Systems International GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * ========================================================================
*/

/**
 *  @file SignalConnector.h
 *  @brief Contains class definition for a VSS to AmbLS connector
 *  @date 2022-11-25
 */

#ifndef SIGNAL_CONNECTOR_INCLUDED
#define SIGNAL_CONNECTOR_INCLUDED


#include <BaseRuntime.h>
#include <PlatformTypes.h>

class SignalConnector
{
public:
    enum DriverPosition
    {
        driverPosLeft = 1u,
        driverPosRight
    };

    /**
     * @brief Constructs a new SignalConnector object
     *        create only one instance of this class
     */
    explicit SignalConnector(BaseRuntime* runtime);

    virtual ~SignalConnector();

    boolean connectSignals();

private:
    SignalConnector() = delete;
    SignalConnector(const SignalConnector&) = delete;
    SignalConnector& operator=(const SignalConnector&) = delete;

    BaseRuntime* pRuntime;
    boolean connected;
    uint8 driverPosition;
    uint8 doorCount;
};
#endif // SIGNAL_CONNECTOR_INCLUDED