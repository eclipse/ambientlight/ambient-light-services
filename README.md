# Eclipse Ambient Light Services
## Realizes a new interactive lighting concept
- Developed as a software component which can be easily ported to different vehicle platforms
- The software calculates the current vehicle status based on input signals
- It provides output signals which contains color, brightness and motion commands
- The software uses a dataset which contains different animation scenarios

![Themepicture](/themecar.PNG)

- Replaying of predefined animation scenarios 
  - „Leaving Home“
  - „Coming Home“
  - „Antitheft-Warning“
- Creation, editing and generating custom animations with the easy-data-set editor

## Software component structure
- The project contains a wrapper:
- Translating signals into a C-struct
- Runtime is independent and portable
- No proprietary ARXML
- Signals are read/write operations from/to C-struct members
- An example is given of how to connect signals to [VSS](https://github.com/COVESA/vehicle_signal_specification) (Vehicle Signal Specification)

![Software component overview](/AmbientLight%20Software%20Component.png)

## How to guides
1. [Building of the library](/README.md#building-of-the-library)
2. [Unittesting](/README.md#unittesing)
3. [Using easy-data-set editor](/README.md#using-easy-data-set-editor)

### Building of the library
Ambiente Light Services uses CMake to configure the project and to create makefiles.

Prerequisits: 
- C/C++ compiler needed to build the library
- Doxygen is used to create documentation
- Googletest is used for unittests <b>and will be automaticly downloaded?</b>
- lcov can be used to generate a codecoverage report if you use the gnu compiler
- A testreport can be generated if you use the gnu compiler

The following buildtargets can be switched on/off in [main cmake file](/CMakeLists.txt):
- AMBLS_DOCUMENTATION: Generate documentation
- AMBLS_TEST_REPORT: Generate test report
- AMBLS_UNIT_TESTS_SPEC: Generate specification for unit tests
- AMBLS_CODE_COVERAGE: Generate code coverage report
- AMBLS_TESTS: Build tests

Buildingsteps:
1. Create an empty "build" directory .
2. Use CMake to configure and generate the build environment with in target directory "build".
3. Navigate to the build directory.
4. Call 'make' to create the library and an example of an executable [simple test](/src/hello_ambls/).
5. If you switched on additional buildtargets you can use 'make <BUILDTARGET>' to create them.


### Unittesting
<b>- will be delivered later -</b>

### Using easy-data-set editor
- The editor is used for the creation of animation scenarios.
- This is a WEB-page-based tool and can be opened in a browser.

![](/EasyDatasetEditor.png)
The resulting binary dataset can be flashed/set to the address of <i>dataSet</i> inside of the C-struct <i>BaseRuntime</i>.

#### Component diagram of edtiable animation events
![](/ComponentDiagramOfEditableAnimationElements.PNG)
